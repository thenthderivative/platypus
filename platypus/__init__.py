'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The names of the authors and contributors may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import threading
import datetime
import time
import tkinter
import sys
import platypus.base
import platypus.core
import os
import re
import numpy as np
from matplotlib import pyplot as plt

from . import widgets

''' Useful for debugging!
import inspect

def caller():
    f = inspect.currentframe().f_back.f_back.f_back
    mod = f.f_code.co_filename
    lineno = f.f_lineno
    return " [{}:{}]".format(mod, lineno)

class SmartLock(object):
    def __init__(self):
        self.lock = threading.RLock()
    def acquire(self):
        print(threading.current_thread().name + " requests  " + str(self) + caller())
        self.lock.acquire()
        print(threading.current_thread().name + " obtains   " + str(self) + caller())
    def release(self):
        print(threading.current_thread().name + " releasing " + str(self) + caller())
        self.lock.release()
    def __enter__(self):
        self.acquire()
    def __exit__(self, type, value, traceback):
        self.release()
'''

# Platypus data directory root
datadir = os.environ['PLATYPUS_DATA'] if "PLATYPUS_DATA" in os.environ else r"C:\data"

# Global platypus lock. Multiple threads can access platypus, but not at the same time.
platypuslock = threading.RLock()

# List of logbooks
logbooks = []

# Lists of the various types of channels
cchannels = []
mchannels = []

# Dictionary of widgets
widgetdict = {}

mainthread = None

# List of open experiments to which a stop command must be sent.
experiments = []
events = []

class NoLock(object):
    '''Dummy lock that can be acquired and released and works with 'with'.'''
    def __enter__(self): pass
    def __exit__(self): pass
    def acquire(self): pass
    def release(self): pass

def r_sig(x,n):
    x = float(x)
    if x == 0: return x
    n = int(n)
    # Round 'x' to 'n' significant digits.
    return np.round(x,-int(np.floor(np.log10(np.abs(x))))+n-1)

def execinmain(f, *args, block=True, **kwargs):
    '''
    Execute f in the main thread.
    if called from outside the main thread and block is False
    (default True), execute asynchronously instead.
    EX: execinmain(lambda:widget.add(...))
    '''
    # Reserve the return value.
    ret = [None]

    # If this needs to execute now AND we're in the main thread,
    # just run it now.
    if threading.current_thread() is mainthread and block:
        ret[0] = f(*args, **kwargs)
        return ret[0]
    # Otherwise, either make a real or fake lock.
    lock = threading.Lock() if block else NoLock()
    lock.acquire() # Engage the lock.
    def _h():
        ret[0] = f(*args, **kwargs)
        lock.release() # Release the lock.
        return False # Ensure this runs one time.
    widgetdict['mainwindow'].after_idle(_h)
    lock.acquire() # Re-engage the lock (waits until it is released).
    return ret[0]

def getcch():
    '''Get a COPY of the list of control channels.'''
    return cchannels[:]
def getmch():
    '''Get a COPY of the list of measurement channels.'''
    return mchannels[:]
def getch():
    '''Get a COPY of the list of channels.'''
    return cchannels + mchannels

def addevent(ev):
    with platypuslock: events.append(ev)
def removeevent(ev):
    with platypuslock: events.remove(ev)

def addchannel(ch):
    '''Add a channel to the monitor.'''
    with platypuslock:
        lst = None
        if ch.gettype() == 'c': lst = cchannels
        elif ch.gettype() == 'm': lst = mchannels
        else:
            raise Exception("Channel "
                         + str(ch)
                         + " is neither a control, nor a measurement channel.")
        def add_GUI():
            w = widgets.ChannelEntry(widgetdict['mainwindow'], ch)
            w.pack(expand = 0, fill = tkinter.X)
            widgetdict[ch] = w
        if ch not in lst:
            lst.append(ch)
            execinmain(add_GUI)

def addchannels(chs):
    '''Add a list of channels to the monitor.'''
    for ch in chs: addchannel(ch)
def removechannel(ch):
    '''Remove a channel from the monitor.'''
    with platypuslock:
        if ch in cchannels:
            cchannels.remove(ch)
        if ch in mchannels:
            mchannels.remove(ch)
        def rmv_GUI():
            widgetdict[ch].pack_forget()
            return False
        execinmain(rmv_GUI)

def read(chs = None):
    '''Read every channel and update the displays (asynchronously).
    Return the results as an ordered tuple.
    Alternatively, pass a single channel or list of channels to read.
    '''
    with platypuslock:
        chall = getch()
        if chs is not None:
            try:
             chs = [e for e in chs]
            except TypeError:
             chs = [chs]
        else:
            chs = chall
        chs = [c for c in chs if c in chall]
        Vs = [c.read() for c in chs]
        for v, c in zip(Vs, chs):
            execinmain(lambda:widgetdict[c].setvalue(v))
        return Vs

def update(chs = None):
    '''
    Updates the value of each channel, updates the displays.
    Returns them as an ordered tuple.
    Alternatively, pass a single channel or list of channels to update.
    '''
    with platypuslock:
        # Convert chs into a list of channels if it isn't one already.
        chall = getch()
        if chs is not None:
            try:
             chs = [e for e in chs]
            except TypeError:
             chs = [chs]
        else:
            chs = chall
        chs = [c for c in chs if c in chall]
        Vs = [c.getvalue() for c in chs]
        for v, c in zip(Vs, chs):
            execinmain(lambda:widgetdict[c].setvalue(v))
        return Vs

def stop(*args):
    '''Stops all experiments and channels.'''
    # Stop all experiments
    for exp in experiments: exp.stop()
    # Stop all channels
    for ch in getch(): ch.stop()
    for e in events: e.set()

def exit():
    ''' Close platypus GUI so the main loop can die. '''
    with platypuslock:
        execinmain(lambda:plt.close("all"))
        if widgetdict['mainwindow']:
            execinmain(lambda:widgetdict['mainwindow'].destroy())
            widgetdict['mainwindow'] = None

class LogBook(object):
    def __init__(self, fname):
        self.fname = fname
        if not os.path.exists(self.fname):
            if not os.path.exists(os.path.dirname(self.fname)) and os.path.dirname(self.fname) != '': os.makedirs(os.path.dirname(self.fname))
            open(self.fname, 'a').close()
        self.reset()
        self.get_datetimefromfile()
    def get_datetime(self):
        t = time.localtime(time.time())
        dat = time.strftime("%A %B %d, %Y", t)
        tim = time.strftime("%H:%M", t)
        return dat,tim
    def get_datetimefromfile(self):
        with open(self.fname, 'r') as fi:
            lines = fi.readlines()
        dateline = re.compile("(\S.*)")
        timeline = re.compile("  (\d{2}:\d{2}) ")
        dates = [dateline.match(line) for line in lines]
        dates = [date.group(1) for date in dates if date]
        if len(dates) > 0:
            lastdate = dates[-1]
            self.ldate = lastdate
        else: self.ldate = "None"
        times = [timeline.match(line) for line in lines]
        times = [time.group(1) for time in times if time]
        if len(times) > 0:
            lasttime = times[-1]
            self.ltime = lasttime
        else:
            self.ltime = "None"
    def reset(self):
        self.time = time.time()
        self.ldate, self.ltime = self.get_datetime()
        self.lvalues = {c.getname():c.read() for c in getcch()}
    def record(self, chs=None):
        lines = []
        if chs is None: chs = getcch()
        for c in chs:
            entry = c.stringify(fmt=None)
            lines.append("Record {}".format(entry))
        self.log(lines)
    def record_changed(self):
        nvalues = {c.getname():c.read() for c in getcch()}
        nnames = {n for n in nvalues.keys() if n in self.lvalues} # Only check names that are in *both* dictionaries.
        lines = []
        for n in nnames:
            if not np.isclose(nvalues[n],self.lvalues[n]):
                entry = "{name} = {value}".format(name=n,value=r_sig(nvalues[n],3))
                lines.append("Changed {}".format(entry))
        self.log(lines)
        self.lvalues = nvalues
    def log(self, lines):
        if isinstance(lines, str): lines = [lines]
        if len(lines) <= 0: return
        ndate, ntime = self.get_datetime()
        lines = [('  ' + ntime + ' ' if i==0 and (ntime!=self.ltime or ndate != self.ldate) else " "*8) + line + '\n' for i, line in enumerate(lines)]
        if ndate != self.ldate: lines.insert(0, ndate+'\n')
        try: # Try in case this is a network file and the network goes down for some reason.
            with open(self.fname, 'a') as fo:
                fo.writelines(lines)
        except Exception as e:
            print(e)
        self.ldate = ndate
        self.ltime = ntime

def openlogbook(dir=None,name=None):
    if dir is None:
        logpath = os.path.join(datadir, "logbooks")
    else:
        logpath = dir
    if not os.path.exists(logpath): os.makedirs(logpath)
    if name is None:
        date = datetime.date.today()
        name = os.path.join(logpath, "{}_{}_{}.log".format(date.year, date.month, date.day))
    else:
        name = os.path.join(logpath, name)
    logbook = LogBook(name)
    logbooks.append(logbook)

def log(*args, sep=' ', reset=False):
    lines = []
    if reset:
        for logbook in logbooks: logbook.reset()
    if len(args) <= 0:
        for logbook in logbooks: logbook.record_changed()
    else:
        lines = [sep.join([str(arg) for arg in args])]
    for logbook in logbooks: logbook.log(lines)

def mainloop(thread=None):
    '''
    Create and manage UI.
    This routine and callbacks from the main loop are the ONLY
    functions allowed to make calls to GTK!
    Runs until the GUI is destroyed.
    '''
    with platypuslock:
        # Create the window
        mainwindow = tkinter.Tk()
        mainwindow.wm_title("Platypus State Monitor")

        # The process should terminate if the user closes the window.
        # FIXME: Should process a STOP signal first when this happens.
        def _exit(*args):
            stop()
            if  widgetdict['mainwindow']:
                widgetdict['mainwindow'].destroy()
                widgetdict['mainwindow'] = None
            sys.exit()
        mainwindow.protocol('WM_DELETE_WINDOW', _exit)

        widget_chanlist = tkinter.Frame(mainwindow)
        widget_chanlist.pack(expand = 1, fill = tkinter.BOTH)

        stopwidget = tkinter.Button(mainwindow, text='STOP', command = stop)
        stopwidget.pack(side=tkinter.BOTTOM, expand = 0, fill = tkinter.X)

        widgetdict['mainwindow'] = mainwindow
        widgetdict['chanlist']   = widget_chanlist
        widgetdict['stop']       = stopwidget
    if thread: thread.start()
    # When the program quits, close the shell.
    tkinter.mainloop()

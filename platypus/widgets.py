'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import tkinter
import numpy as np

class FloatEntry(tkinter.Entry):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bind('<FocusIn>', self.on_changed)
        self.bind('<FocusOut>', self.on_changed)
        self.bind('<Return>', self.on_changed)
        self.v = 0.0
    def on_changed(self, *args):
        text = self.get().strip()
        try:
            self.v = float(text)
        except: pass
        self.delete(0, tkinter.END)
        self.insert(0, str(self.v))
    def setvalue(self, v):
        self.v = v
        self.delete(0, tkinter.END)
        self.insert(0, str(self.v))
    def getvalue(self):
        return float(self.get())

class FloatLabel(tkinter.Label):
    def setvalue(self, v):
        if isinstance(v, np.ndarray) and (len(v.shape) > 1 or v.shape[0] > 4):
            self.config(text="<array>")
        else:
            self.config(text=str(v))

class ChannelEntry(tkinter.Frame):
    def __init__(self, master, ch):
        super().__init__(master)
        self.ch = ch

        self.label = tkinter.Entry(self)
        self.label.bind('<FocusIn>', self.rename)
        self.label.bind('<FocusOut>', self.rename)
        self.label.bind('<Return>', self.rename)
        self.updatename()
        self.label.pack(side=tkinter.LEFT, expand = 0, fill = tkinter.X)

        self.cvalue = FloatLabel(self)
        self.cvalue.setvalue(self.ch.getvalue())
        self.cvalue.pack(side=tkinter.LEFT, expand = 0, fill = tkinter.X)
    def setvalue(self, v):
        self.cvalue.setvalue(v)
    def updatename(self, *args):
        self.label.delete(0, tkinter.END)
        self.label.insert(0, self.ch.getname())
    def updatevalue(self, *args):
        self.cvalue.setvalue(self.ch.getvalue())
    # Try to rename the channel with the current text.
    def rename(self, *args):
        self.ch.setname(self.label.get())
        self.updatename()

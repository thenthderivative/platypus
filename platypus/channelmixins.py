import platypus
import time
import threading
import numpy as np

class RangeLimited(object):
    '''Mixin class for a scalar or array-like valued channel that is limited to values on a closed inteval between two limits.'''
    def __init__(self, *args, limits = None, **kwargs):
        '''
            Parameters:
            - limits [None]: None or a pair of values defining a closed range of valid values for the channel.
        '''
        super().__init__(*args, **kwargs)
        if limits is not None: self.setlimits(*limits)
        else: self.limits = None
    def setlimits(self, low, high):
        '''Set the range of valid values to the closed interval between "low" and "high".'''
        low, high = sorted([low, high])
        v = np.array(self.getvalue())
        if not np.all(low <= v) and np.all(v <= high): raise ValueError('Given value is not within supplied limits for this channel.')
        self.limits = [low, high]
    def getlimits(self):
        '''Get the pair of bounds of the closed inteval of valid values.'''
        return self.limits
    def validate(self, v):
        v = np.array(v)
        if self.limits is None: return False
        low, high = self.limits
        return np.all(low <= v) and np.all(v <= high) and super().validate(v)
    def set(self, v, *args, **kwargs):
        if self.validate(v):
            super().set(v, *args, **kwargs)
        else:
            raise ValueError('Attempted to set a channel to an invalid value.')

class SelfUpdating(object):
    '''Mixin class for channels that makes them update the monitor when they change.'''
    def set(self, *args, **kwargs):
        super().set(*args, **kwargs)
        platypus.update(self)

class Throttled(object):
    '''Mixin class for a scalar valued channel that is ramped, stepwise, with a minimum step size and minimum duration between steps.'''
    def __init__(self, *args, stepsize=None, delay=None, rate=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.setstepsize(stepsize)
        self.setdelay(delay)
        if rate is not None: self.setrate(rate)
        self._last = time.monotonic()
        self.stop_event = threading.Event()

    def setstepsize(self, stepsize):
        self.stepsize=abs(stepsize) if stepsize is not None else None
    def getstepsize(self):
        return self.stepsize

    def setdelay(self, delay):
        self.delay=abs(delay) if delay is not None else None
    def getdelay(self):
        return self.delay

    def setrate(self, rate):
        rate=abs(rate)
        if self.stepsize is not None:
            self.delay = self.stepsize/rate
        elif self.delay is not None:
            self.stepsize = rate*self.delay
        else:
            raise Exception('When supplying a rate, be sure to first specify either the step size or the delay.')
    def getrate(self):
        if self.stepsize is None or self.delay is None: raise Exception('Rate is undefined. Be sure to specify both the step size and the delay.')
        return self.stepsize/self.delay

    def stop(self):
        self.stop_event.set()
    def check_throttle(self):
        if self.delay is None: raise Exception('Throttled channel has no defined delay.')
        if self.stepsize is None: raise Exception('Throttled channel has no defined step size.')
    def ready(self): return self.readyin()<=0 # Not used by the mixin but useful.
    def readyin(self):
        now = time.monotonic()
        return self._last+self.delay - now
    def readywait(self):
        wait_time = Throttled.readyin(self)
        if wait_time > 0: self.stop_event.wait(wait_time)
    def startwait(self):
        self._last = time.monotonic()
    def set(self, v, *args, **kwargs):
        Throttled.check_throttle(self)
        safestep = self.getstepsize()
        self.stop_event.clear()
        while True:
            remainder = v-self.getvalue()
            Throttled.readywait(self)
            if self.stop_event.is_set(): break # Ready to cancel at the last second!
            if abs(remainder) <= safestep:
                # Jump the rest of the way.
                super().set(v, *args, **kwargs)
                break
            else:
                jump = np.sign(remainder)*safestep
                super().set(self.getvalue()+jump, *args, **kwargs)
                Throttled.startwait(self)
        Throttled.startwait(self)

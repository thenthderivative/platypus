'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The names of the authors and contributors may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import platypus
import os
import numpy as np
import threading
import datetime
import re
import itertools as it
import numbers

# Below is an example of using cooperative subclasses:
# class CooperativeA(CooperativeObject):
#     def __init__(self, arg1, arg2, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         print("A: {} {} {} {}".format(arg1, arg2, args, kwargs))
# class CooperativeB(CooperativeObject):
#     def __init__(self, arg3, arg4, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         print("B: {} {} {} {}".format(arg3, arg4, args, kwargs))
# class CooperativeC(CooperativeA):
#     def __init__(self, arg3, arg4, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         print("C: {} {} {} {}".format(arg3, arg4, args, kwargs))
# class CooperativeD(CooperativeA, CooperativeB): pass
# >>> CooperativeC(1, 2, 3, 4, some_keyword = 'blah')
# A: 3 4 () {'some_keyword': 'blah'}
# C: 1 2 (3, 4) {'some_keyword': 'blah'}
# >>> CooperativeD(1, 2, 3, 4, some_keyword = 'blah')
# B: 3 4 () {'some_keyword': 'blah'}
# A: 1 2 (3, 4) {'some_keyword': 'blah'}

def valuesto2Darray(values):
    lengths = [1 if isinstance(x, numbers.Number) else len(x) for x in values]
    M = max(lengths)
    if M == 1: # 1D array (row) of values.
        values = np.array(values)
        values = values.reshape(1, values.shape[0])
    else: # 2D array of values because some values are actually waveforms.
        values = [np.repeat(v, M) if l==1 else np.hstack([np.array(v), np.repeat(np.nan, M-l)]) for v,l in zip(values, lengths)]
        values = np.vstack(values).T
    return values

class IteratedProcedure(object):
    '''
        Common interface for re-entrant procedures.
        For instance: an optimization routine where the number of steps is not known prior to execution.
        .start() will be called at the start of the procedure.
        .advance() will be called repeatedly.
        .done() will be called to check if the procedure is complete.
        .end() will be called the first time the procedure indicates it is complete.
    '''
    def start(self): pass
    def advance(self): pass
    def done(self): return True
    def end(self): pass

class CooperativeObject(object):
    '''
    Somewhat hackish solution to the problem that 'object' doesn't ignore
    extra parameters to its constructor.
    This enables cooperative subclassing with variable
    argument lists to constructors.
    Instead of inheriting 'object' in subclass, inherit this.

    Cooperative objects also have RLocks built in (self.lock)
    # The following is no longer true
    #AND default attributes (set in kwattrs).
    #Ex:
    #    class Example(CooperativeObject):
    #        def __init__(self, *args, **kwargs):
    #            super().__init__(self, *args, kwattrs={'A':10, 'B':5}, **kwargs)
    #    EX = Example(A=3)
    #    print(EX.A) # 3
    #    print(EX.B) # 5 (default)
    '''
    def __init__(self, *args, **kwargs):
        self.lock = threading.RLock()
        super().__init__()

class EventSource(CooperativeObject):
    '''
        A source of events to which one can subscribe a set of callback functions.

        Example usage:
            >>> def f(a): print(a)
            ...
            >>> source = EventSource()
            >>> a = source.bind('s', f, 0)
            >>> b = source.bind('t', f, 1)
            >>> c = source.bind('t', f, 2)
            >>> source.post('t')
            1
            2
            >>> source.post('s')
            0
            >>> source.unbind(b)
            >>> source.post('t')
            2

    '''
    def __init__(self, *args, **kwargs):
        self.events = {}
        super().__init__(*args, **kwargs)
    def bind(self, event, function, *args):
        '''
            Add a listener to this event source.
            In:
                'event': A key identifying the event.
                'function': A callback function, to which the remaining arguments are passed.
            Out:
                A unique object representing the binding, which can be passed to "unbind".
        '''
        nu = (function, args)
        if event not in self.events:
            self.events[event] = [nu]
        else:
            self.events[event].append(nu)
        return (event, nu)

    def unbind(self, nu):
        '''
            Remove a listener from this event source.
            In:
                'nu': A value returned by a previous "binding" call.
        '''
        event, nu = nu
        # Remove this endpoint from subscription lists.
        if event not in self.events: return
        self.events[event].remove(nu)
        if len(self.events[event]) == 0: del self.events[event]

    def post(self, event):
        '''
            Send the given event string to all subscribers.
            In:
                'event': A key identifying the event.
        '''
        if event not in self.events: return
        for nu in self.events[event]:
            f, args = nu
            f(*args)

class Experiment(CooperativeObject):
    '''A stoppable, extendable minimalist experiment.'''
    def __init__(self, *args, **kwargs):
        '''Automatically adds the experiment to the internal experiment list'''
        super().__init__(self, *args, **kwargs)
        self.stopped = False
        self.stoplock = threading.RLock()

    def register(self, on):
        with platypus.platypuslock:
            # Remove the experiment from the registry to enable stop signals.
            if on:
                if self not in platypus.experiments: platypus.experiments.append(self)
            else:
                if self in platypus.experiments: platypus.experiments.remove(self)

    def stop(self):
        '''Stop the experiment.'''
        with self.stoplock:
            self.stopped = True

    def shouldstop(self):
        '''Return whether or not the experiment should stop.'''
        with self.stoplock:
            T = self.stopped
            self.stopped = False
            return T

    def validate(self):
        '''
        Returns True iff the run does not violate constraints or there are none to violate.
        '''
        return True

    def start(self):
        '''
        Set up and execute the run.
        Experiment must manually call self.register(True) and self.register(False)
        to enable and disable stop signals if required.
        This method should encapsulate the entire experiment script.
        '''
        raise NotImplementedError

class Plotter(CooperativeObject):
    '''
    Base class for plotters.
    All methods thread-safe!
    '''
    def __init__(self, *args, **kwargs):
        '''
            Keyword arguments:
            - open: should the plot begin with an open reference (True)
        '''
        self.opened = 0
        super().__init__(*args, **kwargs)
        if 'open' in kwargs and kwargs['open']: self.open()

    def open(self):
        '''Open a reference to the plotter.'''
        with self.lock:
            self.opened += 1

    def close(self):
        '''Close a reference to the plotter and finalize if last one.'''
        with self.lock:
            self.opened -= 1
            if self.opened <= 0:
                self.finalize()

    def finalize(self):
        '''
        Deletes the plot and frees up its resources.
        Unsafe if there are remaining references.
        Called automatically when the last reference is closed.
        '''
        raise NotImplementedError

    def update(self):
        '''
        Signals a new state to the plotter, which then adds this data.
        '''
        pass

    def saveimage(self, file):
        '''Optionally save the plot to the designated file object (or path) 'file'.'''
        pass

    def savedescriptor(self, file):
        '''Write a block of text to the file object 'file' describing what was plotted.'''
        pass

class Channel(CooperativeObject):
    '''Base channel class. Methods are thread-safe!'''
    def stringify(self, fmt=None, scale=False):
        name = self.getname()
        value = self.getvalue()
        p = re.compile("([\w\s]+)\((.+)\)")
        m = p.match(name)
        if m:
            name = m.group(1).strip()
            units = m.group(2).strip()
        else:
            name = name.strip()
            units = None
        if units and scale:
            p = re.compile("[xX]?(\d+[eE][\-\+]?\d+|\d+)\s*(\w*)")
            m = p.match(units)
            if m: # There is a multiplier.
                mult, units = m.groups()
                value *= float(mult)
        if fmt is None:
            fmt = "{name} = {value} ({units})" if units else "{name} = {value}"
        s = fmt.format(name=name, value=value, units=units)
        return s
    def __str__(self):
        return self.stringify()
    def validate(self, value):
        '''Return whether the given value is within constraints.'''
        return True # By default, all values are valid.

    def set(self, v):
        '''
        Synchronously send the channel to the given value safely.
        If the channel cannot be set, this method may pass.
        '''
        raise NotImplementedError

    def read(self):
        '''
        Read the value of the channel (may involve a hardware read).
        If data is available from the last trigger, read it instead.
        '''
        raise NotImplementedError

    def getvalue(self):
        '''Return the last known value of the channel from memory.'''
        raise NotImplementedError

    def stop(self):
        '''Stop any set operation which is underway.'''
        raise NotImplementedError

    def gettype(self):
        '''Return 'c' (control channel) or 'm' (measurement channel).'''
        raise NotImplementedError

    def getname(self):
        '''Get channel name'''
        return self.name
    def setname(self, name):
        '''Set channel name'''
        if not name or not hasattr(self, 'name'): self.name = "UNNAMED"

        # Strip away all whitespace.
        if name: self.name = "".join(name.split())

        # Update widget's name in the state monitor (if appropriate).
        if self in platypus.widgetdict:
            platypus.widgetdict[self].updatename()

class StandardExperiment(Experiment, EventSource):
    '''
        StandardExperiment extends Experiment with some default functionality
        that can be used to create experiments with more regular behaviour.
        The methods and static functions provided are designed to be used by
        the deriving class rather than by the user, who should not assume that they exist.

        Default StandardExperiments create a directory and files describing the run
        and the initial and final states of the channels, as well as standardized data files.
        They also create png images of plots and save them in the same directory.
    '''
    def __init__(self, *args, **kwargs):
        '''
            Creates or reserves a run directory specified by the return value
            of the static method nextdir(). This may be replaced or extended
            at the user's option.
            Keyword Arguments:
                'separator' [', ']: Separator for values on each data line.
                'postfix' [None]: Extra extension to run directory name.
        '''

        # Defaults for keyword attributes.
        kwattrs = {'separator':', ', 'postfix':None}

        # Set keyword-based attributes
        for key in kwattrs:
            setattr(self, key, (kwargs if key in kwargs else kwattrs)[key])

        # Run the superclass constructor
        super().__init__(*args, **kwargs)

        # Generate a new run directory if one wasn't supplied.
        self.rundir = self.nextdir(self.postfix)
        self.runname = os.path.basename(self.rundir)

        # Ensure the directory exists, therefore reserving the name.
        if not os.path.exists(self.rundir): os.mkdir(self.rundir)
        # TODO: Check that this directory is empty.

        self.plots = [] # Empty list of plots
        self.state = 0 # Experiment not yet started.

        self.file = None # Storage for data-file reference.

        #Print experiments Folder(so that it is logged in log file)
        print('------------------------------------------------------')
        print('Experiment ' + self.rundir)

    def done(self):
        return self.state == 2

    def nextdir(self, postfix = None):
        '''
            Computes the next non-existent directory of the form:
              platypus.datadir/yyyymmMondd/yyyymmMondd_NNN
            and returns it, creating the parent directory if necessary.
        '''

        # Get the current date and time.
        today = datetime.date.today()
        # Generate the prefix string: yyyymmdd
        prefix = "{}{:02}{:02}".format(today.year, today.month, today.day)
        today_data = os.path.join(platypus.datadir, prefix)
        if os.path.exists(today_data) and os.path.isfile(today_data):
            raise Exception(today_data + " already exists as a file!")
        if not os.path.isdir(today_data):
            os.makedirs(today_data)

        # today_data is now guaranteed to exist.
        # Search its contents for the largest number, and increase by one.
        prog = re.compile(prefix + "_([0-9]+)")
        dirs = next(os.walk(today_data))[1]
        dirs = [prog.match(x) for x in dirs]
        N = max(it.chain((int(x.group(1)) for x in dirs if x), [-1]))+1

        if postfix:
            # Does a file with this postfix exist for that directory number?
            testdir = os.path.join(today_data, prefix + "_{:03}".format(N-1))
            if not os.path.exists(testdir + "_" + postfix) and not N==0:
                print("Creating {}".format(testdir))
                N -= 1

        # Return the corresponding (non-existent) directory name
        return os.path.join(today_data, prefix + "_{:03}".format(N) + (("_" + postfix) if postfix else ""))

    def filebase(self):
        '''Return the base file path for the experiment's directory.

        If the run directory is "C:\data\X" then
        filebase() returns "C:\data\X\X"
        and "C:\data\X\X.set is the settings file (for example)"
        '''
        return os.path.join(self.rundir, os.path.split(self.rundir)[1])

    def writevalues(self, values):
        '''Write values to the datafile'''
        values = valuesto2Darray(values)
        fo = self.datafile()
        np.savetxt(fo, values, delimiter=self.separator)
#        self.datafile().write(self.separator.join([str(v) for v in values]) + "\n")
        fo.flush()

    def datafile(self):
        '''Returns an open reference to the run's standard data file.'''
        if not self.file:
            self.file = open(self.filebase() + ".dat", 'a')
        return self.file

    def dumpstate(self, ext):
        '''
        Generate a file and dump the state of each tracked
        channel using the extension 'ext'
        '''
        today = datetime.datetime.today()

        # Open and truncate the file.
        fpath = self.filebase() + "." + ext
        if os.path.exists(fpath):
            raise Exception(fpath + " already exists.")
        with open(fpath, 'w') as set:
            set.write(today.isoformat() + "\n")
            set.writelines("{} = {}\n".format(ch.getname(), ch.read()) for ch in platypus.getch())

    def getcolumns(self):
        '''
        Replace with a method returning an iterable of channels
        used in the experiment, in the order they appear in the .dat file.
        '''
        return None
        # EX: return [channel_A, channel_B, ...]

    def dumpheader(self):
        '''
        Generates a file and places the name of each column in it.
        '''
        # Do nothing if getcolumns returns None or []
        cols = self.getcolumns()
        if not cols: return

        # Open and truncate the file.
        fpath = self.filebase() + ".head"
        if os.path.exists(fpath):
            raise Exception(fpath + " already exists.")
        with open(fpath, 'w') as head:
            head.write(self.separator.join([ch.getname() for ch in cols]) + "\n")
            for plot in self.getplots():
                plot.savedescriptor(head)

    def defaultplot(self):
        '''
        Return an instance of the default plot for the experiment (or None).
        This is called as part of the default addplot.
        '''
        return None
    def addplot(self, plots=None, **kwargs):
        '''
        Add the given plot to the experiment.
        If no plot is given, generate the default plot (if there is one).
        Returns the added plot, if any.
        '''
        with self.lock:
            # If no plot given and none already created, create the default set.
            if plots is None and len(self.getplots()) == 0:
                plots = self.defaultplot(**kwargs)
            # If plots is still None, that's all folks!
            if plots is None: return None
            # If plot is a single element, generate an iterable.
            try:
                _ = next(iter(plots))
            except:
                plots = [plots]
            # Add each plot.
            self.plots.extend(plots)
            return plots
    def removeplot(self, plot=None):
        '''
        Remove the given plot from the experiment.
        If no plot is given, remove and close all plots (if any).
        Returns the given plot if there is one and it is not finalized.
        Returns a list of all non-finalized plots that were removed, otherwise.
        '''
        with self.lock:
            # Plot given. Remove it.
            if plot: self.plots.remove(plot)
            # No plot given. Remove all.
            else: self.plots = []

    def getplots(self):
        '''Return a list of plots known to the experiment.'''
        return self.plots[:]

    def _cycle(self):
        '''
        Private method that implements the experiment cycle of repeatedly
        calling iterate().
        '''
        with self.lock:
            if self.state != 1:
                raise Exception('Experiment not running. Cannot iterate.')
            # Stop existing experiments.
            # platypus.stop()

            # Clear last stop signal and register for more.
            self.shouldstop()
            self.register(True)

            # Run until stop signal received or self.iterate() returns False
            while True:
                if self.shouldstop():
                    # Disable stop signal (we are paused)
                    #print("Paused")
                    self.pause()
                    return
                if not self.iterate():
                    #print("Stopped")
                    self.finish()
                    return

    def validate(self): return True

    def start(self):
        '''
        Sets up the run and repeatedly calls self.iterate().
        Returns when a stop signal is received or self.iterate()
        returns False. In the former case, the experiment may be resumed.
        Also adds the experiment to the registry so it can receive stop sig.
        Extend this method rather than replacing it (optionally).
        '''
        if not self.validate():
            raise Exception('Configuration error (experimental validation failed).')
        if self.state != 0:
            raise Exception('Experiments should start at most once.')
        self.state = 1 # Started!
        # Create the .start file.
        self.dumpstate("start")
        self.dumpheader() # Create the .head file.
        #Print start time so that it is logged in the logfile
        platypus.log()
        print('Started: ' + str(datetime.datetime.now().date()) + ' at ' + str(datetime.datetime.now().time()) + '\n')
        self._cycle()

    def iterate(self):
        '''
        One iteration of a multiple-iteration experiment (optional).
        The experiment will process a stop command between calls to iterate,
        so make no assumptions that this function will be called again.
        Returns True if the experiment is going to continue, or False otherwise.
        Replace this method rather than extending it (optionally).
        '''
        return False

    def resume(self):
        '''
        Begin iterating again after receiving a stop command.
        If iterate is not implemented, this does nothing.
        Returns when the experiment is paused (stop signal) or complete.
        In the case of success, finish() will have run.
        Replace or extend this method (optionally).
        '''
        self._cycle()

    def pause(self):
        '''
        Called automatically when the experiment is interrupted by
        a stop signal).
        Extend this method rather than replacing it (optionally).
        '''
        self.register(False)
        if self.file:
            self.file.close()
            self.file = None

    def finish(self):
        '''
        Called automatically when the experiment finishes successfully
        (self.iterate() returns False, NOT on a stop signal).
        Extend this method rather than replacing it (optionally).
        '''
        with self.lock:
            # Check that we WERE running.
            if self.state != 1:
                raise Exception('Experiments must start before finishing, and must finish exactly once.')
            self.state = 2 # Finished!

            # Do all the stuff in pause() too!
            self.pause()

            #Print experimentsfinish time (so that it is logged in log file)
            print('Finished: ' + str(datetime.datetime.now().date()) + ' at ' + str(datetime.datetime.now().time()))
            print('\n------------------------------------------------------\n\n')
            platypus.log()
            # Save a png of each figure!
            plots = self.getplots()
            if len(plots) == 1:
                plots[0].saveimage(self.filebase() + ".png")
            else:
                for i, p in enumerate(plots):
                    p.redraw()
                    p.saveimage(self.filebase() + "_#{}.png".format(i))

            # Remove all plots
            self.removeplot()

            # Dump the end state.
            self.dumpstate("end")

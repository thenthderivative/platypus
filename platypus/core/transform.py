import numbers
import types
import numpy as np
import platypus
import itertools as it
from platypus.core import Dummy

class Transform(object):
    def __init__(self, real_channels, alpha, alpha_inv=None, names=[]):
        '''
            Construct N virtual channels from a list of M real channels (or just one real channel).
            If alpha is a matrix and is provided, transforms real values to virtual values according to v=alpha@r.
            If alpha is an invertible matrix, transforms virtual values to real values as well.
            If alpha is a function, transforms real values to virtual values according to v=alpha(r).
            If alpha is a function and alpha_inv is given, transforms virtual values to real values according to r = alpha_inv(v).
        '''
        # Make sure real_channels is a list.
        try:
            real_channels = list(real_channels)
        except:
            real_channels = [real_channels]

        # Make sure names is a list.
        if isinstance(names, str):
            names = [names]

        # is alpha scalar?
        if isinstance(alpha, numbers.Number):
            # The later code should be able to handle this.
            alpha = np.array([alpha]), np.array([1/alpha])

        # Is alpha a function?
        if isinstance(alpha, types.FunctionType):
            if alpha is not None and not isinstance(alpha, types.FunctionType): raise TypeError("Use either functions or matrices.")
            self.alpha = alpha
            self.alpha_inv = alpha_inv

            N = self.alpha(np.array([rch.getvalue() for rch in real_channels]))

            try: N = len(N)
            except TypeError: N = 1

            names = names + ["V_{}".format(i+1) for i in range(len(names), N)]
            self.rchannels = real_channels
            self.vchannels = [Dummy(name) for name in names]
            self._r_to_v = self._f_r_to_v
            self._v_to_r = self._f_v_to_r if self.alpha_inv else self._invalid
            return

        # Alpha is an N by M matrix.
        if len(alpha.shape) == 1:
            Na = 1
            Ma = alpha.shape[0]
        elif len(alpha.shape) >= 2:
            Na,Ma = alpha.shape[0:2]

        # If alpha is a invertible, invert it.
        if Na==Ma:
            # If alpha is square, try to invert it.
            # Note: iversion will fail on a 1x1 matrix, even though it's trivial.
            if Na == 1:
                self.alpha_inv = np.array([1/alpha[0]])
            else:
                try: self.alpha_inv = np.linalg.inv(alpha)
                except: pass


        # If alpha is square, try to invert it.
        if Na==Ma and Na != 1:
            try: self.alpha_inv = np.linalg.inv(alpha)
            except: pass

        Mr = len(real_channels)
        Nv = len(names)
        names = names + ["V_{}".format(i+1) for i in range(Nv, Na)]
        self.rchannels = real_channels
        self.vchannels = [Dummy(name) for name in names]
        # Add an extra virtual channel to augment the real and/or virtual gate list.
        if Mr+1 == Ma:
            self.rchannels = self.rchannels + [Dummy(value=1.0)]
            self.vchannels[-1].set(1.0)
        self._r_to_v = self._v_r_to_v
        self.alpha = alpha
        self._v_to_r = self._invalid if not hasattr(self, 'alpha_inv') else self._v_v_to_r
    def __iter__(self):
        # Lets you iterate over this object as though it were a list of virtual channels. 
        return iter(self.vchannels)
    def __call__(self):
        # Lets you iterate over this object as though it were a list of virtual channels. 
        return self.vchannels
    def validate(self, V=None, R=None):
        # Validate the given channel values using real channel validation test.
        if R is None:
            if V is None: V = np.array([vch.getvalue() for vch in self.vchannels])
            R = self.alpha@V
        return all(rch.validate(r) for r, rch in zip(R, self.rchannels))

    def _f_r_to_v(self, R): return self.alpha(R)
    def _f_v_to_r(self, V): return self.alpha_inv(V)
    def _v_r_to_v(self, R): return self.alpha@R
    def _v_v_to_r(self, V): return self.alpha_inv@V
    def _invalid(self, X): raise ValueError('The transformation is not invertable.')

    def v_to_r(self):
        '''
            Set the values of the real gates using the pre-set values of the virtual gates.
        '''
        # Get the channel values.
        V = np.array([vch.getvalue() for vch in self.vchannels])
        R = self._v_to_r(V)
        if not self.validate(R=R):
            raise ValueError('Given virtual channel values would fail real channel validation.')
        for r, rch in zip(R, self.rchannels):
            rch.set(r)
        platypus.update(self.rchannels + self.vchannels)
    def r_to_v(self, read=True):
        '''
            Update the values of the virtual gates by getting the values of the real gates.
            Read the channel if read is True, else get the value from memory.
        '''
        R = np.array([rch.read() if read else rch.getvalue() for rch in self.rchannels])
        V = self._r_to_v(R)
        if isinstance(V, numbers.Number):
            self.vchannels[0].set(V)
        else:
            for v, vch in zip(V, self.vchannels):
                vch.set(v)
        platypus.update(self.rchannels + self.vchannels)
    # Aliases for common operations.
    def read(self): self.r_to_v(read=True)
    def update(self): self.r_to_v(read=False)
    def set(self): self.v_to_r()

def Augment(x, *args):
    # Augment the matrix 'x' (or the vector 'x', or the vector [x, args[0], ...]) by adding extra dimensions to support affine transformations.
    x = _tovector(x, *args)
    t1 = np.zeros((x.shape[0], 1))
    t2 = np.zeros((1, x.shape[0]+1))
    if len(x.shape) == 1:
        return np.append(x, [1.0])
    else:
        x = np.hstack([x,t1])
        x = np.vstack([x,t2])
        x[-1][-1] = 1.0
    return x

def _tovector(x, *args):
    # Converts an array_like 'x' or array_like [x, args[0], ...] to a numpy array.
    if len(args) > 0:
        x = np.array([x, *args])
    else:
        x = np.array(x)
    return x
def Translate(x, *args):
    # Translation by vector 'x'.
    x = _tovector(x, *args)
    T = np.eye(x.shape[0]+1)
    T.T[-1][:-1] = x
    return T
def Scale(x, *args):
    # Scaling by vector 'x'.
    x = _tovector(x, *args)
    x = np.append(x, [1.0])
    T = np.diag(x)
    return T
def Rotate2D(theta):
    # Affine counter-clockwise rotation of a 2D vector about z axis.
    c=np.cos(theta)
    s=np.sin(theta)
    T = np.array([[c,-s,0],[s,c,0],[0,0,1]])
    return T
def Rotate3D(theta, n, *args):
    # Affine counter-clockwise rotation of a 3D vector about the axis that points along 'n'.
    n = _tovector(n, *args)
    n = n/np.sqrt(np.sum(n**2))
    c=np.cos(theta)
    s=np.sin(theta)
    T = Augment(c*np.eye(3) + (1-c)*np.outer(n,n) + s*np.array([[0, -n[2], +n[1]], [n[2], 0, -n[0]],[-n[1], n[0], 0]]))
    return T

import numbers
import math
import numpy as np
from scipy.ndimage.filters import gaussian_filter as _gf
import matplotlib.pyplot as plt
import platypus
import platypus.base
from platypus.core import LinePlot, Dummy

## Sequence Functions
def point(CLOCK, V):
    return np.array([V])

def step(CLOCK, V, T):
    T_PT = 1/CLOCK
    N = T/T_PT # Number of points to realize duration 't'.
    N = int(round(N)) # Calculate nearest integer.
    return np.repeat(V, N)

def ramp(CLOCK, V1, V2, T):
    T_PT = 1/CLOCK
    N = T/T_PT # Number of points to realize duration 't'.
    N = int(round(N)) # Calculate nearest integer.
    return np.linspace(V1, V2, N)

def padding(CLOCK, V, T, *args):
    T_PT = 1/CLOCK
    N = (T-sum(args))/T_PT # Number of points to realize duration 't'.
    N = int(round(N)) # Calculate nearest integer.
    if N < 0: N = 0
    return np.repeat(V, N)

## Filters
def gaussian_filter(dat, CLOCK, TC):
    Nc = TC*CLOCK # Convolve over this many points.
    dat = _gf(dat, sigma=Nc, mode='wrap')
    return dat

def rescale(dat, CLOCK, A):
    # Multiply the incoming data by scalar A.
    return A*dat

class PulseViewer(platypus.base.Channel):
    def __init__(self, name=None):
        '''
            'fname'
                The name of the target waveform file on the AWG hard drive.
            'builder'
                PulseBuilder object that produces the arbitrary waveform.
        '''
        self.setname(name)
        self.plot = LinePlot(Dummy('T(s)'),Dummy(name))
        self.xch, self.ych = self.plot.xdat[0], self.plot.ydat[0]
    def set(self, data=None, CLOCK=None):
        if not isinstance(CLOCK, numbers.Number): CLOCK = CLOCK.read()
        T = np.linspace(0, len(data)/CLOCK, len(data))
        self.xch.set(T)
        self.ych.set(data)
        self.plot.redraw()
        
class PulseParameter(platypus.base.Channel):
    def __init__(self, name, value):
        self.value = float(value)
        self.setname(name)
        self.updated = False
    def set(self, value):
        if math.isclose(value, self.value): return
        self.value = float(value)
        self.updated = True
    def read(self): return self.value
    def getvalue(self): return self.value
    def validate(self, v): return True
    def gettype(self): return 'c'
    def stop(self): pass
    def trigger(self): pass

class PulseBuilder(object):
    def __init__(self, CLOCK=1E9, alpha=None, Nchs=1, tile='TRIGGERED', outputs = []):
        if isinstance(alpha, int) or alpha is None:
            self.alpha = None; self.Nchs = max(len(outputs), Nchs)
        else:
            self.alpha = np.array(alpha); self.Nchs = self.alpha.shape[1] if len(self.alpha.shape) == 2 else 1
        if isinstance(CLOCK, platypus.base.Channel): self.CLOCK = CLOCK
        else: self.CLOCK = PulseParameter('CLOCK', CLOCK)
        self.tile = tile
        self.sequence = [[] for i in range(self.Nchs)]
        self.filters = [[] for i in range(self.Nchs)]
        self.parameters = {}
        self.waveforms = None
        self.outputs = outputs
    def register_parameter(self, v):
        if isinstance(v, PulseParameter):
            # It's an actual PulseParameter object. Insert it with its own name.
            self.parameters[v.name] = v
            return v
        elif isinstance(v, numbers.Number):
            # It's a constant. Don't do anything.
            return v
        elif isinstance(v, str):
            # It's a key to the parameter list.

            # Assume a default value of zero.
            default = 0.0
            # Extract the parameter name (ex: 'T1') and (if present) the default value specifier (ex:'=100E-9').
            name, *default_specifier = [s.strip() for s in v.split('=')]
            if len(default_specifier) == 1: # If there is exactly one default value specifier...
                try:
                    default = float(default_specifier[0])
                except: raise ValueError("Error parsing PulseParameter specifier.".format(v))
            if not name in self.parameters:
                # We haven't seen it before. Register a new parameter.
                self.parameters[name] = PulseParameter(name, float(default))
            return self.parameters[name]
        raise TypeError('Pulse parameters must be numberic, strings, or PulseParameter objects.')
    def get_parameter_value(self, v):
        if isinstance(v, PulseParameter):
            # It's a PulseParameter object. Return its value.
            return v.read()
        elif isinstance(v, numbers.Number):
            # It's a constant. Return it.
            return float(v)
        else:
            # It's a key to the parameter list. Return its value.
            return self.parameters[v].read()

    def get_parameters(self): return list(self.parameters.values())
    def get_parameter_names(self): return list(self.parameters.keys())
    def get_parameter_by_name(self, name): return self.parameters["".join(name.split())]
    def get_parameters_by_name(self, names): return [self.parameters["".join(name.split())] for name in names]

#    def add_point(self, V, ch=1): self.add_function(point, V, ch=ch)
#    def add_step(self, V, T, ch=1): self.add_function(step, V, T, ch=ch)
#    def add_ramp(self, V1, V2, T, ch=1): self.add_function(step, V1, V2, T, ch=ch)
#    def add_padding(self, V, T, *Ti, ch=1): self.add_function(padding, V, T, *Ti, ch=ch)

#    def add_gaussian_filter(self, TC, ch=1): self.add_filter(gaussian_smooth, TC, ch=1)

    def add_function(self, function, *args, ch=1):
        args = [self.register_parameter(v) for v in args]
        self.sequence[ch-1].append((function, *args))
    def add_filter(self, function, *args, ch=1):
        args = [self.register_parameter(v) for v in args]
        self.filters[ch-1].append((function, *args))

    def force_update(self): self.update(True)
    def update(self, force=False):
        '''
            Build and upload the prescribed sequences.
            If 'force=False', rebuild only if necessary.
            Force a re-build using 'force=True'.
        '''
        # Check if waveforms need updating, either because they aren't pre-computer or the parameters changed.
        if self.waveforms is not None and not any(p.updated for p in self.get_parameters()) and force is False: return
        # Mark parameters as updated.
        for p in self.get_parameters(): p.updated = False
        self.waveforms = [0]*self.Nchs
        max_length = 0
        for i in range(self.Nchs):
            # Build sequence
            def build(function, *parameters):
                parameters = [self.get_parameter_value(v) for v in parameters]
                return function(self.CLOCK.read(), *parameters)
            dat = np.concatenate([build(*s) for s in self.sequence[i]])
            # Apply filters
            def filter(dat, function, *parameters):
                parameters = [self.get_parameter_value(v) for v in parameters]
                return function(dat, self.CLOCK.read(), *parameters)
            for f in self.filters[i]: dat = filter(dat, *f)
            # Tile (optional)
            tile = self.tile.upper()
            self.waveforms[i] = dat
            max_length = max(max_length, dat.shape[0])
        for i in range(self.Nchs):
            # Pad the waveform to the maximum length if necessary.
            dat = self.waveforms[i]
            v = dat[-1]
            N = max_length-dat.shape[0]
            dat = np.concatenate((dat, np.repeat(v, N)))
            if tile is None or tile == 'NONE': pass
            elif tile == 'CONTINUOUS': dat = np.tile(dat, math.ceil(1000/dat.shape[0])) # 1000 for continuous
            elif tile == 'TRIGGERED':  dat = np.tile(dat, math.ceil(2400/dat.shape[0])) # 2400 for triggered
            else: raise ValueError("Invalid waveform tile mode '{}' selected. Try 'CONTINUOUS', 'TRIGGERED', or 'NONE'.".format(tile))
            self.waveforms[i] = dat
        # Apply tranformation (if any).
        self.waveforms = np.array(self.waveforms)
        if self.alpha is not None: self.waveforms=(self.alpha[None]@np.array(self.waveforms))[0]
        for wave, file in zip(self.waveforms, self.outputs):
            if file: file.set(CLOCK=self.CLOCK, data=wave)
        return

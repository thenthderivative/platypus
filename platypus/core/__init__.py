'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The names of the authors and contributors may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import sys
import platypus
import platypus.base
import time
import threading
import datetime
import itertools as it
import numpy as np
import math
import matplotlib.pyplot as plt

class Dummy(platypus.base.Channel):
    '''A settable memory channel.'''
    def __init__(self, name = None, type='c', value=0.0, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setname(name)
        self.v = value
        self.type = 'm' if type == 'm' else 'c'
    def validate(self, value): return True
    def set(self, v):
        with self.lock: self.v = v
    def read(self):
        return self.getvalue()
    def trigger(self): pass
    def getvalue(self):
        with self.lock: return self.v
    def stop(self): pass
    def gettype(self): return self.type

class LimitedDummy(Dummy):
    def __init__(self, *args, limits=(-np.inf, np.inf), **kwargs):
        self.limits=limits
        super().__init__(*args, **kwargs)
    def setlimits(self,l,h):
        self.limits = min(l,h), max(l,h)
    def getlimits(self): return self.limits
    def validate(self, value):
        l,h = self.getlimits()
        return l <= value and value <= h
    def set(self,v):
        if self.validate(v):
            super().set(v)
        else:
            raise ValueError('Input value exceeds limits.')

class Rando(platypus.base.Channel):
    '''
        Dummy measurement channel that produces samples of coherent noise.
    '''
    def __init__(self, name = None, N=1, *args, **kwargs):
        '''
          'name' (default: None): The channel name.
          'N' (default: 1): The number of samples to take per read operation.

          If 'N' is 1, read() produces scalar values.
          If 'N' is not 1, read() produces 1D numpy arrays of length 'N'.
        '''
        super().__init__(*args, **kwargs)
        self.setname(name)
        self.v = np.zeros(N) if N>1 else 0.0
        self.N = N
    def validate(self, value): return True # Always valid.
    def set(self, v): pass # Meaningless.
    def trigger(self): pass
    def read(self):
        with self.lock:
            # Sample random numbers between -1 and +1.
            # Produce a random array of length N or, if N is 1, produce a single number.
            self.v += np.random.random(self.N if self.N>1 else None)*2.0-1.0
            # Shrink toward 0.0 by 10%. Keeps things from growing out of control.
            self.v /= 1.1
            return self.getvalue()
    def getvalue(self):
        with self.lock: return self.v
    def stop(self): pass
    def gettype(self): return 'm'

class Timer(platypus.base.Channel):
    '''A timer (can be set to a particular time, and counts up from there).'''
    def __init__(self, name = "Time", *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setname(name)
        self.set(0.0)
    def validate(self, value): return True
    def set(self, t):
        with self.lock:
            self.last = time.monotonic() - t
            self.t = t
    def trigger(self): self.read()
    def aread(self): return self.getvalue()
    def read(self):
        with self.lock:
            self.t = time.monotonic() - self.last
            return self.getvalue()
    def getvalue(self):
        with self.lock: return self.t
    def stop(self): pass
    def gettype(self): return 'm'

class DummyTimer(Timer):
    '''A timer that counts from the last time it was set to zero (all other values are ignored)'''
    def set(self, v):
        if np.isclose(v, 0):
            with self.lock:
                self.last = time.clock()
                self.t = 0

class Trace(platypus.base.StandardExperiment):
    '''
        Sweep a single channel (xch) from v1 to v2 through ('steps' + 1) evenly distributed points.
        Positional Arguments:
        'xch': Channel object to sweep.
        'v1': Initial value of xch.
        'v2': Final value of xch.
        'steps': Number of points (-1) to sample at.
        Keyword Arguments:
        'ych': Array of additional channels to record/plot. Default: all measuable channels.
        'delay': Time in seconds to wait between points.
    '''
    def __init__(self, xch, v1, v2, steps, *args, **kwargs):
        # Defaults for keyword-based attributes.
        kwattrs = {
            'ychs': platypus.getmch(), # Default to measuring all channels.
            'delay': None
        }

        # Set keyword-based attributes
        for key in kwattrs:
            setattr(self, key, (kwargs if key in kwargs else kwattrs)[key])

        super().__init__(*args, **kwargs)

        # Save positional arguments.
        self.xch = xch
        self.v1 = v1
        self.v2 = v2
        self.steps = steps

        # Convert ychs into a list of channels if it isn't one already.
        try:
           self.ychs = [e for e in self.ychs]
        except TypeError:
           self.ychs = [self.ychs]

        self.i = 0
        platypus.log("Created", type(self).__name__, self.runname, "Sweeping", xch.stringify("{name}"), "from", v1, "to", v2, "measuring", ", ".join([c.stringify("{name}") for c in self.ychs]))
    def defaultplot(self):
        return LinePlot(self.xch, self.ychs, xlabel=self.xch.getname(), title = self.filebase())

    def validate(self):
        with self.lock:
            return self.steps >= 2 \
            and self.v2 != self.v1 \
            and self.xch \
            and self.xch.validate(self.v1) \
            and self.xch.validate(self.v2)

    def getcolumns(self): return (self.xch,) + tuple(self.ychs)
    def start(self):
        platypus.log(self.runname, "Started")
        super().start()
    def finish(self):
        platypus.log(self.runname, "Finished")
        super().finish()
    def iterate(self):
        # Build a list of channels to read.
        channels = [self.xch] + self.ychs

        # Set xch to the next voltage in the sequence.
        self.xch.set(self.v1 + (self.v2-self.v1)/self.steps*self.i)

        self.post('experiment_set')

        # Wait for the delay time.
        if self.delay: time.sleep(self.delay)

        # Hardware read of the values of each channel we're monitoring.
        values = [c.read() for c in channels] # Read all the channel values.

        self.post('experiment_read')

        # Update the state monitor.
        platypus.update(channels)

        # Write to the file object.
        self.writevalues(values)

        # Update the plots.
        for p in self.getplots(): p.update()

        self.i += 1

        return self.i <= self.steps

class Diagram(platypus.base.StandardExperiment):
    '''
        Sweep, step, and 3D step a variable number of channels, recording data at each point.
        Positional Arguments:
        'height', 'width', '3D steps', '4D steps', etc.
            Defines the resolution of the diagram, as well as its dimensions!
        Keyword Arguments:
        'ych': Array of channels to record/plot. Default: all measuable channels.
        'delay': Time in seconds to wait between each level of the diagram.
            Ex: delay = 0.1 # Wait 100ms between points.
            Ex: delay = [0.1, 1, 5] # Wait 100ms between points, an additional 1 second between sweeps, and an additional 5 seconds between 2D slices!
        'progress': Display progress bar. May interfere with other text output. Default: True
    '''
    class _Sweep(object):
        '''Structure for holding information about what one channel is doing.'''
        def __init__(self, ch, v1, v2):
            self.ch = ch
            self.v1 = v1
            self.v2 = v2
        def validate(self):
            return self.ch \
               and self.ch.validate(self.v1) \
               and self.ch.validate(self.v2) \
               and self.v1 != self.v2
        def __repr__(self):
            return "{}: {} to {}".format(self.ch.getname(), self.v1, self.v2)
    def __init__(self, *args, **kwargs):
        # Specify defaults for keyword-based attributes.
        kwattrs = {
            'ychs': platypus.getmch(), # Default to measuring all channels.
            'delay': None, # None, numeric, or array-like.
            'progress': True # Show progress bar?
        }

        # Set keyword-based attributes
        for key in kwattrs:
            setattr(self, key, (kwargs if key in kwargs else kwattrs)[key])

        super().__init__(**kwargs)

        # Validation of dimensions
        self.dimensions = tuple(int(d) for d in args)
        self.N = 1
        for dim in self.dimensions:
            self.N *= dim+1
        if len(self.dimensions) < 1:
            raise ValueError('Diagram must be at least one dimensional!')
        if not all(x > 0 for x in self.dimensions):
            raise ValueError('Diagram too small')

        # List of channels and ranges at each level (sweep, step, 3Dstep, etc.)
        self.controls = tuple([] for i in args)

        # Convert ychs into a list of channels if it isn't one already.
        try:
           self.ychs = [e for e in self.ychs]
        except TypeError:
           self.ychs = [self.ychs]

        platypus.log("Created", type(self).__name__, self.runname, "measuring", ", ".join([c.stringify("{name}") for c in self.ychs]))
    def sweep(self, *args):  self.stepN(0, *args)
    def step(self, *args):   self.stepN(1, *args)
    def step3D(self, *args): self.stepN(2, *args)
    def stepN(self, N, ch, v1, v2):
        if self.state != 0:
            raise Exception('Cannot add channels to diagram after starting!')
        SWP = self._Sweep(ch, v1, v2)
        SWP.validate()
        self.controls[N].append(SWP)
        verb = "Step{}".format(N)
        noun = ch.stringify("{name}")
        if N == 0: verb = "Sweep"
        if N == 1: verb = "Step"
        if N == 2: verb = "3DStep"
        platypus.log("{} {} from {} to {} in {} steps".format(verb, noun, v1, v2, self.dimensions[N]))

    def defaultplot(self, line=True):
        '''
            Keyword arguments:
                'line' [True]: Whether to force drawing of a line plot for this Diagram, even if it's a multidimensional experiment.
        '''
        plots = []
        def lineplot(): return LinePlot(self.controls[0][0].ch, ychs=self.ychs[0], title = self.filebase())
        def imgplot():
            xspan = (self.controls[1][0].v1, self.controls[1][0].v2)
            yspan = (self.controls[0][0].v1, self.controls[0][0].v2)
            return ImagePlot(self.dimensions[1]+1, self.dimensions[0]+1,
                             xspan, yspan,
                             self.controls[1][0].ch, self.controls[0][0].ch,
                             self.ychs[0],
                             colourmap='hot',
                             autodraw=True,
                             xlabel = self.controls[1][0].ch.getname(),
                             ylabel = self.controls[0][0].ch.getname(),
                             title = self.filebase())
        if len(self.controls[0]) < 1: raise Exception('Must sweep something.')
        if len(self.dimensions) > 1:
            if len(self.controls[0]) < 1: raise Exception('Must step something.')
            plots.append(imgplot())
        if len(self.dimensions) == 1 or line: plots.append(lineplot())
        return plots if plots else None

    def validate(self):
        return all(x.validate() for x in self.getch())

    def getch(self):
        return [control for level in self.controls for control in level]

    def getcolumns(self):
        return [c.ch for c in self.getch()] + self.ychs

    def start(self):
        '''Start the experiment'''

        '''
        Example assumes you did:
            exp = Diagram(2, 1)
            exp.sweep(X, 0, 1)
            exp.sweep(Y, 0, 2)
            exp.step (Z, 0, 1)
            exp.start()
        where X, Y, Z are Channel objects.
        '''

        # Construct a generator of the points we will be visiting.
        '''
        Each level is a list of lists
        of points (one for each channel in the level).
        EX:
            sweeps[0] is now ((0, 1, 2), (0, 2, 4))
            sweeps[1] is now ((0, 1))
        '''
        def swp(v1, v2, steps):
            return [v1 + (v2-v1)/steps*i for i in range(steps+1)]
        sweeps = [[swp(ch.v1, ch.v2, steps) for ch in level] for steps, level in zip(self.dimensions, self.controls)]

        '''
        Zip up each level, so that each level is a sequence of points.
        EX:
            sweeps[0] is now ((0, 0), (1, 2), (2, 4))
            sweeps[1] is now ((0), (1))
        '''
        sweeps = [zip(*level) for level in sweeps]

        '''
        Compute permutations to generate the sequence of all points in the diagram.
        EX:
            sweeps is now
           ((0, 0, 0),
            (1, 2, 0),
            (2, 4, 0),
            (0, 0, 1),
            (1, 2, 1),
            (2, 4, 1))
        '''
        sweeps = (it.chain(*reversed(levels)) for levels in it.product(*reversed(sweeps)))

        # Save the generator
        self.points = enumerate(sweeps)

        # Save a copy of the final list of independent channels.
        self.xchs = [x.ch for x in self.getch()]

        # Start the experiment
        self.start_time = datetime.datetime.now()
        platypus.log(self.runname, "Started")

        # Periods at which to send signals.
        self.periods = [math.prod([x+1 for x in self.dimensions[:i]]) for i in range(1,len(self.dimensions))]

        super().start()
    def iterate(self):
        try:
            # Get the next point
            i, point = next(self.points)
        except StopIteration:
            # Generator empty. End of experiment.
            return False
        # Potentially update the progress bar.
        if self.progress:
            if i == 0:
                # Initial call to print 0% progress
                printProgress(0, prefix = 'Progress:', suffix = '', barLength = 30)
            progress = i / self.N
            if progress > 0:
                elapsed = datetime.datetime.now() - self.start_time
                projected = elapsed / progress
                remaining = projected - elapsed
                d = remaining.days
                h = remaining.seconds // 3600
                m = remaining.seconds // 60 % 60
                s = remaining.seconds % 60
                remaining_strs = [("d", d), ("h", h), ("m", m), ("s", s)]
                remaining_strs = ", ".join(["{} {}".format(number, label) for label, number in remaining_strs if number > 0])
                printProgress(progress, prefix = 'Progress:', suffix = "ETR: " + remaining_strs.ljust(22), barLength = 30)

        # For each channel and value pair,
        for xch, value in zip(self.xchs, point):
            # set the independent channels
            xch.set(value)

        # Post the "set" event.
        self.post('experiment_set')

        # Check for and post the 'step, 3Dstep', etc. events.
        for N,p in enumerate(self.periods):
            if i%p == 0 and i!=0:
                d = {0:"experiment_step", 1:"experiment_3DStep"}
                if N in d: self.post(d[N])

        # Wait the specified time.
        try: # Assume delay is an iterable.
            wait = self.delay[0]
            for p,d in zip(self.periods, self.delay[1:]):
                if d and (i%p == 0): wait = max(wait, d)
            print(wait)
            time.sleep(wait) # Wait the longest delay that is called for.
        except TypeError: # Delay is either not iterable or not subscriptable. Handle it the same way either way.
            if self.delay: # Is it at least truthy?
                time.sleep(self.delay) # Try to wait "delay" seconds.

        # Trigger the dependent channels
##        for ych in self.ychs:
##            ych.trigger()

        # Read the dependent channels
        for ych in self.ychs:
            ych.read()

        # Post the "read" event.
        self.post('experiment_read')

        # Update the state monitor
        chs = self.xchs + self.ychs
        platypus.update(chs)

        # Write data to the file!
        self.writevalues([ch.getvalue() for ch in chs])

        # Update the plots.
        for p in self.getplots(): p.update()
        # Continue to iterate
        return True
    def pause(self):
        if self.progress: sys.stdout.write("__" + "\r".rjust(70))
        super().pause()
    def finish(self):
        if self.progress: sys.stdout.write("__" + "\r".rjust(70))
        platypus.log(self.runname, "Finished")
        super().finish()

class Monitor(platypus.base.StandardExperiment):
    '''
        Keyword Arguments:
        'condition': Callable which returns True when the experiment should end.
        'ych': Array of channels to record/plot. Default: all measuable channels.
        'delay': Time in seconds to wait between each point in the trace.
            Ex: delay = 0.1 # Wait 100ms between points.
            Ex: delay = [0.1, 1, 5] # Wait 100ms between points, an additional 1 second between sweeps, and an additional 5 seconds between 2D slices!
    '''
    def __init__(self, xchs, *args, **kwargs):
        def _null(): return False
        self.xchs = xchs
        try:
            iter(self.xchs)
        except TypeError: # It's not iterable.
            self.xchs = [self.xchs] # Now it is!

        # Specify defaults for keyword-based attributes.
        kwattrs = {
            'on_start': _null,
            'condition': _null,
            'on_iterate': _null,
            'ychs': platypus.getmch(), # Default to measuring all channels.
            'delay': None
        }

        # Set keyword-based attributes
        for key in kwattrs:   # Override non-members with defaults.
          if not hasattr(self, key):
            setattr(self, key, (kwargs if key in kwargs else kwattrs)[key])
          elif key in kwargs: # Only override members if in kwargs.
            setattr(self, key, kwargs[key])

        super().__init__(**kwargs)

        # Convert ychs into a list of channels if it isn't one already.
        try:
           self.ychs = [e for e in self.ychs]
        except TypeError:
           self.ychs = [self.ychs]
        platypus.log("Created", type(self).__name__, self.runname, "measuring", ", ".join([c.stringify("{name}") for c in self.ychs]), "over", ", ".join([c.stringify("{name}") for c in self.xchs]))
    def defaultplot(self):
        return [LinePlot(self.xchs[0], ychs=self.ychs[0], title = self.filebase())]

    def validate(self):
        return True

    def getcolumns(self):
        return self.xchs + self.ychs

    def start(self):
        # Start the experiment
        self.start_time = datetime.datetime.now()
        self.on_start()
        platypus.log(self.runname, "Started")
        super().start()
    def iterate(self):
        self.on_iterate()

        # Post the "set" event.
        self.post('experiment_set')

        # Wait the specified time.
        if self.delay: time.sleep(self.delay)

        chs = self.xchs + self.ychs

        # Read the dependent channels
        for ch in chs:
            ch.read()

        # Post the "read" event.
        self.post('experiment_read')

        # Update the state monitor
        platypus.update(chs)

        # Write data to the file!
        self.writevalues([ch.getvalue() for ch in chs])

        # Update the plots.
        for p in self.getplots(): p.update()
        # Continue to iterate
        return not self.condition()
    def finish(self):
        platypus.log(self.runname, "Finished")
        super().finish()

class SeriesData(platypus.base.Channel):
    '''A channel representing a data series. Not to be created directly. Use sub-classes.'''
    def __init__(self, setfunc, getfunc, source, name, maxcount=np.inf, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setname(name)
        sf = getattr(source, setfunc); self.setfunc = lambda x: platypus.execinmain(sf, x)
        gf = getattr(source, getfunc); self.getfunc = lambda: platypus.execinmain(gf)
        self.maxcount = maxcount
    def clear(self):
        if isinstance(self, XYSeriesData): self.setfunc(np.array(([],[])))
        elif isinstance(self, ImageData): self.setfunc(np.zeros(self.getfunc().shape))
        else: lambda: self.setfunc(np.array([]))
    def extend(self, new):
        existing = self.getfunc()
        Ndelete = (new.shape[-1] + existing.shape[-1]) - self.maxcount # Number to remove.
        Ndelete = max(Ndelete, 0) # Use at least zero.
        new = np.append(existing[Ndelete:], new, axis=-1)
        self.setfunc(new)
    def set(self, v): self.setfunc(v)
    def read(self): return self.getfunc()
    def getvalue(self): return self.getfunc()
    def validate(self, value): return True
    def stop(self): pass
    def gettype(self): return 'c'

class XSeriesData(SeriesData):
    # 1D array, x values.
    def __init__(self, source, name=None, maxcount=1000, *args, **kwargs):
        super().__init__('set_xdata','get_xdata', source, name, maxcount, *args, **kwargs)
class YSeriesData(SeriesData):
    # 1D array, y values.
    def __init__(self, source, name=None, maxcount=1000, *args, **kwargs):
        super().__init__('set_ydata','get_ydata', source, name, maxcount, *args, **kwargs)
class XYSeriesData(SeriesData):
    # 2D array, rows are x,y
    def __init__(self, source, name=None, maxcount=1000, *args, **kwargs):
        super().__init__('set_data','get_data', source, name, maxcount, *args, **kwargs)
class ImageData(SeriesData):
    # 2D array, image data.
    def __init__(self, source, name=None, *args, **kwargs):
        super().__init__('set_array','get_array', source, name, *args, **kwargs)

class LinePlot(object):
    '''
    A simple plotter which scales automatically and dynamically plots multiple lines.
    xch: The X-coordinate of the plot.
    ych: The data channels to plot on the same axes.
    keyword arguments:
        'count' [1000]: The number of points to keep on the plot.
        'autodraw' [True]: Redraw for every data point?
        'plottext' [None]: Initial text to draw on the plot.
        'title': Title of the plot
        'xlabel': Label for the x-axis of the plot.
        'ylabel': Label for the y-axis of the plot.
    '''
    def __init__(self, xch, ychs, experiment=None, *args, **kwargs):
        '''
            Creates a line plot. Spawns settable and readable channels that correspond to X and Y axis data.
        '''
        # The plot is open.
        self.closed = False
        self.lock = threading.RLock() # Reentrant lock.

        # Convert ychs into a list of channels if it isn't one already.
        try: ychs = list(ychs)
        except TypeError: ychs = [ychs]

        # List of defaults for keyword arguments.
        kwdefaults = {
            'name': "LinePlot",          # Name of the LinePlot channel.
            'count': 1000,               # Num. of points to keep
            'xlabel': xch    .getname(), # Default x label.
            'ylabel': ychs[0].getname(), # Default y label.
            'title': experiment.filebase() if experiment else None,
                                # This option exits to speed up data taking.
            'autodraw': True,   # Draw on every update?
        }

        # Set keyword argument defaults.
        for key in kwdefaults:
            if key not in kwargs: kwargs[key] = kwdefaults[key]
            setattr(self, key, kwargs[key])

        # Generate dict of valid matplotlib plot attributes which were included in kwargs.
        plotattributes = ['title', 'xlabel', 'ylabel']
        plotattributes = {k:v for k,v in kwargs.items() if k in plotattributes}

        #Save relevant information.
        self.xch = xch
        self.ychs = ychs
        def create_plot():
            #Create a figure object. Figsize is (width, height) in inches.
            #dpi is dots per inch.
            #(figsize does not include title, axis labels, etc, only the plot)
            self.fig = plt.figure()

            #Make the figure into a graph and assign it a title and axis labels
            self.ax = self.fig.add_subplot(111, **plotattributes)

            #Prints the (optional) given text on the graph.
            if 'text' in kwargs: self.fig.text(x=0.15, y=0.84, s=kwargs['text'], fontdict=None)

            #Plot empty lines to add data to in future:
            self.lines = [self.ax.plot([],[])[0] for i in self.ychs]
            self.fig.show()
        platypus.execinmain(create_plot)
        self.xdat = [XSeriesData(source=line, maxcount=kwargs['count']) for line in self.lines]
        self.ydat = [YSeriesData(source=line, maxcount=kwargs['count']) for line in self.lines]

    def redraw(self):
        '''Draw the visual components of the plot, usually in response to new data.'''
        def _redraw():
            self.ax.relim()
            self.ax.autoscale_view()
            self.fig.canvas.draw()
        platypus.execinmain(_redraw)

    def clear(self):
        for x in self.xdat + self.ydat: x.clear()
        if self.autodraw: self.redraw()

    def getdata(self): return [(x,y) for x,y in zip(self.xdat, self.ydat)]

    def update(self):
        with self.lock:
            if self.closed: return

            values = platypus.base.valuesto2Darray([self.xch.getvalue()] + [ych.getvalue() for ych in self.ychs])
            X, *Ys = values.T

            for Y, x, y in zip(Ys, self.xdat, self.ydat):
                x.extend(X)
                y.extend(Y)
            if self.autodraw: self.redraw()

    def saveimage(self, file):
        def _h(): self.fig.savefig(file, facecolor='w')
        platypus.execinmain(_h)

    def savedescriptor(self, file):
        file.write('LinePlot: ' + ','.join(ch.getname() for ch in ([self.xch] + self.ychs)) + "\n")

class ImagePlot(platypus.base.Plotter):
    '''
    Plot pixels according to a colour map on a 2D surface of finite
    pre-determined resolution.
    Positional Arguments
        xres: The number of x points.
        yres: The number of y points.
        xspan: tuple representing xch's domain: [xlow, xhigh]
        yspan: tuple representing xch's domain: [xlow, xhigh]
        xch: The X-coordinate of the plot.
        ych: The Y-coordinate of the plot.
        zch: The data channel.
    Keyword Arguments:
        'text': Text to display on the plot.
        'xlabel', 'ylabel', 'zlabel': Labels for the x, y and z axes.
        'title': Title of the plot
    Keyword Attributes
        'count' [1000]: The number of points to keep on the plot.
        'autodraw' [True]: Redraw for every data point?
        'colourmap' ['hot']: Matplotlib colour map ID string.
    '''
    def __init__(self, xres, yres, xspan, yspan, xch, ych, zch, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.closed = False

        # List of defaults for keyword attributes.
        kwattrs = {
            'xlabel': xch.getname(),
            'ylabel': ych.getname(),
            'zlabel': zch.getname(),
            # This option exits to speed up data taking.
            'autodraw': True, # Draw on every update?
            'colourmap': 'hot'
        }
        # Set keyword arguments
        for key in kwattrs:
            if key not in kwargs: kwargs[key] = kwattrs[key]
            setattr(self, key, (kwargs if key in kwargs else kwattrs)[key])

        #Module input values are all converted into class attributes
        self.xres = xres
        self.yres = yres
        self.xspan = xspan
        self.yspan = yspan
        self.xch = xch
        self.ych = ych
        self.zch = zch
        
        #As part of the __init__ function, this function initializes the 3D plot
        def create_plot():
            # Create plot object:

            #Figure figsize is (width, height) in inches, dpi is dots per inch
            #(figsize does not include title, axis labels, etc, only the plot)
            self.fig = plt.figure()

            def _close(evt):
                self.hide()

            self.fig.canvas.mpl_connect('close_event', _close)

            #Make the figure a graph, and assign it a title and axis labels.
            plotattrs = ['title', 'xlabel', 'ylabel']
            self.ax = self.fig.add_subplot(111, **{k:v for k, v in kwargs.items() if k in plotattrs})
            self.ax.set_xlim(self.xspan[0], self.xspan[1])
            self.ax.set_ylim(self.yspan[0], self.yspan[1])

            #Declare the x,y increments and create a grid with the desired x and y points
            dx, dy = (self.xspan[1]-self.xspan[0])/(self.xres-1), (self.yspan[1]-self.yspan[0])/(self.yres-1)

            # transform x and y to boundaries of x and y
            x,y = np.meshgrid(np.arange(self.xspan[0],self.xspan[1]+dx,dx)-dx/2,np.arange(self.yspan[0],self.yspan[1]+dy,dy)-dy/2)

            # Allocate storage for the data matrix.
            self.matrix = np.zeros((self.yres, self.xres)) + np.nan

            #Make plot it, and specify the colour scheme and x/y limits.
#            self.image = self.ax.pcolormesh(self.matrix, cmap=self.colourmap, vmin=0.0, vmax=1.0)
            self.image = self.ax.imshow(self.matrix,
                interpolation='nearest', 
                origin='lower', 
                aspect='auto', # get rid of this to have equal aspect
                vmin=0.0,
                vmax=1.0, 
                cmap='Greys',
                extent=(self.xspan[0], self.xspan[1], self.yspan[0], self.yspan[1]))
            self.cbar = self.fig.colorbar(self.image, label=self.zlabel)

            #Add colorbar to the side of the plot to indicate intensity values
            ### NOTE: Not yet functioning
#            colorbar(self.ax)
#            cbar.set_label(self.zlabel)

            #Prints the name of the associated datafile on the graph. Only on
            #3D plot. May be commented out.
            if 'text' in kwargs: self.fig.text(x=0.15,y=0.84,s=text,fontdict=None)

            # Show the window
            self.fig.show()
        platypus.execinmain(create_plot)
        self.zdat = ImageData(source = self.image)

    # Draw the visual components of the plot, usually in response to new data.
    def redraw(self):
        if self.closed: return
        def _redraw():
            #self.image.set_data(self.matrix)
            self.image.autoscale() #?
            self.fig.canvas.draw()
        platypus.execinmain(_redraw)

    def clear(self):
        if self.closed: return
        self.zdat.set(np.zeros((self.yres, self.xres)) + np.nan)
        self.redraw()

    def savedescriptor(self, file):
        file.write('ImagePlot: ' + ','.join(ch.getname() for ch in (self.xch, self.ych, self.zch)) + "\n")

    def getdata(self): return self.zdat

    def update(self):
        with self.lock:
            if self.closed: return
            # Get x, y, and z values
            xval = self.xch.getvalue()
            yval = self.ych.getvalue()
            zval = self.zch.getvalue()

            X,Y,Z = platypus.base.valuesto2Darray([xval, yval, zval]).T
            for xval, yval, zval in zip(X,Y,Z):
                # Get position of this point in the matrix.
                xpos = round((xval-self.xspan[0])/(self.xspan[1]-self.xspan[0])*(self.xres-1))
                ypos = round((yval-self.yspan[0])/(self.yspan[1]-self.yspan[0])*(self.yres-1))
                if xpos < 0 or ypos < 0 or xpos >= self.xres or ypos >= self.yres:
                    print('__Experimental conditions set outside plot bounds: xval={}, yval={}'.format(xval, yval))
                    return

    #            print(self.xch.getname(), self.ych.getname(), self.zch.getname(), xpos, ypos, zval)

                #3D matrix is updated with new z data
                self.image._A[int(ypos), int(xpos)] = zval
                #self.matrix[int(ypos), int(xpos)] = zval

            #Redraws plot to include new data
            if self.autodraw:
                self.redraw()            
                
    #Saves the plot as a png image under the specified name
    def saveimage(self, file):
        def _h(): self.fig.savefig(file, dpi=None, facecolor='w')
        platypus.execinmain(_h)

    #Show and hide the windows.
    def show(self, *args): pass
        #def _show(): self.fig.show()
        #platypus.execinmain(_show)
    def hide(self, *args): pass
        #def _hide(): self.fig.hide()
        #platypus.execinmain(_hide)
        #self.open()
        #self.close()

# Print iterations progress
def printProgress (fraction, prefix = '', suffix = '', decimals = 2, barLength = 100):
    """
    Call in a loop to create terminal progress bar
    @params:
        fraction    - Required  : completion percentage
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
    """
    filledLength    = int(round(barLength * fraction))
    percents        = round(100.00 * fraction, decimals)
    bar             = '#' * filledLength + '-' * (barLength - filledLength)
    sys.stdout.write('__%s [%s] %s%s %s\r' % (prefix, bar, percents, '%', suffix)),
#    sys.stdout.flush() # Normally required, except that Tee already does this for us automatically.

class Transform(platypus.base.Channel):
    def __init__(self, chan, transform, inverse, name = None, *args, **kwargs):
        '''
            A virtual channel representing a transformation of 'chan' of the form x = f(y), where y is the new channel and x is the old one.
            Inputs:
            - transform: a function (with one argument) that evaluates x = f(y).
            - inverse: a function (with one argument) that evaluates y = f^{-1}(x).
        '''
        super().__init__(*args, **kwargs)
        self.setname(name)
        self.transform = transform
        self.inverse = inverse
        self.chan = chan
    def validate(self, v):
        return self.chan.validate(self.transform(v))
    def set(self, v):
        try:
            self.chan.set(self.transform(v))
        except ValueError: pass
    def read(self):
        try:
            return self.inverse(self.chan.read())
        except ValueError:
            return np.nan
    def getvalue(self):
        try:
            return self.inverse(self.chan.getvalue())
        except ValueError:
            return np.nan
    def trigger(self): self.chan.trigger()
    def stop(self): self.chan.stop()
    def gettype(self): return self.chan.gettype()

class SweepTogether(platypus.base.CooperativeObject):
    '''
        Sweep any number of channels to either a single value or to individual values.
    '''
    def __init__(self, chs, vs, immediate=True, *args, **kwargs):
        # Specify defaults for keyword-based attributes.
        kwattrs = {
            'delay': 0.05, # 50 ms default delay time.
            'progress': True,
            'maxstep': 0.01
        }

        self.chs = chs
        if isinstance(self.chs, platypus.base.Channel):
            self.chs = [self.chs]

        try:
            self.vs = list(it.repeat(float(vs), len(self.chs)))
        except:
            self.vs = list(vs)

        # Set keyword-based attributes
        for key in kwattrs:
            setattr(self, key, (kwargs if key in kwargs else kwattrs)[key])

        super().__init__(**kwargs)

        if immediate: self.start()

    def validate(self):
        return all(x.validate(x.read()) and x.validate(v) for v,x in zip(self.vs, self.chs))

    def start(self):
        '''
            Immediately sweeps all channels towards their chosen values, such that they all converge at the same time.
            This operation is repeatable.
        '''
        # Make sure the sweep is valid and get the current values.
        if not self.validate(): raise ValueError("Channel start or finish value out of range.")

        # List start values in order. getvalue() is guarantee to be accurate because of the validate() call above.
        S = [ch.getvalue() for ch in self.chs]

        # Get the end values in order.
        E = self.vs

        # Calculate the minimum number of steps to keep each step under "maxstep" in size (limited by the biggest total change).
        N = int(np.ceil(max([abs(e-s)/self.maxstep for s,e in zip(S,E)])))

        stop_event = threading.Event();
        platypus.addevent(stop_event)

        # Perform the synchronized sweep.
        for i in range(N+1):
            if stop_event.is_set(): break
            # Step each channel by one.
            for ch, s, e in zip(self.chs,S,E):
                ch.set(s + (e-s)*i/N)
            platypus.update(self.chs)
            # Wait "delay" seconds.
            if self.delay: time.sleep(self.delay)

        platypus.removeevent(stop_event)

class HilbertDiagram(Diagram):
    '''
        Sweep, step, and 3D step a variable number of channels, recording data at each point.
        Positional Arguments:
        'height', 'width', '3D steps', '4D steps', etc.
            Defines the resolution of the diagram, as well as its dimensions!
            Note that for Hilbert curves, these dimensions must be equal and equal to a power of two (at least 2^1).
        Keyword Arguments:
        'ych': Array of channels to record/plot. Default: all measuable channels.
        'delay': Time in seconds to wait between each level of the diagram.
            Ex: delay = 0.1 # Wait 100ms between points.
            Ex: delay = [0.1, 1, 5] # Wait 100ms between points, an additional 1 second between sweeps, and an additional 5 seconds between 2D slices!
        'progress': Display progress bar. May interfere with other text output. Default: True
    '''
    def __init__(self, *args, **kwargs):
        # Specify defaults for keyword-based attributes.
        kwattrs = {
            'ychs': platypus.getmch(), # Default to measuring all channels.
            'delay': None, # None, numeric, or array-like.
            'progress': True # Show progress bar?
        }

        # Set keyword-based attributes
        for key in kwattrs:
            setattr(self, key, (kwargs if key in kwargs else kwattrs)[key])

        platypus.base.StandardExperiment.__init__(self,**kwargs)

        # Validation of dimensions
        self.dimensions = tuple(int(d) for d in args)
        self.N = len(self.dimensions)
        if any((dim&(dim-1)) for dim in self.dimensions):
            raise ValueError("All dimensions must be the same power of 2")
        if not all(dim == self.dimensions[0] for dim in self.dimensions):
            raise ValueError("All dimensions must be the same power of 2")
        if len(self.dimensions) < 1:
            raise ValueError('Hilbert diagram must be at least one dimensional!')
        if not all(dim > 1 for dim in self.dimensions):
            raise ValueError('Hilbert diagram must be of order 1 at least (2x2)')

        self.Hilbert_order = int(np.log2(self.dimensions[0]))

        p = self.Hilbert_order
        n = len(self.dimensions)
        self.N = 2**(p*n)-1

        # List of channels and ranges at each level (sweep, step, 3Dstep, etc.)
        self.controls = tuple([] for i in args)

        # Convert ychs into a list of channels if it isn't one already.
        try:
           self.ychs = [e for e in self.ychs]
        except TypeError:
           self.ychs = [self.ychs]

    def defaultplot(self, line=True):
        '''
            Keyword arguments:
                'line' [True]: Whether to force drawing of a line plot for this Diagram, even if it's a multidimensional experiment.
        '''
        plots = []
        def imgplot():
            xspan = (self.controls[1][0].v1, self.controls[1][0].v2)
            yspan = (self.controls[0][0].v1, self.controls[0][0].v2)
            return ImagePlot(self.dimensions[1], self.dimensions[0],
                             xspan, yspan,
                             self.controls[1][0].ch, self.controls[0][0].ch,
                             self.ychs[0],
                             colourmap='hot',
                             autodraw=True,
                             xlabel = self.controls[1][0].ch.getname(),
                             ylabel = self.controls[0][0].ch.getname(),
                             title = self.filebase())
        if len(self.controls[0]) < 1: raise Exception('Must sweep something.')
        if len(self.dimensions) < 2 or len(self.controls[1]) < 1: raise Exception('Must step something.')
        plots.append(imgplot())
        return plots if plots else None

    def start(self):
        '''Start the experiment'''

        try: import hilbertcurve.hilbertcurve
        except: raise Exception('Requires the "hilbertcurve" package from PyPi (https://pypi.org/project/hilbertcurve/)')

        p = self.Hilbert_order
        n = len(self.dimensions)
        h = hilbertcurve.hilbertcurve.HilbertCurve(p,n)

        def curve():
            for i in range(2**(p*n)):
                pt = h.point_from_distance(i)
                yield [ch.v1 + (ch.v2-ch.v1)*pt[i]/(2**p-1) for i, level in enumerate(self.controls) for ch in level]

        # Save the generator
        self.points = enumerate(curve())
        #for pt in self.points: print(pt)

        # Save a copy of the final list of independent channels.
        self.xchs = [x.ch for x in self.getch()]

        # Pointless... even misleading. TODO: Remove this in the next refactor.
        self.periods = [math.prod([x+1 for x in self.dimensions[:i]]) for i in range(1,len(self.dimensions))]

        # Start the experiment
        self.start_time = datetime.datetime.now()
        platypus.base.StandardExperiment.start(self)

class ReversingDiagram(Diagram):
    '''
        Sweep, step, and 3D step a variable number of channels, recording data at each point.
        Positional Arguments:
        'height', 'width', '3D steps', '4D steps', etc.
            Defines the resolution of the diagram, as well as its dimensions!
            Note that for Hilbert curves, these dimensions must be equal and equal to a power of two (at least 2^1).
        Keyword Arguments:
        'ych': Array of channels to record/plot. Default: all measuable channels.
        'delay': Time in seconds to wait between each level of the diagram.
            Ex: delay = 0.1 # Wait 100ms between points.
            Ex: delay = [0.1, 1, 5] # Wait 100ms between points, an additional 1 second between sweeps, and an additional 5 seconds between 2D slices!
        'progress': Display progress bar. May interfere with other text output. Default: True
    '''
    def start(self):
        from math import prod
        controls = [(ch.v1, ch.v2, i) for i, level in enumerate(self.controls) for ch in level]
        max_order = max([order for start, end, order in controls])

        def single_linspace(start, end, steps, copies):
            for i in range(steps+1):
                for n in range(copies):
                    yield start+(end-start)*i/(steps)

        def dual_linspace(start, end, steps, copies):
            while True:
                for pt in single_linspace(start, end, steps, copies): yield pt
                for pt in single_linspace(end, start, steps, copies): yield pt

        ch_loops = []
        for start, end, order in controls:
            steps = self.dimensions[order]
            copies = prod([r+1 for r in self.dimensions[:order]])
            if order == max_order:
                ch_loop = single_linspace(start, end, steps, copies)
            else:
                ch_loop = dual_linspace(start, end, steps, copies)
            ch_loops.append(ch_loop)

        points = zip(*ch_loops)

        # Save the generator
        self.points = enumerate(points)

        # Save a copy of the final list of independent channels.
        self.xchs = [x.ch for x in self.getch()]

        # Start the experiment
        self.start_time = datetime.datetime.now()
        platypus.log(self.runname, "Started")

        # Periods at which to send signals.
        self.periods = [math.prod([x+1 for x in self.dimensions[:i]]) for i in range(1,len(self.dimensions))]

        platypus.base.StandardExperiment.start(self)

import platypus
import threading
import numpy as np

try:
    from scipy.optimize import OptimizeResult
except:
    class OptimizeResult(dict):
        """ Represents the optimization result.

        Parameters
        ----------
        x : ndarray
            The solution of the optimization.
        success : bool
            Whether or not the optimizer exited successfully.
        status : int
            Termination status of the optimizer. Its value depends on the
            underlying solver. Refer to `message` for details.
        message : str
            Description of the cause of the termination.
        fun, jac, hess, hess_inv : ndarray
            Values of objective function, Jacobian, Hessian or its inverse (if
            available). The Hessians may be approximations, see the documentation
            of the function in question.
        nfev, njev, nhev : int
            Number of evaluations of the objective functions and of its
            Jacobian and Hessian.
        nit : int
            Number of iterations performed by the optimizer.
        maxcv : float
            The maximum constraint violation.
        Notes
        -----
        There may be additional attributes not listed above depending of the
        specific solver. Since this class is essentially a subclass of dict
        with attribute accessors, one can see which attributes are available
        using the `keys()` method.
        """
        def __getattr__(self, name):
            try:
                return self[name]
            except KeyError:
                raise AttributeError(name)

        __setattr__ = dict.__setitem__
        __delattr__ = dict.__delitem__

        def __repr__(self):
            if self.keys():
                m = max(map(len, list(self.keys()))) + 1
                return '\n'.join([k.rjust(m) + ': ' + repr(v)
                                  for k, v in self.items()])
            else:
                return self.__class__.__name__ + "()"

class SPSAMinimizer(platypus.base.Channel):
    def __init__(self, func, x0=[], bounds=[], niter=100, args=(), kwargs={}, name="xk", a=1.0, c=1.0):
        super().setname(name)
        self.stop_event = self.stop_event = threading.Event()
        self.setup(func, x0, bounds, niter, args, kwargs, a, c)
    def set(self, x): self.x0 = x
    def getvalue(self): return self.x0
    def gettype(self): return 'm'
    def stop(self): self.stop_event.set()

    def setup(self, func=None, x0=None, bounds=None, niter=None, args=None, kwargs=None, a=None, c=None):
        self.func   = func   if func   is not None else self.func
        self.x0     = x0     if x0     is not None else self.x0
        self.bounds = bounds if bounds is not None else self.bounds
        self.niter  = niter  if niter  is not None else self.niter
        self.args   = args   if args   is not None else self.args
        self.kwargs = kwargs if kwargs is not None else self.kwargs
        self.a      = a      if a      is not None else self.a
        self.c      = c      if c      is not None else self.c

    def optimize(self, func=None, x0=None, bounds=None, niter=None, args=None, kwargs=None, a=None, c=None):
        # Clear the stop flag.
        self.stop_event.clear()
        platypus.events.append(self.stop_event)

        # Get default arguments for unspecified functions.
        if func is None: func = self.func
        if x0 is None: x0 = self.x0
        if bounds is None: bounds = self.bounds
        if niter is None: niter = self.niter
        if args is None: args = self.args
        if kwargs is None: kwargs = self.kwargs
        if a is None: a = self.a
        if c is None: c = self.c

        # If func is a channel, optimize its "read" method instead.
        func = func.read if isinstance(func, platypus.base.Channel) else func

        # If x0 is a channel, read it.
        x0 = x0.read() if isinstance(x0, platypus.base.Channel) else x0
        # Read any channels in x0
        x0 = [x.read() if isinstance(x, platypus.base.Channel) else x for x in x0]

        # Fixed parameters of the SPSA algorithm.
        A = 0.01 * niter
        alpha = 0.602
        gamma = 0.101

        message = None

        # Create a clipping function.
        if not bounds:
            # No bounds. Pass through.
            clip = lambda x: x
        else:
            assert(len(bounds) == len(x0))
            # No bounds. Pass through.
            bounds = np.asarray(bounds)
            clip = lambda x: np.clip(x, bounds[:, 0], bounds[:, 1])

        # Do the optimization
        N = len(x0)
        x = x0
        for k in range(niter):
            ak = a/(k+1.0+A)**alpha
            ck = c/(k+1.0)**gamma
            delta = np.random.choice([-1, 1], size=N)

            # Clip evaluation points if necessary.
            xplus = clip(x + ck*delta)
            xminus = clip(x - ck*delta)

            # Calculate gradient.
            grad = (func(xplus, *args, **kwargs) - func(xminus, *args, **kwargs)) / (xplus-xminus)

            # Descend gradient, decreasing the step each time to guarantee convergence.
            x = clip(x - ak*grad)
            if self.stop_event.is_set():
                message = 'terminated by STOP event'
                break
        platypus.events.remove(self.stop_event)
        if message is None: message = 'terminated after reaching max number of iterations'
        return OptimizeResult(fun=func(x, *args, **kwargs), x=x, nit=niter, nfev=2*niter, message=message)

#!python
'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The names of the authors and contributors may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import os.path
import code
import sys
import datetime
import threading
from io import StringIO
import platypus

class Tee(object):
    '''
        Splits the standard program input and output streams (console I/O) and sends a copy to a log file.
        Implements file stream methods.
    '''
    def openlog(self):
        # Create a log in the default data directory with today's date if it doesn't exist.
        date = datetime.date.today()
        logpath = os.path.join(platypus.datadir, "logs")
        if not os.path.exists(platypus.datadir): os.mkdir(platypus.datadir)
        if not os.path.exists(logpath): os.mkdir(logpath)
        return open(os.path.join(logpath, "{}_{}_{}.log".format(date.year, date.month, date.day)), "a")

    def __init__(self, log=None):
        self.log = log if log else self.openlog()

    def flush(self):
        # Implements the flush method to emulate the REAL standard i/o streams.
        pass

    def write(self, data):
        '''
            Write a copy of the input to stdout and the log file.
            If lines begin with "__" this is stripped and they are NOT logged.
	    If lines begin with "#" (comment) write to the logbook(s).
        '''
        if data[:2] == "__":
            sys.__stdout__.write(data[2:])
            sys.__stdout__.flush()
        else:
            self.log.write(data)
            self.log.flush()
            sys.__stdout__.write(data)
            sys.__stdout__.flush()

    def readline(self):
        '''
            Write a copy of the input to stdio and the log file.
        '''
        s = sys.__stdin__.readline()
        sys.__stdin__.flush()
        self.log.write(s)
        self.log.flush()
        return s

class PlatypusConsole(code.InteractiveConsole):
    def push(self, line):
        d = line.split('#')
        if len(d)>1:
          platypus.log('"' + '#'.join(d[1:]).strip() + '"')
        super().push(line)
    def runcode(self, code):
        super().runcode(code)
        platypus.log()

def main():
    '''
        Threaded function that creates a fake console in a non-main thread.
        This gets around the problem that the standard python REPL and Tkinter GUI both want to run in the "main" thread.
        Also creates a "Tee" object to log all console I/O to help with errors.
    '''
    # Copy i/o to a memory buffer.
    buffer = StringIO()
    sys.stdout = sys.stderr = sys.stdin = Tee(buffer)
    # Session start banner.
    sys.stdout.write("\n~~~New Session: {}~~~\n\n".format(datetime.datetime.now().isoformat()))
    # Create the fake console.
    console = PlatypusConsole()

    # Create a logbook in the default data directory with today's date if it doesn't exist.
    platypus.openlogbook()
    platypus.log(reset=True)

    try:
        # Is there a config file to run? Run it.
        with open("config.py") as f:
            config = compile(f.read(), "config.py", 'exec')
        console.runcode(config)
    except FileNotFoundError:
        # Couldn't find config.py? Don't run it, then.
        pass
    except: # Anything else
        buffer = buffer.read()
        tee = sys.stdout = sys.stderr = sys.stdin = Tee()
        tee.log.write(buffer)
        raise

    buffer = buffer.read()
    tee = sys.stdout = sys.stderr = sys.stdin = Tee()
    tee.log.write(buffer)

    try:
        # Start the fake console.
        console.interact()
    except SystemExit:
        # Stop logging and mark the platypus main loop for death.
        sys.stdout.log.close()
        platypus.exit()

# Start the non-main thread with the fake console.
repl_thread = threading.Thread(target = main)
#repl_thread.start() # Note: it's not a daemon, so the program won't close until the repl closes.

# Run the platypus main loop, and TK-based GUI, in the main thread.
platypus.mainloop(repl_thread)

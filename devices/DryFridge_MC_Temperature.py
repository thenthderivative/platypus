# -*- coding: utf-8 -*-
"""
Created on March 18 2016

@author: GaudreauL
"""

import platypus
from platypus import base
import pyvisa
import pyvisa
import os
import datetime


class DryFridge_MC_Temperature(base.Channel):
    '''
    Documentation:
    This module is used to get the mixing chamber temperature of the dry fridge.
    It requires the folder name where the dryfridge logs are,
    for example: '//NRC-000819/Users/dryfridge194c/Desktop/Logs'
    Input:
    name (str)
    log_folder (str)
    '''
    def __init__(self, name, log_folder, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided name and parameters for this channel
        self.setname(name)

        # Pass any parameters to this channel
        self.log_folder = log_folder
        
        #Create any required initialising parameters
        self.MC_temperature = 0.0

    def validate(self):
        #No validation required
        return True

    def stop(self):pass #No stop required

    def read(self):
        '''
        Documentation:
        Read the mixing chamber temperature of the dry fridge by
        looking at the last entry in the log file.
        Note: the logfile in the log folder for mixing chamber is 'CH6 T'.
        Input:
        None
        Return:
        MC_temperature (float)
        '''
        #Get today's date
        today = datetime.datetime.today()
        #next line required to format date as yyyy-mm-dd
        date = "{:02}-{:02}-{:02}".format(today.year, today.month, today.day)
        #change to yy-mm-dd to match format of log files
        date = date[2:]
        #create the path to today's log file (self.log_folder defined in __init__
        log_file_path = self.log_folder+'/'+date+'/CH6 T '+date+'.log'

        
        #We need to beware that the log file might not yet be created in the
        #dryfridge computer, if that happens we use yesterdays date
        try:
            #open the log file in reading mode
            file = open(log_file_path,'r')
        except FileNotFoundError:
            #Get yesterday's date
            yesterday = datetime.datetime.today()-datetime.timedelta(days=1)
            #next line required to format date as yyyy-mm-dd
            date = "{:02}-{:02}-{:02}".format(yesterday.year, yesterday.month, yesterday.day)
            #change to yy-mm-dd to match format of log files
            date = date[2:]
            #create the path to today's log file (self.log_folder defined in __init__
            log_file_path = self.log_folder+'/'+date+'/CH6 T '+date+'.log'
            #open the log file in reading mode
            file = open(log_file_path,'r')
        
        #create a list of all the lines in the file
        list_of_temperatrures = file.readlines()
        #Take the last line containing the latest temperature reading
        self.MC_temperature = list_of_temperatrures[-1]
        #Extract the temperature from the last line
        self.MC_temperature = float(self.MC_temperature[19:30])
        platypus.update()
        return self.MC_temperature
        


    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.MC_temperature


    def gettype(self):
        #This is a measurement channel, so the string 'c' is returned.
        return 'm'

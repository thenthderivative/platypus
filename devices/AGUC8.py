# -*- coding: utf-8 -*-
"""
Created on Tue May 12 12:01:47 2015

@author: Phoenixj
modified by Louis Gaudreau
"""

from platypus import base
import time
import pyvisa
import pyvisa

class AGUC8(base.Channel):
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided name for this channel
        self.setname(name)

        # Create the instrument
        rm = pyvisa.ResourceManager()
        lib = rm.visalib
        self.inst = rm.open_resource(address,
                baud_rate = 921600,
                data_bits = 8,
                stop_bits = pyvisa.constants.StopBits.one,
                parity=pyvisa.constants.Parity.none,
                read_termination = '\r',
                write_termination = '\r\n')
        #Set device to remote control mode.
        self.inst.write('MR')
        #Set step amplitude.
        self.inst.write('1SU50')
        #Set step delay time.
        self.inst.write('1DL0')
        #Delay time between steps is 10 us for setting 0.
        self.delay_time = 10*10E-6
        #The approximate number of steps required to rotate the device by one
        #degree, as determined through calibration tests, is declared here.
        self.steps_per_degree = 529
        self.relative_position = 0

    def validate(self):
        '''
        Documentation:
        No validation required. Always return True.
        Input:
        None
        Return:
        valid (boolean)
        '''
        #No validation required
        return True

        
    def set(self, new_position):
        '''
        Documentation:
        Sets the rotator to an angle position
        Input:
        new_position in degrees (float)
        Return:
        None
        '''
        if self.relative_position == 360:
            self.relative_position = 0
    
        if new_position != self.relative_position:
            angle = new_position - self.relative_position
            #The total number of steps needed to rotate the device to the desired
            #angle is calculated.
            total_steps = angle*self.steps_per_degree
            self.inst.write('1PR'+str(total_steps))
    
            #Wait for step time to expire, then continuously querry whether device
            #is in motion before continuing program.
            time.sleep(abs(total_steps)*self.delay_time)
            proceed = 0
            while proceed == 0:
                proceed = int(self.inst.query('1TS')[-1])
                time.sleep(0.05)
                
            self.relative_position = new_position
            
        else: print('Device already at ' +str(new_position)+' degrees.')
        

    def stop(self):
        '''
        Documentation:
        The rotator is instructed to cease all movement.
        Input:
        None
        Return:
        None
        '''
        #The rotator is instructed to cease all movement.
        self.inst.write('1ST')

        
    def read(self):
        '''
        Documentation:
        Gets the current value of the rotator.
        Input:
        None
        Return:
        Last set position (float)
        '''
        #The device does not have absolute position values, so there is
        #nothing to read. Return the last set position.
        return self.relative_position

    
    def getvalue(self):
        '''
        Documentation:
        Gets the current value of the rotator.
        Input:
        None
        Return:
        Last set position (float)
        '''
        return self.relative_position

    
    def gettype(self):
        #This is a control channel, so the string 'c' is returned.
        return 'c'

# -*- coding: utf-8 -*-
"""
Created on May 09 2017

@author: Jason Phoenix
"""

import platypus
import time
import pyvisa
import pyvisa

#Give a name to the device in the next line
class Keithley2010(platypus.base.Channel):
    
    '''
    Documentation
    Controls the Keithley 2010 multimeter..
    
    Required Inputs: (name,address)
    '''    
    
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided channel name for this instrument
        self.setname(name)

        # Create the instrument. Ensure that baud rate, data bits, and other
        # settings are correct for the instrument being used.
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address)
##                baud_rate = 9600,
##                data_bits = 8,
##                stop_bits = pyvisa.constants.StopBits.one,
##                parity=pyvisa.constants.Parity.none,
##                read_termination = '\r\n',
##                write_termination = '\r\n')

        #Initialize DMM so that voltage measurements may be made.
        self.inst.write(':SENS:FUNC "VOLT"') #Volts measure function.
        self.inst.write(':SENS:VOLT:RANGE:AUTO OFF')
        self.inst.write(':SENS:VOLT:RANG 10') #Range options: 0.1, 1, 10, 100, 1000 volts
        self.inst.write(':INIT:CONT OFF') #Turn off contiunous triggering so :READ? can be used
        #self.inst.write(':FORM:ELEM VDC') #Volts only.
        self.inst.write(':SYST:BEEP:STAT OFF')

        self.inst.timeout = 10000

        #Initialize the DMM source by reading its value
        self.voltage = self.read()
        
    def validate(self):
        #No validation required
        return True
        
        
    def stop(self): pass
        #No stop required
    
    
    def read(self):
        '''
        Reads the voltage of the source
        Input:
        None
        Return:
        voltage in V (float)
        '''
        self.voltage = float(self.inst.query(':READ?'))
        platypus.update(self)
        return self.voltage
         
    def volt_range(self,range):
        '''
        Sets the voltage range of the kiethley 2010.
        Input: range in volts (float, 0 to 1010)
        Return: None
        '''
        #Set voltage range.
        self.inst.write(':SENS:VOLT:RANG '+str(range))

    def filter_on(self,avg_count=1,PLC=1):
        '''
        Turn on filtering of kiethley 2010 measurements.
        Input: number of measurements to average (int, between 1 and 100)
        Optional Input: NPLC (float, 0.01 to 10)
        Return: None
        '''

        self.inst.write(':SENS:VOLT:NPLC '+str(PLC)) # Set volts speed (PLC = 0.01 to 10).
        self.inst.write(':SENS:VOLT:AVER:TCON REP') # Select filter type (type = REPeat or MOVing).
        self.inst.write(':SENS:VOLT:AVER:COUN '+str(int(avg_count))) # Set filter count (avg_count = 1 to 100).
        if avg_count>1:
            self.inst.write(':SENS:VOLT:AVER:STAT ON') # Enable filter.
        else:
            self.inst.write(':SENS:VOLT:AVER:STAT OFF') # Disable filter.

    def filter_off(self):
        self.inst.write(':SENS:VOLT:NPLC 0.01')
        self.inst.write(':SENS:VOLT:AVER:STAT OFF') # Disable filter.
    
    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.voltage
        
        
    def gettype(self):
        #Measurement channel, so 'm' is returned.
        return 'm'

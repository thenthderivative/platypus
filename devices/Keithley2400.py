# -*- coding: utf-8 -*-
"""
Created on September 01 2016

@author: Louis Gaudreau
@author: Jason Phoenix
"""

import platypus
import time
import pyvisa
import pyvisa

#Give a name to the device in the next line
class Keithley2400(platypus.base.Channel):
    
    '''
    Documentation
    Controls the Keithley 2400 Source meter. Used as DMM.
    
    Required Inputs: (name,address)
    '''    
    
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided channel name for this instrument
        self.setname(name)

        # Create the instrument. Ensure that baud rate, data bits, and other
        # settings are correct for the instrument being used.
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                baud_rate = 9600,
                data_bits = 8,
                stop_bits = pyvisa.constants.StopBits.one,
                parity=pyvisa.constants.Parity.none,
                read_termination = '\r\n',
                write_termination = '\r\n')

        self.inst.timeout = 10000

        #Initialize DMM so that voltage measurements may be made.
        self.inst.write(':SOUR:FUNC CURR') #Current source function.
        self.inst.write(':SOUR:CURR:MODE FIXED') #Fixed current source mode.
        self.inst.write(':SENS:FUNC "VOLT"') #Volts measure function.
        #self.inst.write(':SOUR:CURR:RANG MIN') #Lowest source range.
        self.inst.write(':SOUR:CURR:LEV 0') #0μA source level.
        self.inst.write(':SENS:VOLT:PROT 25') #25V compliance.
        self.inst.write(':SENS:VOLT:RANG 20') #20V range.
        self.inst.write(':FORM:ELEM VOLT') #Volts only.

        #Turn on the voltage source
        self.source_on()

        #Initialize the DMM source by reading its value
        self.voltage = self.read()

    def validate(self):
        #No validation required
        return True
        
        
    def stop(self): pass
        #No stop required
    
    
    def read(self):
        '''
        Reads the voltage of the source
        Input:
        None
        Return:
        voltage in V (float)
        '''
        self.inst.write(':READ?')
        self.voltage = float(self.inst.read())
        platypus.update()
        return self.voltage

    def source_on(self):
        '''
        Turns the voltage source of the keithley 2400 on.
        Input: None
        Return: None
        '''
        #Output1 is the voltage source, 0=off, 1=on
        self.inst.write(':OUTP ON')

    def source_off(self):
        '''
        Turns the voltage source of the keithley 2400 off.
        Input: None
        Return: None
        '''
        #Output1 is the voltage source, 0=off, 1=on
        self.inst.write(':OUTP OFF')            
    def volt_range(self,range):
        '''
        Sets the voltage range of the kiethley 2400.
        Input: range in volts (0.2, 2, 20, or 200)
        Return: None
        '''
        #Set voltage range.
        self.inst.write(':SENS:VOLT:RANG '+str(range))

    def filter_on(self,avg_count,PLC=1):
        '''
        Turn on filtering of kiethley 2400 measurements.
        Input: number of measurements to average (int)
        Optional Input: PLC (float)
        Return: None
        '''
        
            
        self.inst.write(':SENS:CURR:NPLC '+str(PLC)) # Set volts speed (PLC = 0.01 to 10).
        self.inst.write(':SENS:AVER:TCON REP') # Select filter type (type = REPeat or MOVing).
        self.inst.write(':SENS:AVER:COUN '+str(int(avg_count))) # Set filter count (avg_count = 1 to 100).
        self.inst.write(':SENS:AVER ON') # Enable filter.

    def filter_off(self):
        self.inst.write(':SENS:CURR:NPLC 0.01')
        self.inst.write(':SENS:AVER OFF') # Disable filter.
    
    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.voltage
        
        
    def gettype(self):
        #Measurement channel, so 'm' is returned.
        return 'm'

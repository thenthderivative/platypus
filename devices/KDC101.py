# -*- coding: utf-8 -*-
"""
Created on Wed OCt 21 10:27:47 2015

@author: GaudreauL
"""

import platypus 
import pyvisa
import pyvisa
import struct
import time


class KDC101(platypus.base.Channel):
    '''
    Documentation:
    Controls the KDC101 controller for the Thorlabs rotators.
    Note: If the device is not on a COM port in the device manager, remember
    to look for it as APT USB Device, then select 'Load VCP' in the
    properties, then restart the computer. It should then appear in the
    COM ports list.

    Required Inputs:
        name (string)
        address (string)

    Optional Inputs:
        reverse (True/False, default = False)

    If True, 'reverse' will cause rotator to behave as if it is oriented backwards,
    so angles will be reversed with respect to 360, i.e. 10 becomes 350, 20
    becomes 340, etc.
    '''



    def __init__(self, name, address, *args, reverse = False, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided name for this channel
        self.setname(name)

        # Create the instrument
        self.rm = pyvisa.ResourceManager()
        self.lib = self.rm.visalib
        self.inst = self.rm.open_resource(address,
                baud_rate = 115200,
                data_bits = 8,
                stop_bits = pyvisa.constants.StopBits.one,
                parity=pyvisa.constants.Parity.none,
                read_termination = '',
                write_termination = '',
                timeout = 100000)#This timeout is needed to let the rotator do a full 360 deg rotation


	#Multiplying factor to set the angle of rotation in degrees of the PRM1Z8E rotator.
	#This value is found in the communication protocol manual located in
	#the PRM1Z8E Instrument folder
        self.multiplying_factor = 1919.64
        
        self.reverse = reverse

        #The device must be homed upon initialization
        self.home()

        self.angle = self.read()

    def validate(self, angle):
        return True

    def wait(self):
        warn = self.lib.issue_warning_on
        self.lib.issue_warning_on = []
        self.lib.read(self.inst.session, 20)#20 is the number of bytes to read (taken from the communication protocol manual)
        self.lib.issue_warning_on = warn

    def set(self, new_angle):
        '''
        Set the angle of the rotator.
        Input:
        new_angle in deg (float, between 0 and 360 inclusive)
        '''
        if 0 <= new_angle <= 360:

            #If rotator is oriented backwards, reverse actual position angle
            #without altering value of angle you get when making a read
            #measurement.
            if self.reverse == True:
                set_angle = 360-new_angle
            else:
                set_angle = new_angle
            
            #This instrument uses bytes in hexadecimal strings (not ascii) so that is why we need the
            #'write.raw(b"\xblababla)' command. Programming this device can be nasty. It is very 
            #important to understand the communication protocol located in the PRM1Z8E Instrument folder.
            #Remember to multiply the angle in degrees by the multiplying factor.
            self.inst.write_raw(b"\x50\x04\x06\x00\xd0\x01\x01\x00" + struct.pack("<I", int(set_angle*self.multiplying_factor))) #MGMSG_MOT_MOVE_ABSOLUTE in the communication protocol

            self.inst.write_raw(b"\x53\x04\x01\x00\x50\x01")
            #we now need to wait for the rotator to reach the angle. To do this, we send the appropriate
            #read command and wait for the answer. The lines containing the warn variable are used to set
            #warnings from pyvisa off and then on again. This is to avoid a warning that appears when the
            #instrument sends the 'completed movement' 20 byte hexadecimal string message.
            self.wait()
            
            #print(0.1073*abs(self.angle-new_angle))
            #time.sleep(0.109*abs(self.angle-new_angle))

            
            self.angle=new_angle

            #Update platypus so that the new value appears on the state monitor
            platypus.update()
            
        else:
            print('')
            print('ERROR: INVALID ANGLE')
            print('Angle values must be between 0 and 360.')
            print('')


    def home(self):
        '''
        Set the angle of the rotator to 0 deg.
        Input: None
        Return: None
        '''
	#This instrument uses bytes in hexadecimal strings (not ascii).Programming 
        #this device can be nasty. It is very important to understand the 
        #communication protocol located in the PRM1Z8E Instrument folder.

        print('')
        print('KDC101 Rotator Moving to Home Position...')
        print('')
        
        self.inst.write("\x43\x04\x01\x00\x50\x01") #MGMSG_MOT_MOVE_HOME in the communication protocol

        self.angle=0

        #we now need to wait for the rotator to reach the angle. To do this, we send the appropriate
        #read command and wait for the answer. The lines containing the warn variable are used to set
        #warnings from pyvisa off and then on again. This is to avoid a warning that appears when the
        #instrument sends the 'completed movement' 20 byte hexadecimal string message.
        warn = self.lib.issue_warning_on
        self.lib.issue_warning_on = []
        
        data, status = self.lib.read(self.inst.session, 6)#6 is the number of bytes to read (taken from the communication protocol manual)
        
        self.lib.issue_warning_on = warn

        #This is required because the self.read() function specifically returns the last value which
        #was input into the device. The 'home' function does not change this value, so upon startup,
        #the device resets to position zero, but self.read() will still return whatever value was
        #last used in self.set().
        #self.set(0)
        
        #Update platypus so that the new value appears on the state monitor
        platypus.update()


    def stop(self):pass

    def read(self):
        '''
        Reads the last value set and stored in memory of the TDC001.
        Input: None
        Return:
        angle in deg (float)
        '''
        
        return self.angle


    def getvalue(self):
        #Returns the last set value stored in memory.
        return self.angle

    def gettype(self):
        #This is a control channel, so the string 'c' is returned.
        return 'c'

# -*- coding: utf-8 -*-
"""
Created on March 21st 2016

@author: louis gaudreau
@author: Jason Phoenix
"""

from platypus import base
import pyvisa
import pyvisa
import time

class Pathway7334(object):
    
    '''
    Controls the elctronic switchers.
    Input:
    address (str)

    '''    
    
    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)


        # Create the instrument
        rm = pyvisa.ResourceManager()
        lib = rm.visalib
        self.inst = rm.open_resource(address,
                baud_rate = 9600,
                data_bits = 8,
                stop_bits = pyvisa.constants.StopBits.one,
                parity=pyvisa.constants.Parity.none,
                read_termination = '\r\n',
                write_termination = '\r\n')

        self.active_port = 'E'

    def validate(self):
        return True
    
    def set_port(self, port):
        '''
        Sets the port of the  electronic switch.       
        Input:
        port letter (str): 'A' or 'B' or 'C', etc.
        Return:
        None       
        '''

        #Sets the port of the switch
        port = str(port)
        if (port == 'A'): self.inst.write('\x01')
        elif (port == 'B'): self.inst.write('\x02')
        elif (port == 'C'): self.inst.write('\x03')
        elif (port == 'D'): self.inst.write('\x04')
        elif (port == 'E'): self.inst.write('\x05')
        self.active_port = port
 

    def get_port(self):
        '''
        Gets the active port of the  electronic switch.        
        Input:
        None
        Return:
        Active port (str).
        '''
        return self.active_port


# -*- coding: utf-8 -*-
"""
Created on Thu May 14 09:34:02 2015

@author: Phoenixj
Modified by Louis Gaudreau
"""

import platypus
import pyvisa

class PM100D(platypus.base.Channel):
    '''
    Documentation:
    Controls the PMD100D powermeter.
    
    Inputs:
        name (str)
        address (str)
    '''
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)
        # Use the provided name for this channel
        self.setname(name)
        # Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address)
        #Declare power variable
        self.power = 0.0

    def validate(self, value):
        return True #No validation required

    def stop(self):pass #No stop required
        
    def read(self):
        '''
        Documentation:
        Retrieve the power reading from the powermeter.
        Input:
        None
        Return:
        power in Watts (float)
        '''
        #Retrieve the power reading from the device
        self.power = float(self.inst.query("read?"))
        #Update the state monitor
        platypus.update()
        return self.getvalue()
        
    def getvalue(self):
        #Return last power value saved in memory
        return self.power
        
    def gettype(self):
        #This is a measurment channel, so the type 'm' is returned.
        return 'm'

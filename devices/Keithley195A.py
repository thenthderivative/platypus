import platypus
import pyvisa
import pyvisa

class Keithley195A(platypus.base.Channel):

    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided name for this channel
        self.setname(name)

        # Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address)

        self.last_value = 0.0

    def validate(self, value):
        return True
    
    def set(self, v): pass
    
    def stop(self): pass

    def read(self):
        measurement = self.inst.read()
        measurement = float(measurement[4:-5])
        self.last_value = measurement
        platypus.update()
        return measurement
        
    def getvalue(self): return self.last_value

    def gettype(self): return 'm'

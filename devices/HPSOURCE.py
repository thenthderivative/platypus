'''

Copyright (c) 2019, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import platypus
import pyvisa,threading
import pyvisa
import time
from math import nan
import math

class HPSOURCE(platypus.base.Channel):
    def __init__(self, name, address, safestepsize=0.010, delay=0.1, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided name for this channel
        self.setname(name)

        # Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                                     read_termination='\r\n',
                                     write_termination='\r\n',
                                     timeout=60000)
        # Set parameters used keep operation safe.
        self.setsafestepsize(safestepsize)
        self.setdelay(delay)
        self.tlast = time.monotonic()-delay

        self.read()
    def validate(self, value): return True
    def getvalue(self): return self.v
    def set(self, V):
        with self.lock:
            self.stopped = False
            if math.isclose(V, self.getvalue()): return
        while True:
            if self.stopped or self.steptowards(V): break
            platypus.update(self)
            self.wait()
        platypus.update(self)
    def steptowards(self, V):
      with self.lock:
        if not self.validate(V):
            print("Target voltage invalid")
            raise ValueError
        if not self.validate(self.getvalue()):
            print("Current voltage invalid! Change or do a read() first.")
            raise ValueError
        def sign(v): return 1.0 if v >= 0 else -1.0
        Vold = self.getvalue()
        s = sign(V - Vold) # Sign of voltage difference.
        d = abs(V - Vold)  # Magnitude of voltage difference.
        doneness = True # Did we reach the voltage this step?
        # Don't try to make a bigger step than 'safestepsize'
        if d > self.safestepsize:
            d = self.safestepsize
            doneness = False
        self.v = Vold + s*d
        self.inst.write("APPLY DCV {:E}".format(self.v))
        self.tlast = time.monotonic() # Update last set operation
        return doneness
    def stop(self):
      with self.lock:
        self.stopped = True
    # Read the value of the channel (may involve a hardware read).
    def read(self):
      with self.lock:
        self.inst.write("OUTPUT?")
        time.sleep(0.05)
        msg = self.inst.read().strip()
        self.v = float(msg)
        self.triggered = False
        platypus.update()
        return self.getvalue()
    # Sets the maximum safe step size (in V).
    def setsafestepsize(self, safestepsize):
      with self.lock:
        self.safestepsize = abs(safestepsize)
    def getsafestepsize(self):
      with self.lock:
        return self.safestepsize
    # Sets the delay time (in seconds) between operations.
    def setdelay(self, delay):
      with self.lock:
        self.delay = abs(delay)
    def getdelay(self):
      with self.lock:
        return self.delay
    # Returns once 'delay' seconds have elapsed since the last
    # voltage change operation through this channel.
    # If 'delay' has already elapsed, returns immediately.
    def wait(self):
      with self.lock:
        waittime = self.tlast - time.monotonic() + self.delay
#        print(self.tlast, time.monotonic(), self.delay, waittime)
        if  waittime > 0.0001:
#            print("W!")
            time.sleep(waittime)
     # Could be useful. Read on request for now.
    def getvalue(self): return self.v
    def gettype(self): return 'c'

'''

Copyright (c) 2019, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import platypus
import pyvisa
import pyvisa
import time
from math import isclose, inf
import math
import numpy as np
from numpy import linspace

import threading

class SR124Base(platypus.base.Channel):
    def __init__(self, parent, name, limits=(-inf, inf), safestepsize=0.1, delay=0.001):
        self.setname(name)
        self.parent = parent
        self.limits = limits
        self.v = 0.0
        
        self.setsafestepsize(safestepsize)
        self.setdelay(delay)
        self.tlast = time.monotonic()- delay

    def validate(self, v):
        low, high = min(self.limits), max(self.limits)
        return (v >= low and v <= high)
    
    def stop(self):
        # Halt all processes where they are.
        self.stopped = True
    def getvalue(self): return self.v
    def gettype(self): return 'c'
    def setsafestepsize(self, safestepsize):
        self.safestepsize = abs(safestepsize)
    def setdelay(self, delay):
        self.delay = abs(delay)
    def getdelay(self):
        return self.delay
    def wait(self):
        # Returns once 'delay' seconds have elapsed since the last
        # voltage change operation through this channel.
        # If 'delay' has already elapsed, returns immediately.
        
        waittime = self.tlast - time.monotonic() + self.delay
#       print(self.tlast, time.monotonic(), self.delay, waittime)
        if waittime > 0.0001:
#           print("W!")
            time.sleep(waittime)

'''
class SR124Output(SR124Base):
    def __init__(self, parent, name, limits=(-inf, inf)):
        self.setname(name)
        self.parent = parent
        self.limits = limits
        self.v = 0.0
        
        self.n = name
        self.unit = self.getunit()
        self.setname(str(name)+" ("+str(self.unit)+")")
    
    def validate(self, v):
        return (v >= -10.0 and v <= 10.0)
    def SetName(self):
        self.unit = self.getunit()
        self.setname(str(self.n)+" ("+str(self.unit)+")")
    def getunit(self):
        source = self.parent.query_input_source()
        unit = ""
        if (source == 0 or source == 1): unit = "V"
        elif (source == 2 or source == 3): unit = "A"
        return unit
    def read(self):
        self.SetName()
        
        factor = 1
        source = self.parent.query_input_source()
        if (source == 2): factor = 1E-6
        if (source == 3): factor = 1E-8
        
        self.v = self.parent.output_RTI() * factor
        platypus.update()
        return self.getvalue()
    def gettype(self): return 'm'
    
    
class SR124InputSensitivity(SR124Base):
    def read(self):
        self.v = self.parent.query_input_sens()
        platypus.update()
        return self.getvalue()
'''

class SR124Output(SR124Base):
    PHASE = "TI"
    def __init__(self, parent, name, limits=(-inf, inf)):
        self.setname(name)
        self.parent = parent
        self.limits = limits
        self.v = 0.0

        self.n = name
        self.unit = self.getunit()
        self.setname(str(name)+" ("+str(self.unit)+")")
    
    def validate(self, v):
        return (v >= -10.0 and v <= 10.0)
    def getunit(self):
        source = self.parent.query_input_source()
        unit = ""
        if (source == 0 or source == 1): unit = "V"
        elif (source == 2 or source == 3): unit = "A"
        return unit
    def read(self):
        factor = 1
        source = self.parent.query_input_source()
        if (source == 2): factor = 1E-6
        if (source == 3): factor = 1E-8
        
        self.v = self.parent.output_RTI(self.PHASE) * factor
        platypus.update(self)
        return self.getvalue()
    def gettype(self): return 'm'

class SR124OutputX(SR124Output):
    PHASE = "IX"
class SR124OutputY(SR124Output):
    PHASE = "IY"
    
class SR124InputSensitivity(SR124Base):
    def read(self):
        self.v = self.parent.query_input_sens()
        platypus.update(self)
        return self.getvalue()
    def set(self, sens):
        self.parent.set_input_sens(sens)
        platypus.update(self)


class SR124Freq(SR124Base):
    def validate(self, v):
        setting = self.getrange()
        if (setting == 0): Range = [0.2, 21]
        elif (setting == 1): Range = [2, 210]
        elif (setting == 2): Range = [20, 2100]
        elif (setting == 3): Range = [200, 21000]
        elif (setting == 4): Range = [2000, 210000]
        
        return (v >= Range[0] and v <= Range[1])
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_frequency(v)
        else:
            raise ValueError("Invalid Frequency")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_frequency()
        platypus.update(self)
        return self.getvalue()
        
    def setrange(self, setting):
        '''setting must be = 0,1,2,3,4'''
        if (setting != 0 and setting != 1 and setting != 2 \
        and setting != 3 and setting != 4):
            raise ValueError("Invalid Frequency Range")
        self.parent.set_range(setting)    
    def getrange(self):
        return self.parent.get_range()
        
    def measurefreq(self):
        self.parent.measure_frequency()


class SR124Amplitude(SR124Base):
    def validate(self, v):
        #dcbias = self.parent.get_bias()
        #if (dcbias <= -0.1 or dcbias >= 0.1):        
        return (v >= 0.00000010 and v <= 10.00) and super().validate(v)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_amplitude(v)
        else:
            raise ValueError("Invalid Amplitude")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_amplitude()
        platypus.update(self)
        return self.getvalue()

class SR124Phase(SR124Base):
    def validate(self, v):
        return (v >= 0.0 and v <= 360.0)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_phase(v)
        else:
            raise ValueError("Invalid Phase")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_phase()
        platypus.update(self)
        return self.getvalue()
    def setquad(self, quad):
        '''quad must be = 1,2,3,4'''
        self.parent.set_phase_quadrant(quad)

class SR124Bias(SR124Base):
    def validate(self, v):
        ampl = self.parent.get_amplitude()
        if (ampl >= 0.0001 and ampl <= 0.00999):
            return (v >= -0.1 and v <= 0.1)
        else:
            return (v >= -10 and v <= 10)
    
    def set(self, V):
        # Steps in 'safestepsize' steps, waiting 'delay'
        # between steps, until the voltage reaches V.
        
        self.stopped = False
        if math.isclose(V, self.getvalue()): return
        while True:
            if self.stopped or self.steptowards(V): break
            platypus.update(self)
            self.wait()
        platypus.update(self)
    def steptowards(self, V):
        # Take a single step toward the target voltage.
        
        if not self.validate(V):
            print("Target DC voltage invalid")
            raise ValueError
        if not self.validate(self.getvalue()):
            print("Current DC voltage invalid! Change or do a read() first.")
            raise ValueError
        def sign(v): return 1.0 if v >= 0 else -1.0
        Vold = self.getvalue()
        s = sign(V - Vold) # Sign of voltage difference.
        d = abs(V - Vold)  # Magnitude of voltage difference.
        doneness = True # Did we reach the voltage this step?
        # Don't try to make a bigger step than 'safestepsize'
        if d > self.safestepsize:
            d = self.safestepsize
            doneness = False
        self.v = Vold + s*d
        self.parent.set_bias(round(self.v, 3))
        self.tlast = time.monotonic() # Update last set operation
        return doneness
    '''
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_bias(round(v, 3))
        else:
            raise ValueError("Invalid DC Bias")
        platypus.update(self)
    '''
    def read(self):
        self.v = self.parent.get_bias()
        platypus.update(self)
        return self.getvalue()
    def on(self):
        self.parent.bias_on()
    def off(self):
        self.parent.bias_off()

class SR124FilterFreq(SR124Base):
    def validate(self, v):
        return (v >= 2.0 and v <= 110000.0)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_filter_frequency(v)
        else:
            raise ValueError("Invalid Frequency")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_filter_frequency()
        platypus.update(self)
        return self.getvalue()

class SR124LockInAmplifier(object):
    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        #Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                                     write_termination="\n",
                                     read_termination="\n")

    def write(self, msg):
        self.inst.write(msg)
        return None

    def query(self, msg):
        self.inst.write(msg)
        return self.inst.read()

    def set_ref_mode(self, mode):
        '''Sets the reference mode'''
        msg = "FMOD {}".format(mode)
        self.write(msg)
        
    def get_ref_mode(self):
        '''Gets the reference mode'''
        msg = "FMOD?"
        return float(self.query(msg))

    def set_range(self, range):
        '''Sets reference oscillator frequency range'''
        '''Input:   0 --> 0.2 - 21 Hz'''
        '''         1 --> 2 - 210 Hz'''
        '''         2 --> 20 - 2100 Hz'''
        '''         3 --> 200 - 21000 Hz'''
        '''         4 --> 2000 - 210000 Hz'''
        msg = "FRNG {}".format(range)
        self.write(msg)

    def get_range(self):
        '''Gets reference oscillator frequency range (see above)'''
        msg = "FRNG?"
        return float(self.query(msg))

    def set_frequency(self, freq):
        '''Change the reference oscillator frequency (in Hz)'''
        msg = "FREQ {}".format(freq)
        self.write(msg)

    def get_frequency(self):
        '''Read the reference oscillator frequency (in Hz)'''
        '''If in Internal mode, reads commanded frequency'''
        '''If in External or rear-VCO mode, returns most recent measuerd value of frequency'''
        msg = "FREQ?"
        return float(self.query(msg))

    def measure_frequency(self):
        '''Initiate an oscillator referency frequency measurement cycle'''
        msg = "AREF 1"
        self.write(msg)

    def set_phase(self, phase):
        '''Change the reference phase (in degrees, from 0 to 360)'''
        msg = "PHAS {}".format(phase)
        self.write(msg)

    def get_phase(self):
        '''Read the reference phase (in degrees)'''
        msg = "PHAS?"
        return float(self.query(msg))
    
    def set_phase_quadrant(self, quad):
        '''Change the reference phase quadrant (ie. add multiples'''
        '''of 90 deg to bring phase into desired quadrant)'''
        '''quad must be 1, 2, 3, 4'''
        msg = "QUAD {}".format(quad)
        self.write(msg)

    def get_phase_quadrant(self):
        '''Read the reference phase quadrant'''
        msg = "QUAD?"
        return float(self.query(msg))
        

    def set_amplitude(self, amp):
        '''Change the reference oscillator output amplitude (in volts RMS)'''
        msg = "SLVL {}".format(amp)
        self.write(msg)

    def get_amplitude(self):
        '''Read the reference oscillator output amplitude (in volts RMS)'''
        msg = "SLVL?"
        return float(self.query(msg))
        
        
    def bias_on(self, status=True):
        '''Enable the reference output DC bias'''
        msg = "BION 1" if status else "BION 0"
        self.write(msg)
        
    def bias_off(self):
        '''Disable the reference output DC bias'''
        self.bias_on(False)
        
    def bias_state(self):
        '''Query the reference output DC bias state'''
        msg = "BION?"
        return float(self.query(msg))
        
    def set_bias(self, bias):
        '''Set the DC bias magnitude in Volts'''
        msg = "BIAS {}".format(bias)
        self.write(msg)
        
    def get_bias(self):
        '''Get the DC bias magnitude in Volts'''
        msg = "BIAS?"
        return float(self.query(msg))
        
        
    def set_filter_frequency(self, frequency):
        '''Set the input filter frequency in Hz'''
        msg = "IFFR {}".format(frequency)
        self.write(msg)
    
    def get_filter_frequency(self):
        '''Get the input filter frequency in Hz'''
        msg = "IFFR?"
        return float(self.query(msg))
    
    def query_input_sens(self):
        '''Query full-scale input sensitivity setting'''
        sensitivity = np.array(
            [100E-9,
            200E-9,
            500E-9,
            1E-6,
            2E-6,
            5E-6,
            10E-6,
            20E-6,
            50E-6,
            100E-6,
            200E-6,
            500E-6,
            1E-3,
            2E-3,
            5E-3,
            10E-3,
            20E-3,
            50E-3,
            100E-3,
            200E-3,
            500E-3], float)
            
        msg = "SENS?"
        back = self.query(msg)
        try: d = int(back)
        except: d = int(back[:-1])
        return float(sensitivity[d])
    
    def set_input_sens(self, sens):
        '''Set input sensitivity (ie. gain)'''
        '''Allowable inputs are 0 to 20, corresponding to instrument'''
        msg = "SENS {}".format(sens)
        self.write(msg)


    def set_input_source(self, source):
        '''Set the input sourec configuration'''
        ''' 'A' (voltage, 'AMINUSB' (differential voltage)'''
        ''' 'CUR1E6' (Current amplifier 1E6 V/A), 'CUR1E8' (Current amplifier 1E8 V/A) '''
        msg = "ISRC {}".format(source)
        self.write(msg)
        
    def query_input_source(self):
        '''Query the input sourec configuration'''
        ''' 'A' (voltage, 'AMINUSB' (differential voltage)'''
        ''' 'CUR1E6' (Current amplifier 1E6 V/A), 'CUR1E8' (Current amplifier 1E8 V/A) '''
        msg = "ISRC?"
        return int(self.query(msg))

    def output(self):
        '''Query the output value in Volts'''
        '''Always in the range -10 to +10'''
        msg = "OUTR?"
        return float(self.query(msg))
        
    def output_RTI(self, phase="I"):
        '''Query the output value in Volts referenced to input'''
        '''Follows ORTI? = (OUTP?/10V) * VFS'''
        msg = "OR{}?".format(phase)
        return float(self.query(msg))

    def F(self, name="SR124 Frequency (Hz)", limits=(-inf, inf)):
        '''
        Return a frequency control channel with 'name'
        specified as positional argument.
        Units are in GHz.
        '''
        return SR124Freq(self, name, limits)

    def A(self, name="SR124 Amplitude (V)", limits=(-inf, inf)):
        '''
        Return an amplitude control channel with 'name'
        specified as positional argument.
        Setting is in mV.
        '''
        return SR124Amplitude(self, name, limits)
        
    def P(self, name="SR124 Phase (deg)", limits=(-inf, inf)):
        '''
        Return a phase control channel with 'name'
        specified as positional argument.
        Setting is in degrees.
        '''
        return SR124Phase(self, name, limits)
        
    def B(self, name="SR124 DC Bias (V)", limits=(-inf, inf)):
        '''
        Return a DC Bias control channel with 'name'
        specified as positional argument.
        Setting is in Volts.
        '''
        return SR124Bias(self, name, limits)
        
    def FF(self, name="SR124 Filter Frequency (Hz)", limits=(-inf, inf)):
        '''
        Return a Filter Frequency control channel with 'name'
        specified as positional argument.
        Setting is in Hz.
        '''
        return SR124FilterFreq(self, name, limits)
        
    def O(self, name="SR124 Output", limits=(-inf, inf)):
        '''
        Return an output measurement channel with 'name'
        specified as positional argument.
        Setting is in V or A.
        '''
        id_string = self.query("*IDN?")
        if id_string.find("SR124") >= 0:
            return SR124Output(self, name, limits)
        elif id_string.find("SR2124") >= 0:
            return SR124OutputX(self, name, limits), SR124OutputY(self, name, limits)
        else:
            raise Exception('This is not a type of SR lock in that this driver recognizes (yet).')
        
    def S(self, name="SR124 Input Range (V)", limits=(-inf, inf)):
        '''
        Return an input sensitivity control channel with 'name'
        specified as positional argument.
        Setting is in V.
        '''
        return SR124InputSensitivity(self, name, limits)

    def unlock(self):
        '''
        Returns the SR124 command interface to "local" mode.
        '''
        self.write("LOCL 0")
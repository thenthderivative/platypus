# -*- coding: utf-8 -*-
"""
Created on November 7 2018

@author: Jason Phoenix
"""

import platypus
import pyvisa
import pyvisa
from numpy import linspace
import time
import math

#Give a name to the device in the next line
class HP3245A(platypus.base.Channel):
    
    '''
    Control software for HP 3245A universal source, used exclusively as a voltage source.
    
    Required Inputs: (name,address)
    '''    
    
    def __init__(self, name, address, delay=0.01, maxstep = 0.002, *args, **kwargs): #maxstep, rate,
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided channel name for this instrument
        self.setname(name)

        # Create the instrument. Ensure that baud rate, data bits, and other
        # settings are correct for the instrument being used.
        rm = pyvisa.ResourceManager()
        lib = rm.visalib
        self.inst = rm.open_resource(address)

        self.output = 0
        self.maxstep = maxstep
        self.delay = delay

        #Enables EOI function
        self.inst.write('END ON')
        #Use channel A at front of device.
        self.inst.write('USE 0')
        self.stopped = False

    def validate(self,v): return True
        #This function ensures that the values input by the user are valid.
        
    def set_voltage(self, v):
        #This function allows one to alter the settings of the device.
        #May be used to adjust internal, digital settings of device, or to
        #induce mechanical motion.
        #for example:
        self.inst.write('APPLY DCV '+str(v))
        self.output = v
        platypus.update()

    # Step until you're there!
    def set(self, V):
        def steptoward(V):
            v_i = self.getvalue()
            v_f = V
            d = v_f - v_i
            sign = lambda x: math.copysign(1, x)
            if abs(d) >= self.maxstep:
                v_i = v_i + self.maxstep * sign(d)
                self.set_voltage(v_i)
            else:
                self.set_voltage(v_f)
            if self in platypus.getch(): platypus.update(self)
            time.sleep(self.delay)
            return abs(d) > self.maxstep
        while (True):
            with self.lock:
                if not steptoward(V): break
                if self.stopped:
                    self.stopped = False
                    break

#    def set(self,stop):
#        '''
#        Required Inputs:
#
#        voltage in V (float)
#        '''
#        start = self.read()
#        if start != stop:
#            num_steps = round(abs(start-stop)*self.maxstep**-1)
#            direction = round((stop-start)/abs(stop-start))
#            vals = linspace(start+direction*self.maxstep,stop,num_steps)

#            for i in vals:
#                self.set_voltage(i)
#                time.sleep(self.maxstep/self.rate) #sleep time in seconds per step

    def stop(self):
        with self.lock:
            self.stopped = True
        #Instructs the device to cease all operations in progress. For example:
        #self.inst.write('stop')
    
    
    def read(self):
        #Reads the value from the channel. May require a query statement
        #instead of read, depending on the instrument and how it is being used.
        #Updates the attribute.

        self.output = float(self.inst.query('OUTPUT?').strip())
        platypus.update()
        
        return self.output
        
        
    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.output
        
        
    def gettype(self):
        #If this is a control channel, 'c' should be returned. If it is a
        #measurement channel, 'm' should be returned.
        return 'c'

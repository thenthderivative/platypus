# -*- coding: utf-8 -*-
"""
Created on March 21st 2016
@author: louis gaudreau
"""

import platypus
import pyvisa
import pyvisa
import time
from numpy import linspace

class ANC300_Channel(platypus.base.Channel):
    def __init__(self, name, anc, axis):
        super().__init__()
        self.setname(name)
        self.instr = anc
        self.axis = axis
    def set(self, v): raise NotImplementedError
    def read(self): raise NotImplementedError

class ANC300_Offset(ANC300_Channel):
    def set(self, v):
        self.value = v
        self.instr.offset(self.axis, self.value)
        return self.value
    def read(self):
        self.value = self.instr.get_offset(self.axis)
        return self.value

class ANC300(object):
    
    '''
    Documentation:
    Module that controls each axis of the ANC300.
    '''    
    
    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        self.name = 'ANC300'

        # Create the instrument
        rm = pyvisa.ResourceManager()
        lib = rm.visalib
        self.inst = rm.open_resource(address,
                    read_termination = '\r\n',
                    write_termination = '\r\n')


        #disable the echo of the console
        self.inst.query('echo off')

              
##        #The controller is set to stepping mode for all axes
##        self.inst.query('setm %d stp' %(1))
##        self.inst.query('setm %d stp' %(2))
##        self.inst.query('setm %d stp' %(3))
##        self.inst.query('setm %d stp' %(4))

    def validate(self):
        return True

    def set_frequency(self, axis, frequency):
        '''
        Documentation:
        Sets the frequency of the piezo of a given axis.
        Input:
        axis (1,2,3, or 4) (int)
        frequency in Hz (int)
        Return:
        None
        '''
        self.inst.query('setf %d %d' %(axis,frequency))

    def get_frequency(self, axis):
        '''
        Documentation:
        Gets the frequency of the piezo of a given axis.
        Input:
        axis (1,2,3, or 4) (int)
        Return:
        frequency in Hz (float)
        '''
        frequency = self.inst.query('getf %d' %(axis))
        last_line = self.inst.read()
        frequency = float(frequency[12:-2])
        return frequency

        
    def set_voltage(self, axis, voltage):
        '''
        Documentation:
        Sets the voltage amplitude of the piezo of a given axis.
        Input:
        axis (1,2,3, or 4) (int)
        voltage in Volts (float)
        Return:
        None
        '''
        self.inst.query('setv %d %f' %(axis,voltage))
        try:
            self.inst.read()
        except:
            pass
        

    def get_voltage(self, axis):
        '''
        Documentation:
        Gets the voltage amplitude of the piezo of a given axis.
        Input:
            axis (1,2,3, or 4) (int)
        Return:
            voltage in Volts (float)
        '''
        voltage = self.inst.query('getv %d' %(axis))
        last_line = self.inst.read()
        voltage = float(voltage[10:-2])
        return voltage
    
    
    def step_by(self, axis, number_of_steps,settings_check = True):
        '''
        Documentation:
        Moves the piezo of a given axis by a given number of steps.
        Input:
            axis (1,2,3, or 4) (int)
            number_of_steps (int)
        Optional Keyword Inputs:
            settings_check (True or False, default = True)
        Return:
            None
        '''
        if settings_check == True:
            self.inst.timeout = 100000
            self.inst.query('setm %d stp' % (axis))#set to stepping mode
            self.inst.timeout = 30
        if (number_of_steps > 0) :
            #Do the stepping in the upwards direction (stepu query)
            self.inst.query('stepu %d %d' %(axis,number_of_steps))
            #Increase the timeout time to an arbitrary number to be able
            #to wait for the stepwait query to finish.
            self.inst.timeout = 100000
            done = self.inst.query('stepw %d' % (axis))#wait for stepping to finish
            #Reset the timeout to 30ms
            self.inst.timeout = 30
        elif number_of_steps == 0:
            pass
        else :
            #Do the stepping in the downwards direction (stepd query)
            self.inst.query('stepd %d %d' %(axis,abs(number_of_steps)))
            #Increase the timeout time to an arbitrary number to be able
            #to wait for the stepwait query to finish.
            self.inst.timeout = 100000
            done = self.inst.query('stepw %d' % (axis))#wait for stepping to finish
            #Reset the timeout to 30ms
            self.inst.timeout = 30

    def offset(self, axis, voltage):
        '''
        Documentation:
        Moves the piezo of a given axis using a DC offset voltage (range ~800nm at 4K).
        Input:
            axis (int: 1,2,3, or 4)
            voltage (float: 0V to 150V)
        Return:
            None
        '''
        if 150 >= voltage >= 0:
            current_offset = self.get_offset(axis)
            if abs(current_offset-voltage) > 1:
                vals = linspace(current_offset,voltage,abs(current_offset-voltage)+1)
            else:
                vals = [voltage]
            for i in vals:
                self.inst.query('setm %d off' % (axis))#set to offset mode 
                self.inst.query('seta %d %f' %(axis,i)) #set offset voltage
                time.sleep(0.05)
        else:
            print('Offset voltage must be between 0V and 150V.')
            error

    def get_offset(self, axis):
        '''
        Documentation:
        Gets the piezo DC offset voltage (0V to 150V).
        Input:
        None
        Return:
        Offset voltage
        '''
        voltage = float(self.inst.query('geta %d' %(axis)).split('voltage = ')[1][:-2])
        self.inst.read()#clears the buffer of the 'OK' which follows each 'get offset'
        return  voltage#get offset voltage

    def ground(self):
        '''
        Documentation:
        Grounds all the attocube piezos controlled by the ANC300
        Input:
        None
        Return:
        None
        '''
        self.inst.query('setm 1 gnd')
        self.inst.query('setm 2 gnd')
        self.inst.query('setm 3 gnd')
        self.inst.query('setm 4 gnd')

    def stop(self):
        '''
        Documentation:
        Stops all mouvement of all the attocube piezos controlled by the ANC300
        Input:
        None
        Return:
        None
        '''
        self.inst.query('stop 1')
        self.inst.query('stop 2')
        self.inst.query('stop 3')
        self.inst.query('stop 4')
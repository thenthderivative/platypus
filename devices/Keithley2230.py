# -*- coding: utf-8 -*-
"""
Created on September 01 2016

@author: Louis Gaudreau
"""

import platypus 
import pyvisa
import pyvisa
import time
from numpy import linspace

#Give a name to the device in the next line
class Keithley2230(platypus.base.Channel):
    
    '''
    Documentation
    Controls the Keithley 2230 Programmable Triple Channel DC Power Supply.
    Create a platypus channel for each individual channel of the Keithley.
    
    Required Inputs: (name,address, channel)
    Optional Keyword Inputs: ramp (True or False, default=False)
    '''    
    
    def __init__(self, name, address, channel, *args, ramp = False, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided channel name for this instrument
        self.setname(name)

        # Create the instrument. Ensure that baud rate, data bits, and other
        # settings are correct for the instrument being used.
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address)

        # Set device channel. Can only access one channel of Kiethley 2230 at a time,
        # so channel will be changed before each action.
        self.channel = channel
        
        # Initialize the power supply by reading its value
        self.voltage = self.read()

        self.ramp_on = ramp

    def validate(self,voltage):
        #No validation required
        return True

    def stop(self): pass
        #No stop required

    

    def change_channel(self,*args):
        '''
        Switch to new channel, if channel specified. If no channel specified,
        returns current channel. Example:
        >>> Source.change_channel(3) #Sets instrument to channel 3.
        >>> Source.change_channel() #Returns current channel.
        3
        
        Optional Input: channel (int - 1,2 or 3)
        Optional Return: channel (int)
        '''
        
        if len(args) == 0:
            return int(self.inst.query('INST:SEL?')[2])
        else:
            self.inst.write('INST:SEL CH'+str(args[0]))

    def channel_check(self):
        '''
        Checks to make sure Kiethley 2230 is set to appropriate channel, and
        switches channels if appropriate.
        Input:
        None
        Return:
        None
        '''
        if self.change_channel() != self.channel:
            self.change_channel(self.channel)
    
    def read(self):
        '''
        Reads the voltage of the selected source channel
        Input:
        None
        Return:
        voltage in V (float)
        '''
        self.channel_check()
        
        self.voltage = float(self.inst.query('MEAS:VOLT?'))
        platypus.update(self)
        return self.voltage

    def ramp(self,V_f,increment):
        if increment == None:
            increment = 0.1
        #limited to microvolts or more
        V_i = round(self.read(),6)
        if V_i < V_f:
                direction = 1
        elif V_i > V_f:
                direction = -1
        else:
                return self.read()
        for i in range(1,int(abs(V_f-V_i)/increment)):

                self.channel_check()
                self.inst.write('VOLT '+str(V_i+i*direction*increment))

                self.voltage = round(V_i+i*direction*increment,6)
                platypus.update(self)
                time.sleep(0.1)
        self.channel_check()
        self.inst.write('VOLT '+str(V_f))
        self.voltage = round(V_f,6)
        # platypus.update(self)
        return self.voltage

    def set(self,value,increment = None):
        '''
        Sets the voltage of the selected source channel
        Input:
        voltage in V (float)
        Optional Input:
        increment in V (float, only used when ramping)
        Return:
        None
        '''

        if self.ramp_on == False:
            self.channel_check()
            
            self.inst.write('VOLT '+str(value))
            self.read()
        elif self.ramp_on == True:
            self.ramp(value,increment)

    def step_to(self,stop,step_size,time_delay):
        '''
        Steps the output to the desired voltage in time-delayed increments.
        Input:
        stop voltage in V (float)
        step size in V (float)
        time delay in s (float)
        Return:
        None
        '''
        self.channel_check()
        
        num_points = abs(stop-self.voltage)/step_size + 1
        if self.voltage < stop:
            step_vals = linspace(self.voltage+step_size,stop,num_points)
        else:
            step_vals = linspace(self.voltage-step_size,stop,num_points)
        for val in step_vals:
            self.set(val)
            time.sleep(time_delay)

    def source_on(self):
        '''
        Turns the voltage source of the keithley 2230 on.
        Input: None
        Return: None
        '''
        #Output is the voltage source, 0=off, 1=on
        self.inst.write('OUTP:ENAB 1')

    def source_off(self):
        '''
        Turns the voltage source of the keithley 2230 off.
        Input: None
        Return: None
        '''
        #Output is the voltage source, 0=off, 1=on
        self.inst.write('OUTP:ENAB 0')
        
    def volt_limit(self,*args):
        '''
        Sets the voltage limit of the kiethley 2230 or returns voltage limit
        of current channel. Example:
        >>> Source.volt_limit(3.5) #Sets voltage limit to 3.5V.
        >>> Source.volt_limit() #Returns voltage limit.
        3.5
        
        Optional Input: limit in volts (0-30 for CH1 and CH2, 0-6 for CH3, or use 'MAX','MIN')
        Optional Return: limit in volts
        '''
        self.channel_check()
        
        if len(args) == 0:
            return float(self.inst.query('VOLT:LIM?')[:-1])
        else:
            #Set voltage range.
            self.inst.write('VOLT:LIM '+str(args[0]))

    def remote_on(self):
        '''
        Sets the device to operate in remote mode.
        Input:
        None
        Output:
        None
        '''
        self.inst.write('SYST:REM')

    def remote_off(self):
        '''
        Sets the device to operate in local mode.
        Input:
        None
        Output:
        None
        '''
        self.inst.write('SYST:LOC')
    
    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.voltage
        
        
    def gettype(self):
        #Control channel, so 'c' is returned.
        return 'c'

'''
Copyright (c) 2019, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@Author: Aviv Padawer-Blatt
'''

import platypus
import pyvisa
import pyvisa
import time
import numpy as np
from numpy import linspace
import math

import threading


class PowerMeterChannel(platypus.base.Channel):

    def __init__(self, powermeter, channel, name, *args, **kwargs):
        # Run the (mandatory) superclass constructor.
        super().__init__(*args, **kwargs)

        # Remember the port number this channel corersponds to.
        self.powermeter = powermeter
        self.channel = channel

        # Set the name of the channel.
        self.n = name
        self.unit = self.getunit()
        self.setname(str(name)+" ("+str(self.unit)+")")

        # Initialize channel by reading its value.
        self.output = self.powermeter.read_output(self.channel)

    ## Standard methods (see platypus documentation)

    def validate(self, value):
        return True

    def stop(self): pass

    def gettype(self):
        # Control channel, so 'm' is returned.
        return 'm'

    
    
    def read(self):
        # Updates stored output in channel and returns output.
        self.output = self.powermeter.read_output(self.channel)
        platypus.update(self)
        return self.getvalue()

    def set(self, v): pass

    def getvalue(self):
        # Returns last read output.
        return self.output
        
        
    def SetName(self):
        self.unit = self.getunit()
        self.setname(str(self.n)+" ("+str(self.unit)+")")
        self.read()
            
    
    def setunit(self, unit):
        self.powermeter.set_channel_unit(self.channel, unit)
        
        # Set the name of the channel so units are still correct.
        self.SetName()

        
    def getunit(self):
        return self.powermeter.get_channel_unit(self.channel)
    
    
    def setres(self, res):
        self.powermeter.set_channel_res(self.channel, res)
        
        
    def sensorA(self):
        self.powermeter.channel_input_config(self.channel, "A")
        self.SetName()
        
    def extV(self):
        self.powermeter.channel_input_config(self.channel, "V")
        self.SetName()
        
    
    def setautorange(self):
        sensor = self.getsensor()
        
        self.powermeter.set_range(sensor, "0")
    
        
    #def off(self):
    #    self.powermeter.channel_input_config(self.channel, "OFF")
        
    def getsensor(self):
        sensor = self.powermeter.get_channel_input_config(self.channel)
        return sensor
        
        
    def default(self):
        sensor = self.getsensor()
        
        if (sensor == "A"):
            self.powermeter.sensor_measurement_mode("A", "DEFAULT")
        else:
            raise Exception("Channel not reading sensor A")
            
    def mod(self):
        sensor = self.getsensor()
        
        if (sensor == "A"):
            self.powermeter.sensor_measurement_mode("A", "MOD")
        else:
            raise Exception("Channel not reading sensor A")
            
    def custom(self):
        sensor = self.getsensor()
        
        if (sensor == "A"):
            self.powermeter.sensor_measurement_mode("A", "CUSTOM")
        else:
            raise Exception("Channel not reading sensor A")


       



class ML2437APowerMeter(object):

    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor.
        super().__init__(*args, **kwargs)
        
        #Use provided name for this channel

        # Create the instrument. Ensure that baud rate, data bits, and other
        # settings are correct for the instrument being used.
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                read_termination = '\n',
                write_termination = '\n')
        #self.inst.timeout = 2500
        
    
    def write(self, msg):
        self.inst.write(msg)
        return None

    def query(self, msg):
        return self.inst.query(msg)

        
    def set_channel_unit(self, channel, unit):
        # Channel may be 1 or 2
        # May be W (watts), DBM (dB), DBUV (dBuV), DBMV (dBmV)
        # Note: Formula for dBmV is dBmV = 20*log(10)(V/1mV)
        if unit == "mV": unit = "DBMV"
        if unit == "V": unit = "DBUV"
        #if unit == "mVpp": unit = "DBM"
        msg = "CHUNIT {}, {}".format(channel, unit)
        self.write(msg)
        
    def get_channel_unit(self, channel):
        # Get units for channel 1 or 2
        msg = "CHUNIT? {}".format(channel)
        unit = (self.query(msg))[10:]
        if unit == "DBMV": unit = "mV"
        if unit == "DBUV": unit = "V"
        #if unit == "DBM": unit = "mVpp"
        return unit
        
    def set_channel_res(self, channel, res):
        # Channel may be 1 or 2
        # Resolution may be 1, 2 or 3 decimal points
        msg = "CHRES {}, {}".format(channel, res)
        self.write(msg)
        
    def channel_input_config(self, channel, config):
        # Set channel to specific configuration
        # OFF, A (sensor A), B (sensor B), V (External Volts), A-B, B-A, A/B, B/A
        # Note: Either sensor B is not installed, or something is wrong with it
        msg = "CHCFG {}, {}".format(channel, config)
        self.write(msg)
        
    def get_channel_input_config(self, channel):
        msg = "CHCFG? {}".format(channel)
        return (self.query(msg))[8:]
        
    def sensor_measurement_mode(self, sensor, mode):
        # Set sensor A or B to DEFAULT (carrier wave),
        # MOD (modulated average), or CUSTOM (user configurable trigger setup mode)
        msg = "SENMM {}, {}".format(sensor, mode)
        self.write(msg)
        
        
    def set_range(self, sensor, range):
        # set range for read powers
        # 0 is auto (powermeter finds best range for current power)
        msg = "RGH {}, {}".format(sensor, range)
        self.write(msg)        
        
    def get_range(self, sensor):
        # Get range for read powers
        msg = "RGH? {}".format(sensor)
        return float((self.query(msg))[6:])
        
        
    def read_output(self, channel):
        # Read power or voltage from specified channel (1 or 2)
        msg = "O {}".format(channel)
        output = float(self.query(msg)[1:])
        
        unit = self.get_channel_unit(channel)
        if unit == "mV": output = self.dBmVtomV(output)
        if unit == "V": output = self.dBuVtoV(output)
        #if unit == "mVpp": output = self.dBmtomV(output)
        
        return output
        
        
        
    def dBmVtomV(self, dBmV):
        return np.power(10, dBmV/20)
        
    def mVtodBmV(self, mV):
        return 20 * np.log10(mV)
        
    def dBuVtoV(self, dBuV):
        return 0.000001 * np.power(10, dBuV/20)
        
    def VtodBuV(self, V):
        return 20 * np.log10(V/0.000001)
        
    #def dBmtomV(self, dBm):
    #    return 632.360 * np.power(10, dBm/20)
        
    #def mVtodBm(self, mV):
    #    return 20 * np.log10(mV/632.360)


    # Spawns channel for Power Meter (either 1 or 2), with a given name.
    def getchannel(self, channel, name, *args, **kwargs):
        assert channel in range(1, 3), "Invalid channel"
        return PowerMeterChannel(self, channel, name, *args)

'''

Copyright (c) 2019, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Author: Aviv Padawer-Blatt

'''

import platypus
import pyvisa
import pyvisa
import time
from math import isclose, inf


class TSGBase(platypus.base.Channel):
    def __init__(self, parent, name, limits=(-inf, inf)):
        self.setname(name)
        self.parent = parent
        self.limits = limits
        self.v = 0.0

    def validate(self, v):
        low, high = min(self.limits), max(self.limits)
        return (v >= low and v <= high)
    
    def stop(self): pass
    def getvalue(self): return self.v
    def gettype(self): return 'c'
    
    def setlimits(self, low, high):
        self.limits = (low, high)
    def getlimits(self):
        return self.limits


class TSGFreq(TSGBase):
    def validate(self, v):
        return (v >= 1e-12 and v <= 2000)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_frequency(v)
        else:
            raise ValueError("Invalid Frequency")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_frequency()
        return self.getvalue()
        


class TSGPower(TSGBase):
    #def validate(self, v):
    #    return (v >= -100 and v <= 0) and super().validate(v)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_power(v)
        else:
            raise ValueError("Invalid Power")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_power()
        return self.getvalue()
        


class TSGPhase(TSGBase):
    def validate(self, v):
        return (v >= 0.0 and v <= 360.0)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_phase(v)
        else:
            raise ValueError("Invalid Phase")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_phase()
        return self.getvalue()



class TSG4102AMW(object):
    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        #Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                                     write_termination="\n",
                                     read_termination="\n")


    def write(self, msg):
        self.inst.write(msg)
        return None

    def query(self, msg):
        self.inst.write(msg)
        return self.inst.read()
    

    def set_frequency(self, freq):
        '''Change the frequency'''
        msg = "FREQ {} MHZ".format(freq)
        self.write(msg)

    def get_frequency(self):
        '''Read the frequency'''
        msg = "FREQ? MHz"
        return float(self.query(msg))

    def set_power(self, power):
        '''Change the power level'''
        msg = "AMPR {}".format(power)
        self.write(msg)

    def get_power(self):
        '''Read the power level'''
        msg = "AMPR?"
        return float(self.query(msg))
        
    def set_phase(self, phase):
        '''Change the phase of the carrier'''
        msg = "PHAS {}".format(phase)
        self.write(msg)
    
    def get_phase(self):
        '''Read the phase of the carrier'''
        msg = "PHAS?"
        return float(self.query(msg))

    

    def lf_on(self, status=True):
        '''Turn on Low Frequency BNC output or, if the argument is false, turn it off'''
        msg = "ENBL 1" if status else "ENBL 0"
        self.write(msg)

    def lf_off(self):
        '''Turn off Low Frequency BNC output'''
        self.lf_on(False)
        

    def rf_on(self, status=True):
        '''Turn on Low Frequency Type N RF output or, if the argument is false, turn it off'''
        msg = "ENBR 1" if status else "ENBR 0"
        self.write(msg)

    def rf_off(self):
        '''Turn off Low Frequency Type N RF output'''
        self.rf_on(False)


    
    # Ignore these four commands for now
    def mod_on(self, status=True):
        '''Turn on MW modulation or, if the argument is false, turn it off'''
        self.IQ_on()
        msg = ":OUTP:MOD:STAT ON" if status else "OUTP:MOD:STAT OFF"
        self.write(msg)
        self.on()
        
    def mod_off(self):
        '''Turn off MW modulation'''
        self.IQ_off()
        self.mod_on(False)
                

    def IQ_on(self, status=True):
        '''Enable IQ modulator'''
        msg = "SOUR:DM:STAT ON" if status else "SOUR:DM:STAT OFF"
        self.write(msg)

    def IQ_off(self):
        '''Disable IQ modulator'''
        self.IQ_on(False)

    


    def F(self, name="TSG4102A Frequency (MHz)", limits=(-inf, inf)):
        '''
        Return a frequency control channel with 'name'
        specified as positional argument.
        Units are in MHz.
        '''
        return TSGFreq(self, name, limits)

    def L(self, name="TSG4102A Power (dBm)", limits=(-100, 0)):
        '''
        Return a power control channel with 'name'
        specified as positional argument.
        Setting is in dBm.
        '''
        return TSGPower(self, name, limits)
        
    def P(self, name="TSG4102A Phase (deg)", limits=(-inf, inf)):
        '''
        Return a phase control channel with 'name'
        specified as positional argument.
        Setting is in degrees.
        '''
        return TSGPhase(self, name, limits)

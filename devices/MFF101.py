# -*- coding: utf-8 -*-
"""
Created on Wed OCt 21 10:27:47 2015

@author: GaudreauL
"""

import platypus 
import pyvisa
import pyvisa
import struct


class MFF01(platypus.base.Channel):
    '''
    Documentation:
    Controls the MFF101 flip mount.
    Note: If the device is not on a COM port in the device manager, remember
    to look for it as APT USB Device, then select 'Load VCP' in the
    properties, then restart the computer. It should then appear in the
    COM ports list.
    '''



    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided name for this channel
        self.setname(name)

        # Create the instrument
        self.rm = pyvisa.ResourceManager()
        self.lib = self.rm.visalib
        self.inst = self.rm.open_resource(address,
                baud_rate = 115200,
                data_bits = 8,
                stop_bits = pyvisa.constants.StopBits.one,
                parity=pyvisa.constants.Parity.none,
                read_termination = '',
                write_termination = '',
                timeout = 40000)#This timeout is needed to let the rotator do a full 360 deg rotation


        #Initialize the flip mount  position to its current position
        self.position = self.read()

    def validate(self, angle):
        return True

    def set(self, position):
        '''
        Sets the position of the flip mount.
        Input:
        position (1 or 2) (int)
        Return: None
        '''
        #This instrument uses bytes in hexadecimal strings (not ascii) so that is why we need the
        #'write.raw(b"\xblababla)' command. Programming this device can be nasty. It is very 
        #important to understand the communication protocol located in the PRM1Z8E Instrument folder.
        #Remember to multiply the angle in degrees by the multiplying factor.
        self.inst.write_raw(b"\x50\x04\x06\x00\xd0\x01\x01\x00" + struct.pack("<I", int(new_angle*self.multiplying_factor))) #MGMSG_MOT_MOVE_ABSOLUTE in the communication protocol

        self.inst.write_raw(b"\x53\x04\x01\x00\x50\x01")            	
        #we now need to wait for the rotator to reach the angle. To do this, we send the appropriate
        #read command and wait for the answer. The lines containing the warn variable are used to set
        #warnings from pyvisa off and then on again. This is to avoid a warning that appears when the
        #instrument sends the 'completed movement' 20 byte hexadecimal string message.
        warn = self.lib.issue_warning_on
        self.lib.issue_warning_on = []
        self.lib.read(self.inst.session, 20)#20 is the number of bytes to read (taken from the communication protocol manual)
        self.lib.issue_warning_on = warn
        
        self.angle=new_angle
        print(self.name,' set to ',new_angle,' degrees.')

        #Update platypus so that the new value appears on the state monitor
        platypus.update()    


    def stop(self):pass

    def read(self):
        '''
        Reads the last value set and stored in memory of the MFF101.
        Input: None
        Return:
        position (1 or 2) (int)
        '''
        #This instrument uses bytes in hexadecimal strings (not ascii).Programming 
        #this device can be nasty. It is very important to understand the 
        #communication protocol located in the PRM1Z8E Instrument folder.

        #Request and then read the absolute position of the rotator
        self.inst.write_raw(b"\x51\x04\x01\x00\x50\x01")#MGMSG_MOT_REQ_MOVEABSPARAMS from communication protocol 
        self.angle = self.lib.read(self.inst.session, 12)#12 is the number of bytes to read (taken from the communication protocol manual)

        #Take the last 4 bytes (which contain the absolute position) from the previous read command
        #It comes as a 'tuple', so we take the last 4 bytes of the first item([0][-4:])
        self.angle = struct.unpack('<I',self.angle[0][-4:])
        
        #divide by the multiplying factor) rounded to 2 decimals
        self.angle = round(self.angle[0]/self.multiplying_factor,2)
        
        return self.angle


    def getvalue(self):
        #Returns the last set value stored in memory.
        return self.position

    def gettype(self):
        #This is a control channel, so the string 'c' is returned.
        return 'c'

# -*- coding: utf-8 -*-
"""
Created on Friday June 03 2016

@author: Louis Gaudreau
"""

import platypus
import pyvisa
import numpy as np
import time


class MSO5204B(object):
    '''
    Controls the TDS6154C oscilloscope.
    Inputs:
    address (str)
    '''
    
    def __init__(self, address,*args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)
        # Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address)

        #Create default scope channel
        self.channel = 1

    def validate(self):
        return True
        
    def acquire_sample_waveform(self, channel):
        '''
        Acquire a sample waveform (i.e. non averaged) from a given channel.
        Inputs:
        channel (int)
        Return:
        time in seconds (float array)
        waveform in Volts (float array)
        '''
        #Select channel
        self.select_channel(channel)
        #Remove headers from queries, makes parsing easier
        self.inst.write('HEADER OFF')
        #Set the encoding to binary signed integer data-point representation
        #with the most significant byte transferred first.
        self.inst.write('DATA:WIDTH 1')
        self.inst.write('DATa:ENCdg RIB')
        #Get the Y multiplying factor
        y_multiplier = self.get_y_multiplier()
        #Get the Y offset
        y_offset = self.get_y_offset()
        #Get the zero of Y
        y_zero = self.get_y_zero()
        #Get the time increment per point
        time_step = self.get_time_step()
        #Set to 'Sample' mode acquisition
        self.inst.write('ACQuire:MODe SAMPLE')
        print('Acquiring mode: Sample')
        #Set the acuisition mode on
        self.inst.write('ACQuire:STATE ON')
        #Set the length of the waveform to 25000000 (maximum possible)to get the whole waveform
        self.inst.write('DATa:STOP 25000000')
        #Start the measurement (equivalent of pressing 'SINGLE' on the scope)
        #This indicates that the next acquisition the instrument makes will
        #be of the single-sequence type.
        self.inst.write('ACQuire:STOPAfter SEQUENCE')
        #Wait until the trace is completed
        busy = 1
        while (busy == 1):
            busy = int(self.inst.query('BUSY?'))
        #Acquire the waveform from the scope after trace is completed
        waveform = self.inst.query_binary_values('CURVe?', datatype='b', is_big_endian=True)
        #Transform the python list into an numpy array of strings
        waveform = np.asarray(waveform)
        #Transform the array of strings into an array of floats
        waveform = waveform.astype(np.float)
        #The data commes in 8 bit integer format (-128->127), the following line transforms it into
        #the actual voltage
        waveform = y_multiplier*(waveform - y_offset) + y_zero
        #Create the time array(horiz. axis)
        time_array = np.arange(0, time_step * len(waveform), time_step)
        return time_array,waveform
    
    
    
    def acquire_averaged_waveform(self, channel, number_of_averages):
        '''
        Acquire an averaged waveform (i.e. non averaged) from a given channel.
        Inputs:
        channel (int)
        number_of_averages (int)
        Return:
        time in seconds (float array)
        waveform in Volts (float array)
        '''
        #Select channel
        self.select_channel(channel)
        #Remove headers from queries, makes parsing easier
        self.inst.write('HEADER OFF')
        #Set the encoding to binary signed integer data-point representation
        #with the most significant byte transferred first.
        self.inst.write('DATA:WIDTH 1')
        self.inst.write('DATa:ENCdg RIB')
        #Get the Y multiplying factor
        y_multiplier = self.get_y_multiplier()
        #Get the Y offset
        y_offset = self.get_y_offset()
        #Get the zero of Y
        y_zero = self.get_y_zero()
        #Get the time increment per point
        time_step = self.get_time_step()
        #Set to 'Average' mode acquisition
        self.inst.write('ACQuire:MODe AVErage')
        #Set the number of traces to average
        self.inst.write('ACQuire:NUMAVg '+str(number_of_averages))
        print('Acquiring mode: Average '+str(number_of_averages)+' traces')
        #Set the acuisition mode on
        self.inst.write('ACQuire:STATE ON')
        #Set the length of the waveform to 500000 (maximum possible)to get the whole waveform
        self.inst.write('DATa:STOP 500000')
        #Start the measurement (equivalent of pressing 'SINGLE' on the scope)
        #This indicates that the next acquisition the instrument makes will
        #be of the single-sequence type.
        self.inst.write('ACQuire:STOPAfter SEQUENCE')
        #Wait until the trace is completed
        busy = 1
        while (busy == 1):
            busy = int(self.inst.query('BUSY?'))
        #Acquire the waveform from the scope after trace is completed
        waveform = self.inst.query_binary_values('CURVe?', datatype='b', is_big_endian=True)
        #Transform the python list into an numpy array of strings
        waveform = np.asarray(waveform)
        #Transform the array of strings into an array of floats
        waveform = waveform.astype(np.float)
        #The data commes in 8 bit integer format (-128->127), the following line transforms it into
        #the actual voltage
        waveform = y_multiplier*(waveform - y_offset) + y_zero
        #Create the time array(horiz. axis)
        time_array = np.arange(0, time_step * len(waveform), time_step)
        return time_array,waveform

    def trigger(self, channel):
        '''
        Trigger a given channel.
        Inputs:
        channel (int)
        Return:
        None
        '''
        self.inst.write('DATa:SOUrce CH'+str(channel))
        ready = 0
        while ready == 0:
            query = self.inst.query('TRIGger:STATE?')
            if query[:5] == 'READY':
                self.inst.write('TRIGger FORCe')
                time.sleep(0.25)
                ready = 1
            else:
                time.sleep(0.0)

    def select_channel(self, channel):
        '''
        Selects a channel on the scope
        Input:
        channel (int)
        Return:
        None
        '''
        self.inst.write('DATa:SOUrce CH'+str(channel))
        self.inst.write('MEASUrement:IMMed:SOUrce1 CH'+str(channel))
        #print('Channel '+str(channel)+' selected.')
        self.channel = channel
        

    def get_y_multiplier(self):
        '''
        Returns the vertical scale factor per digitizing
        level for the waveform.
        Input:
        None
        Return:
        y_multiplier (float)
        '''
        y_multiplier = float(self.inst.query('WFMPRE:YMULT?'))
        return y_multiplier

    def get_y_offset(self):
        '''
        Returns the vertical offset in digitizing levels
        for the waveform
        Input:
        None
        Return:
        y_offset (float)
        '''
        y_offset = float(self.inst.query('WFMO:YOFF?'))
        return y_offset

    def get_y_zero(self):
        '''
        Returns the vertical offset for the waveform.
        Input:
        None
        Return:
        y_zero (float)
        '''
        y_zero = float(self.inst.ask('WFMPRE:YZERO?'))
        return y_zero

    def get_time_step(self):
        '''
        Returns the horizontal sampling interval.
        Input:
        None
        Return:
        time_step (float)
        '''
        time_step = float(self.inst.query('WFMPRE:XINCR?'))
        return time_step

    def get_RMS(self):
        '''
        Returns the root mean square value of the selected channel.
        Inputs:
        None
        Return:
        RMS (float)
        '''
        #Set the immediate acquisition to RMS
        self.inst.write('MEASUREMENT:IMMED:TYPE RMS')
        #Get the RMS value
        RMS = float(self.inst.query('MEASUREMENT:IMMED:VALUE?'))
        return RMS


class MSO5204B_CHAN(platypus.base.Channel):
    '''
    This class is to be able to create multiple channels from the same instrument.
    You need to create a child of this class for every channel you need.
    '''
    def __init__(self, name, scope, *args, **kwargs):
        self.setname(name)
        self.scope = scope
        self.channel = 1
        self.last_value = None
        super().__init__(*args, **kwargs)
    def validate(self, value): return True
    def set(self, v): pass
    def stop(self):pass
    def getvalue(self):
        return self.last_value
    def gettype(self):
        #This is a measurment channel, so the type 'm' is returned.
        return 'm'



class MSO5204B_RMS(MSO5204B_CHAN):
    '''
    This class can be used to create a channel that always
    shows the RMS value of the scope. It is a child of TDS6154C_CHAN
    One has to declare it in the platypus config file as follows:
        scope = MSO5204B('TCPIP0::169.254.62.140::inst0::INSTR') #Declare the scope
        scope_RMS = MSO5204B_RMS('scope_RMS', scope) #Declare the scope_RMS channel

    '''
                   
    def read(self):
        if self.scope.channel != self.channel:
            self.scope.select_channel(self.channel)
        self.last_value = self.scope.get_RMS()
        return self.last_value

class MSO5204B_mean(MSO5204B_CHAN):
    '''
    This class can be used to create a channel that always
    shows the Mean value of the scope. It is a child of MSO5204B_CHAN
    One has to declare it in the platypus config file as follows:
        FASTscope = TDS5052('TCPIP0::169.254.62.140::inst0::INSTR') #Declare the scope
        FASTscope_RMS = TDS5052_RMS('Fast Scope RMS', FASTscope) #Declare the scope_RMS channel
    '''
    def read(self):
        if self.scope.channel != self.channel:
            self.scope.select_channel(self.channel)
        self.last_value = self.scope.get_mean()
        platypus.update()
        return self.last_value


class MSO5204B_amplitude(MSO5204B_CHAN):
    '''
    This class can be used to create a channel that always
    shows the Vpp value of the scope. It is a child of MSO5204B_CHAN
    One has to declare it in the platypus config file as follows:
        FASTscope = TDS5052('TCPIP0::169.254.62.140::inst0::INSTR') #Declare the scope
        FASTscope_RMS = TDS5052_RMS('Fast Scope RMS', FASTscope) #Declare the scope_RMS channel
    '''
    def read(self):
        if self.scope.channel != self.channel:
            self.scope.select_channel(self.channel)
        self.last_value = self.scope.get_amplitude()
        platypus.update()
        return self.last_value


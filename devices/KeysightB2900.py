import platypus, math, time
import numpy as np

try:
  import pyvisa as visa
  pyvisa.ResourceManager
except:
  import pyvisa

def make_iterable(X):
    if isinstance(X, str): return iter([X])
    try:
        return iter(X)
    except:
        return iter([X])

valid_modes = ["TIME", "RES", "CURR", "VOLT"]
def validate_modes(modes):
    valid = [mode in valid_modes for mode in modes]
    invalid = [mode for mode, v in zip(modes, valid) if v is False]
    if len(invalid) > 0:
        raise Exception("Invalid mode(s): {}".format(', '.join(invalid)))

class B2901A(object):
  def __init__(self, addr):
    self.device = pyvisa.ResourceManager().open_resource(addr)
  def on(on=True):
    if on: self.device.write("")
  def check_error(self):
    error = self.device.query(":SYST:ERR?")
    i,s = error.split(',')
    return int(i), s.strip()
  def measure_config(self, *modes):
    validate_modes(modes)
    modes = ['"{}"'.format(mode.upper().strip()) for mode in modes]
    modes = ','.join(modes)
    self.device.write(":SENS:FUNC:OFF:ALL")
    self.device.write(":SENS:FUNC {}".format(modes))
  def read_config(self, *modes):
    validate_modes(modes)
    modes = ['{}'.format(mode.upper().strip()) for mode in modes]
    modes = ','.join(modes)
    self.device.write(":FORM:ELEM:SENS {}".format(modes))
  def source_config(self, mode):
    validate_modes([mode])
    #modes = ['{}'.format(mode.upper().strip()) for mode in modes]
    #modes = ','.join(modes)
    #self.device.write(":FORM:ELEM:SENS {}".format(modes))
  def measure(self):
    results = [float(x) for x in self.device.query(":MEAS?").split(',')]
    if len(results) == 1:
        return results[0]
    return results
  def trace_config(self, clear=True, points=None, feed='SENS'):
    if clear:
        self.device.write(":TRAC:CLEAR")
    if points:
        self.device.write(":TRAC:POINTS {}".format(int(points)))
    if feed:
        self.device.write(":TRAC:FEED {}".format(feed))
        self.device.write(":TRAC:FEED:CONT NEXT")
  def stop(self):
    self.device.write(":ABORT")
  def trigger_config(self, mode='ALL',
        trig_source=None,
        arm_source=None,
        trig_out=None,
        arm_out=None,
        trig_count=None,
        arm_count=None,
        trig_timer=None,
        arm_timer=None,
        trig_delay=None,
        arm_delay=None,
        ):
    '''
        Valid arm and trigger 'modes':
        - 'ALL': Configure the output and measurement triggers.
        - 'ACQire': Just configure the measurement trigger.
        - 'TRANsient': Just configure the output trigger.
        Valid arm and trigger 'source':
        - 'AINT' (auto)
        - 'BUS'
        - 'TIMer'
        - 'LAN'
        - 'INT<1/2>'
        - 'EXT<1-14>'
        Valid arm and trigger outputs:
        - 'OFF': Disables trigger output.
        - 'LAN'
        - 'INT<1/2>'
        - 'EXT<1-14>'
        Valid trig and arm counts: integers, 1 or more triggers to revert to previous layer (arm->trig->sample)
        Valid trig and arm timer intervals: 2E-5 seconds to 1E+5 seconds.
        Valid trig and arm delay intervals: 0 seconds to 100000 seconds.
    '''
    if trig_source: self.device.write(':TRIG:{}:SOURCE {}'.format(mode, trig_source))
    if arm_source: self.device.write(':ARM:{}:SOURCE {}'.format(mode, arm_source))

    if trig_delay: self.device.write(':TRIG:{}:DELAY {}'.format(mode, trig_delay))
    if arm_delay: self.device.write(':ARM:{}:DELAY {}'.format(mode, arm_delay))
    if trig_timer: self.device.write(':TRIG:{}:TIMER {}'.format(mode, trig_timer))
    if arm_timer: self.device.write(':ARM:{}:TIMER {}'.format(mode, arm_timer))

    if trig_count: self.device.write(':TRIG:{}:COUNT {}'.format(mode, trig_count))
    if arm_count: self.device.write(':ARM:{}:COUNT {}'.format(mode, arm_count))

    if trig_out:
        self.device.write(':TRIG:{}:TOUT:SIGNAL {}'.format(mode, trig_out))
        self.device.write(':TRIG:{}:TOUT:STATE {}'.format(mode, 'ON' if trig_out != 'OFF' else 'OFF'))
    if arm_out:
        self.device.write(':ARM:{}:TOUT:SIGNAL {}'.format(mode, arm_out))
        self.device.write(':ARM:{}:TOUT:STATE {}'.format(mode, 'ON' if arm_out != 'OFF' else 'OFF'))

class B2901A_DC_Source(platypus.base.Channel):
    WORD = "VOLT"
    def __init__(self, ks, name, safestepsize=0.025, delay=0.1, limits=(0,0), *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ks = ks
        self.setname(name)

        # Set parameters used keep operation safe.
        self.setsafestepsize(safestepsize)
        self.setdelay(delay)
        self.tlast = time.monotonic()-delay

        # Perform a hardware read
        self.read()

        # Don't let the user do anything until they specify limits!
        self.setlimits(*limits)

    def read(self):
      with self.lock:
        self.v = float(self.ks.device.query(":SOUR:{}?".format(self.WORD)).strip())
        return self.getvalue()

    # Steps in 'safestepsize' steps, waiting 'delay'
    # between steps, until the voltage reaches V.
    def set(self, V):
        with self.lock:
            self.stopped = False
            if math.isclose(V, self.getvalue()): return
        while True:
            if self.stopped or self.steptowards(V): break
            platypus.update(self)
            self.wait()
        platypus.update(self)
    def getvalue(self):
      with self.lock:
        return self.v
    def validate(self, value):
      with self.lock:
        def within(v, l, h): return v>=l and v<=h
        return within(value, self.low, self.high) and within(value, -200, 200)
    def stop(self):
      with self.lock:
        self.stopped = True
    def gettype(self): return 'c'

    # Take a single step toward the target voltage.
    def steptowards(self, V):
      with self.lock:
        if not self.validate(V):
            print("Target voltage invalid")
            raise ValueError
        if not self.validate(self.getvalue()):
            print("Current voltage invalid! Change or do a read() first.")
            raise ValueError
        def sign(v): return 1.0 if v >= 0 else -1.0
        Vold = self.getvalue()
        s = sign(V - Vold) # Sign of voltage difference.
        d = abs(V - Vold)  # Magnitude of voltage difference.
        doneness = True # Did we reach the voltage this step?
        # Don't try to make a bigger step than 'safestepsize'
        if d > self.safestepsize:
            d = self.safestepsize
            doneness = False
        self.v = Vold + s*d
        self.ks.device.write(":SOUR:{}:LEV:IMM:AMPL {}".format(self.WORD,self.v))
        self.ks.device.write(":SOUR:{}:LEV:TRIG:AMPL {}".format(self.WORD,self.v))
        #self.ks.device.write(":SOUR:{} {}".format(self.WORD,self.v))
        self.tlast = time.monotonic() # Update last set operation
        return doneness
    # Returns once 'delay' seconds have elapsed since the last
    # voltage change operation through this channel.
    # If 'delay' has already elapsed, returns immediately.
    def wait(self):
      with self.lock:
        waittime = self.tlast - time.monotonic() + self.delay
#        print(self.tlast, time.monotonic(), self.delay, waittime)
        if  waittime > 0.0001:
#            print("W!")
            time.sleep(waittime)

    # Sets the maximum safe step size (in V).
    def setsafestepsize(self, safestepsize):
      with self.lock:
        self.safestepsize = abs(safestepsize)
    def getsafestepsize(self):
      with self.lock:
        return self.safestepsize

    # Sets the delay time (in seconds) between operations.
    def setdelay(self, delay):
      with self.lock:
        self.delay = abs(delay)
    def getdelay(self):
      with self.lock:
        return self.delay

    # Get and set the voltage limits for this channel.
    def getlimits(self):
      with self.lock:
        return (self.low, self.high)
    def setlimits(self, low, high):
      with self.lock:
        # Swap them if necessary
        if low > high:
            low, high = high, low
        if any(lim < -200.0 or lim > 200.0 for lim in (low, high)):
            print("Invalid limit. The voltage source can't output more than 200V!")
            raise ValueError
        if self.getvalue() < low or self.getvalue() > high:
            print("Can't set limits. Current value outside limits!")
            raise ValueError
        self.low = low
        self.high = high
class B2901A_DC_VSource(B2901A_DC_Source):
    WORD="VOLT"
    def __init__(self, ks, name, safestepsize=0.025, delay=0.1, *args, **kwargs):
        super().__init__(ks, name, safestepsize, delay, *args, **kwargs)
class B2901A_DC_ISource(B2901A_DC_Source):
    WORD="CURR"
    def __init__(self, ks, name, safestepsize=100E-12, delay=0.1, *args, **kwargs):
        super().__init__(ks, name, safestepsize, delay, *args, **kwargs)

class B2901A_Scope(platypus.base.Channel):
    def __init__(self, ks, name, points=1000, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ks = ks
        self.setname(name)
        self.ks.measure_config('CURR')
        self.ks.read_config('CURR')
        self.set_points(points)
        self.read()
    def set_points(self, N):
        self.N = N
        self.ks.trigger_config(mode="ACQ", trig_source='TIM', trig_count=self.N)
        self.ks.trace_config(points=self.N)        
    def get_points(self): return self.N
    def read(self):
      with self.lock:
        self.v = np.genfromtxt([self.ks.device.query(":READ:ARR?").strip()],delimiter=',')
        self.v.shape = (self.N,)
        platypus.update(self)
        return self.getvalue()
    def nplc(self, N):
        self.ks.device.write(':SENS:{}:NPLC {}'.format(self.WORD,N))
    def set(self, V): pass
    def getvalue(self):
      with self.lock:
        return self.v
    def validate(self, value): return True
    def stop(self): pass
    def gettype(self): return 'm'

class B2901A_DC_Meter(platypus.base.Channel):
    def __init__(self, ks, name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ks = ks
        self.setname(name)
        self.ks.device.write(':SENS:FUNC "{}"'.format(self.WORD))
        self.read()
    def read(self):
      with self.lock:
        self.v = float(self.ks.device.query(":MEAS:{}?".format(self.WORD)).strip())
        platypus.update(self)
        return self.getvalue()
    def nplc(self, N):
        self.ks.device.write(':SENS:{}:NPLC {}'.format(self.WORD,N))
    def set(self, V): pass
    def getvalue(self):
      with self.lock:
        return self.v
    def validate(self, value): return True
    def stop(self): pass
    def gettype(self): return 'm'
class B2901A_DC_VMeter(B2901A_DC_Meter): WORD = 'VOLT'
class B2901A_DC_IMeter(B2901A_DC_Meter): WORD = 'CURR'
class B2901A_DC_RMeter(B2901A_DC_Meter):
    WORD = 'RES'
    def nplc(self, N):
        self.ks.device.write(':SENS:VOLT:NPLC {}'.format(N))
        self.ks.device.write(':SENS:CURR:NPLC {}'.format(N))
class B2901A_Scope_VMeter(B2901A_Scope): WORD = 'VOLT'
class B2901A_Scope_IMeter(B2901A_Scope): WORD = 'CURR'
class B2901A_Scope_RMeter(B2901A_Scope):
    WORD = 'RES'
    def nplc(self, N):
        self.ks.device.write(':SENS:VOLT:NPLC {}'.format(N))
        self.ks.device.write(':SENS:CURR:NPLC {}'.format(N))
class B2901AVoltageSource(B2901A):
    def __init__(self, addr, vdc_name=None, idc_name=None, force_config=True):
        super().__init__(addr)

        if force_config:
            self.device.write(":SOUR:FUNC:MODE VOLT")
            self.device.write(":SOUR:FUNC DC")
            self.device.write(":SOUR:VOLT:MODE FIX")
            self.device
        else:
            # Check if it's already configured as a voltage source.
            mode = self.device.query(":SOUR:FUNC:MODE?") # VOLT
            if not mode.strip() == 'VOLT':
                raise ValueError('B2901 Not In Voltage Output Mode')
            func = self.device.query(":SOUR:FUNC?") # DC
            if not func.strip() == 'DC':
                raise ValueError('B2901 Not in DC Source Mode')
            volt_mode = self.device.query(":SOUR:VOLT:MODE?") # FIX
            if not volt_mode.strip() == 'FIX':
                raise ValueError('B2901 Not in FIXED Voltage Output Mode')

'''
ks = B2901A('USB0::0x0957::0x8B18::MY51143771::INSTR')
#ks.device.write("*RST")
#time.sleep(5.0)
#ks.measure_config('VOLT', 'CURR')
#ks.read_config('CURR')
#print(ks.check_error())
#ks.trace_config(clear=True, points=100, feed='SENS')

print(ks.check_error())
ks.device.write("*RST")
#ks.device.write(":SOUR:FUNC:TRIG:CONT OFF")
ks.device.write(":SOUR:FUNC:MODE VOLT")
#ks.device.write(":SOUR:FUNC DC")
#ks.device.write(":SOUR:VOLT:MODE LIST")
#ks.device.write(":SOUR:VOLT:MODE FIX")
#ks.device.write(":SOUR:VOLT:TRIG 10E-3")
ks.device.write(":SOUR:LIST:VOLT {}".format(','.join(str(x) for x in np.array([-1E-3,1E-3]))))
ks.device.write(':SENS:FUNC "CURR", "VOLT"')
ks.device.write(':SENS:CURR:NPLC 0.1')
ks.device.write(':SENS:CURR:PROT 1E-7')
print(ks.check_error())
'''


class B2902B_DC_Source(platypus.base.Channel):
    WORD = "VOLT"
    def __init__(self, ks, name, channel = 1,safestepsize=0.025,delay=0.1, limits=(0,0), *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ks = ks
        self.setname(name)
        self.channel = channel

        # Set parameters used keep operation safe.
        self.setsafestepsize(safestepsize)
        self.setdelay(delay)
        self.tlast = time.monotonic()-delay

        # Perform a hardware read
        self.read()

        # Don't let the user do anything until they specify limits!
        self.setlimits(*limits)

    def read(self):
      with self.lock:
        self.v = float(self.ks.device.query(":SOUR{}:{}?".format(self.channel, self.WORD)).strip())
        return self.getvalue()

    # Steps in 'safestepsize' steps, waiting 'delay'
    # between steps, until the voltage reaches V.
    def set(self, V):
        with self.lock:
            self.stopped = False
            if math.isclose(V, self.getvalue()): return
        while True:
            if self.stopped or self.steptowards(V): break
            platypus.update(self)
            self.wait()
        platypus.update(self)
    def getvalue(self):
      with self.lock:
        return self.v
    def validate(self, value):
      with self.lock:
        def within(v, l, h): return v>=l and v<=h
        return within(value, self.low, self.high) and within(value, -200, 200)
    def stop(self):
      with self.lock:
        self.stopped = True
    def gettype(self): return 'c'

    # Take a single step toward the target voltage.
    def steptowards(self, V):
      with self.lock:
        if not self.validate(V):
            print("Target voltage invalid")
            raise ValueError
        if not self.validate(self.getvalue()):
            print("Current voltage invalid! Change or do a read() first.")
            raise ValueError
        def sign(v): return 1.0 if v >= 0 else -1.0
        Vold = self.getvalue()
        s = sign(V - Vold) # Sign of voltage difference.
        d = abs(V - Vold)  # Magnitude of voltage difference.
        doneness = True # Did we reach the voltage this step?
        # Don't try to make a bigger step than 'safestepsize'
        if d > self.safestepsize:
            d = self.safestepsize
            doneness = False
        self.v = Vold + s*d
        self.ks.device.write(":SOUR{}:{}:LEV:IMM:AMPL {}".format(self.channel, self.WORD,self.v))
        self.ks.device.write(":SOUR{}:{}:LEV:TRIG:AMPL {}".format(self.channel, self.WORD,self.v))
        #self.ks.device.write(":SOUR:{} {}".format(self.WORD,self.v))
        self.tlast = time.monotonic() # Update last set operation
        return doneness
    # Returns once 'delay' seconds have elapsed since the last
    # voltage change operation through this channel.
    # If 'delay' has already elapsed, returns immediately.
    def wait(self):
      with self.lock:
        waittime = self.tlast - time.monotonic() + self.delay
#        print(self.tlast, time.monotonic(), self.delay, waittime)
        if  waittime > 0.0001:
#            print("W!")
            time.sleep(waittime)

    # Sets the maximum safe step size (in V).
    def setsafestepsize(self, safestepsize):
      with self.lock:
        self.safestepsize = abs(safestepsize)
    def getsafestepsize(self):
      with self.lock:
        return self.safestepsize

    # Sets the delay time (in seconds) between operations.
    def setdelay(self, delay):
      with self.lock:
        self.delay = abs(delay)
    def getdelay(self):
      with self.lock:
        return self.delay

    # Get and set the voltage limits for this channel.
    def getlimits(self):
      with self.lock:
        return (self.low, self.high)
    def setlimits(self, low, high):
      with self.lock:
        # Swap them if necessary
        if low > high:
            low, high = high, low
        if any(lim < -200.0 or lim > 200.0 for lim in (low, high)):
            print("Invalid limit. The voltage source can't output more than 200V!")
            raise ValueError
        if self.getvalue() < low or self.getvalue() > high:
            print("Can't set limits. Current value outside limits!")
            raise ValueError
        self.low = low
        self.high = high
class B2902B_DC_VSource(B2902B_DC_Source):
    WORD="VOLT"
    def __init__(self, ks, name, channel, safestepsize=0.025, delay=0.1, *args, **kwargs):
        super().__init__(ks, name, channel, safestepsize, delay, *args, **kwargs)
class B2902B_DC_ISource(B2902B_DC_Source):
    WORD="CURR"
    def __init__(self, ks, name, channel, safestepsize=100E-12, delay=0.1, *args, **kwargs):
        super().__init__(ks, name, channel, safestepsize, delay, *args, **kwargs)
        

class B2902B_DC_Meter(platypus.base.Channel):
    def __init__(self, ks, name, channel, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ks = ks
        self.setname(name)
        self.channel = channel
        self.ks.device.write(':SENS{}:FUNC "{}"'.format(self.channel, self.WORD))
        self.read()
    def read(self):
      with self.lock:
        self.v = float(self.ks.device.query(":MEAS:{}? (@{})".format(self.WORD,self.channel)).strip())
        #self.v = float(self.ks.device.query(":MEAS:CURR? (@1)").strip())
        platypus.update(self)
        return self.getvalue()
    def nplc(self, N):
        self.ks.device.write(':SENS{}:{}:NPLC {}'.format(self.channel, self.WORD,N))
    def set(self, V): pass
    def getvalue(self):
      with self.lock:
        return self.v
    def validate(self, value): return True
    def stop(self): pass
    def gettype(self): return 'm'
class B2902B_DC_VMeter(B2902B_DC_Meter): WORD = 'VOLT'
class B2902B_DC_IMeter(B2902B_DC_Meter): WORD = 'CURR'
class B2902B_DC_RMeter(B2902B_DC_Meter):
    WORD = 'RES'
    def nplc(self, N):
        self.ks.device.write(':SENS{}:VOLT:NPLC {}'.format(self.channel, N))
        self.ks.device.write(':SENS{}:CURR:NPLC {}'.format(self.channel, N))
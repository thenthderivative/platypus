import platypus
from platypus.channelmixins import RangeLimited, SelfUpdating, Throttled

class DummyDac(object):
    def __init__(self, *args, **kwargs):
        self.vs = [None]+[0.0]*48
    def getDCVoltage(self, N):
        return self.vs[N]
    def setDCVoltage(self, N, v):
        self.vs[N] = v
    def getVoltageRange(self, N): return 10.0

class _DacChannelDCRaw(platypus.base.Channel):
    def __init__(self, instrument, number, *args, **kwargs):
        self.instr = instrument
        self.number = number
        self.setname("Channel {}".format(number))
        self.read()
        super().__init__(self, *args, **kwargs)
    def set(self, v):
        self.instr.setDCVoltage(self.number, v)
        self.v = v
    def read(self):
        self.v = self.instr.getDCVoltage(self.number)
        return self.getvalue()
    def validate(self, v):
        return abs(v) <= self.instr.getVoltageRange(self.number)
    def getvalue(self):
        return self.v
    def gettype(self): return 'c'

# Extend it with the relevant mixins, and OH YES THE ORDER DEFINITELY MATTERS.
class DacChannelDC(RangeLimited, Throttled, SelfUpdating, _DacChannelDCRaw):
    def __init__(self, instrument, number, *args, **kwargs):
        if 'limits' not in kwargs:
            r = instrument.getVoltageRange(number)
            kwargs['limits'] = (-r,r)
        if not any([(k in kwargs) for k in ['stepsize', 'delay', 'rate']]):
            kwargs['stepsize'] = 10E-3 # 10 mV
            kwargs['delay'] = 0.1      # 0.1 seconds
        super().__init__(instrument, number, *args, **kwargs)

class QDAC_DC(object):
    def __init__(self, instrument):
        self.instr = instrument
        # List of channels. First one has to be None because they insist on indexing from one. How obnoxious! ;)
        self.channel_list = [None]+[DacChannelDC(self.instr, i) for i in range(1,49)]
    def __getitem__(self, K):
        try: # Assume K can index a list, so it can be an integer or a slice.
            if isinstance(K, slice) and K.start is None:
                K = slice(1, K.stop, K.step)
            return self.channel_list[K]
        except TypeError:
            # Ok apparently it can't, so it might be a string or another iterable.
            if isinstance(K, str):
                candidates = [c for c in self.channel_list[1:] if c.getname() == K]
                if len(candidates) == 0: raise KeyError(K)
                if len(candidates) == 1: return candidates[0]
                return candidates
            # It's not a string, so return a list of channels corresponding to the given numerical keys.
            return [self[k] for k in K]
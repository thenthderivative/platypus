# -*- coding: utf-8 -*-
"""
Created on March 22 2016

@author: Louis Gaudreau
         & Jason Phoenix
"""

from platypus import base
import pyvisa
import pyvisa
import time

#Give a name to the device in the next line
class Piezo_Using_Fit(base.Channel):
    
    '''
    Controls the position of the specified nano positioning stage.

    This requires that the ANC300, voltmeter (even better: RMS oscilloscope) channel and the
    Pathway7334 be declared in the config file:

    scopeRMS = MSO5204B_RMS('scopeRMS',scope)
    Switcher1 = Pathway7334('ASRL17::INSTR')
    Switcher2 = Pathway7334('ASRL17::INSTR')
    Piezo_Controller = ANC300('ASRL3::INSTR')

    To declare it: Piezo_x = Piezo(name, ID, scopeRMS, Switcher1, Switcher2, Piezo_Controller)

    Piezo ID must be 'X', 'Y', 'Z' or 'R'. This will allow the program to identify
    the device more easily.    
    '''    
    
    def __init__(self, name, piezo_ID, oscilloscopeRMS, read_switcher, V_switcher, piezo_controller, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided channel name for this instrument
        self.setname(name)

        # For every channel, write self.mychannel = mychannel
        self.piezo_controller = piezo_controller
        self.oscilloscopeRMS = oscilloscopeRMS
        self.V_switcher = V_switcher
        self.read_switcher = read_switcher
        self.piezo_ID = piezo_ID

        #Set the port to use for both switchers and note the ANC300 axis
        #(x=['A',4], y=['B',3], z=['C',2], theta = ['D',1])
        #Also specify the file containing the fit data.
        if self.piezo_ID == 'X':
            self.controller_axis = 4
            self.port = 'A'
            self.fit_file = r'C:\Data\20160609\20160609_034\20160609_034.fit'
        elif self.piezo_ID == 'Y':
            self.controller_axis = 3
            self.port = 'B'
            self.fit_file = r'C:\Data\20160609\20160609_023\20160609_023.fit'
        elif self.piezo_ID == 'Z':
            self.controller_axis = 2
            self.port = 'C'
            self.fit_file = r'C:\Data\20160609\20160609_019\20160609_019.fit'
        elif self.piezo_ID == 'R':
            self.controller_axis = 1
            self.port = 'D'
            self.fit_file = r'C:\Data\20160610\20160610_018\20160610_018.fit'

        # Open .fit file and load position fit function parameters into list, with
        # one line of text per list item
        with open (self.fit_file, "r") as myfile:
            fit_data = myfile.readlines()
        # Removing all '\n' (next line markers), and pick out fit data.
        self.fit_data = [x.replace('\n', '') for x in fit_data]
        self.vmax = float(fit_data[4][7:])
        self.vmin = float(fit_data[5][7:])
        self.slope = float(fit_data[6][8:])
        self.intercept = float(fit_data[7][12:])
        # This value is the location where the resistive encoder for the rotator
        # ends. It is the angle in degrees where the measured voltage drops to 0.
        if self.piezo_ID == 'R':
            self.end_angle = float(fit_data[10][28:])

        #Initialize the position to 0.0
        self.position = 0.0

        #This variable is used only in the self.set and self.move_by functions.
        #It is used to prevent messages being displayed on screen when these
        #functions are used in experiments. (Set to zero in experiment code if
        #on-screen updates stating new piezo position are not desired.)
        self.display_messages = 1

    def validate(self):
        return True
        #This function ensures that the values input by the user are valid.


    def step_by(self,steps):
        '''
        Moves the piezo by a specific number of steps. Step size determined
        by ANC300 voltage. Does not update position value, and ANC300 voltage
        is set automatically by this function.

        Inputs:
            Number Of Steps (int)
        Returns:
            None
        '''
        #Set piezo controller voltage.
        self.piezo_controller.set_voltage(self.controller_axis,25)
        #Step piezo by a certain number of steps.
        self.piezo_controller.step_by(self.controller_axis,steps)

        
    def set(self, new_position):
        '''
        Moves the piezo to the specific desired position.

        Linear Piezos: Positions given in mm, as measured from the center
                       of the range of motion. Total range of motion is
                       approximately -2.5mm to 2.5mm. Accuracy is within
                       about 0.015mm due to noise in voltage measurement.

        Rotating Piezo: Positions given in degrees. Ranges between 0 and
                        ~335 degrees due to position encoder length.
                        Accuracy may vary up to ~6 degrees due to non-
                        linearity of encoder resistivity.

        Inputs:
            New Position (float)
        Returns:
            None  
        '''

        #For linear piezos, if specified position is greater than max range value, message
        #is displayed on screen and piezo is merely moved to max position.
        if new_position > 2.5 and self.piezo_ID != 'R':
            print('')
            print('Piezo motion range exceeded. Piezo will move to edge of motion range.')
            print('')
            self.move_to_end('max')

        #Piezo is moved to min position of specified position is too low, and message is
        #displayed on screen.
        elif new_position < -2.5 and self.piezo_ID != 'R':
            print('')
            print('Piezo motion range exceeded. Piezo will move to edge of motion range.')
            print('')
            self.move_to_end('min')
            
        #Otherwise, for the linear piezos:
        elif self.piezo_ID != 'R':

            #Display a message indicating that piezo is currently in motion.
            if self.display_messages == 1:
                print("")
                print("Please wait while piezo is repositioned. May take up to a minute, depending on travel distance.")
                print("(Wait for new position to be displayed on screen before proceeding.)")
                print("")

            #Read current position.
            current_position_voltage = self.read()
            #Determine from fit parameters the position encoder voltage value of the desired position.
            new_position_voltage = new_position*self.slope + self.intercept

            #Now move piezo into desired position.
            current_position_voltage = self.motion(new_position_voltage,current_position_voltage,1)

            #Display new location based on measured voltage and fit line.
            if self.display_messages == 1:
                print("")
                print("New Location Based On Fit Data: "+str(round((current_position_voltage-self.intercept)/self.slope,7))+"mm")
                print("")

            #Set position value to be new measured voltage.
            self.position = current_position_voltage

        #Otherwise, for the rotator, if the specified position is between 0 and 360 degrees:
        elif 0 <= new_position <= 360:

            #If the specified position is in the range which the position does not cover,
            #display message indicating that the rotator cannot be set to that position.
            if self.end_angle < new_position < 360:
                print('')
                print('Input invalid. Position encoder exists only between 0 and '+str(round(self.end_angle,2))+' degrees.')
                print('')
            else:
                #If the new position is 360, set it to be zero.
                if new_position == 360:
                    new_position = 0

                #Display message indicating that the rotator is in motion.
                if self.display_messages == 1:
                    print("")
                    print("Please wait while piezo is repositioned. May take up to a minute, depending on travel distance.")
                    print("(Wait for new position to be displayed on screen before proceeding.)")
                    print("")

                #Read current position.
                current_position_voltage = self.read()
                #Determine position encoder voltage value of new position from fit.
                new_position_voltage = new_position*self.slope + self.intercept

                #Now move piezo into desired position.
                current_position_voltage = self.motion(new_position_voltage,current_position_voltage,1)

                #Display new location based on measured voltage and fit line.
                if self.display_messages == 1:
                    print("")
                    print("New Location Based On Fit Data: "+str(round((current_position_voltage-self.intercept)/self.slope,7))+"mm")
                    print("")

        #If, for rotator, the specified angle is outside the 0-360 degree range, display error message.
        #(Only allowing a 0-360 range is easiest, due to gap in encoder.)
        else:
            print('')
            print('Input invalid. Piezo position must be between 0 and '+str(round(self.end_angle,2))+' degrees.')
            print('')
            

    def move_by(self,distance):
        '''
        Moves the piezo by the specified distance in mm. (Total range
        of motion is approximately -2.5mm to 2.5mm.) Accuracy is within
        about 0.015mm due to noise in voltage measurement.

        NOTE: If specified distance is outside range of piezo, programme will
              move piezo to maximum/minimum.

        Inputs:
            Distance In mm (float)
        Returns:
            None  
        '''

        #First, read current position.
        current_position_voltage = self.read()
        #Get distance in terms of position encoder voltage.
        voltage_distance = distance*self.slope
        #Determine position encoder voltage value of new position from fit.
        new_position_voltage = current_position_voltage + voltage_distance
        #Determine new position in mm.
        new_position = (new_position_voltage - self.intercept)/self.slope

        #For linear piezos, if specified distance is greater than available range of motion,
        #display message indicating this and move piezo to max position.
        if new_position > 2.5 and self.piezo_ID != 'R':
            print('')
            print('Piezo motion range exceeded. Piezo will move to edge of motion range.')
            print('')
            self.move_to_end('max')
        #For linear piezos, if specified distance is less than -2.5mm (outside motion range),
        #display message indicating this and move piezo to min position.
        elif new_position < -2.5 and self.piezo_ID != 'R':
            print('')
            print('Piezo motion range exceeded. Piezo will move to edge of motion range.')
            print('')
            self.move_to_end('min')
        #For rotator, if specified distance is too large, request a smaller distance (this will
        #prevent the rotator from just moving continuously for very long periods if a very large
        #value is input).
        elif self.piezo_ID == 'R' and new_position > 720 or self.piezo_ID == 'R' and new_position < -720:
            print('')
            print('Specified angle too large. Please specify an angle between -720 and 720 degrees.')
            print('')
        #If input value is within acceptable limits:
        else:
            #Display message indicating piezo is in motion.
            if self.display_messages == 1:
                print("")
                print("Please wait while piezo is repositioned. May take up to a minute, depending on travel distance.")
                print("(Wait for new position to be displayed on screen before proceeding.)")
                print("")
            
            #Move piezo desired distance.
            current_position_voltage = self.motion(new_position_voltage,current_position_voltage,1)

            #Display new location based on measured voltage and fit line.
            if self.display_messages == 1:
                print("")
                print("New Location Based On Fit Data: "+str(round((current_position_voltage-self.intercept)/self.slope,7))+"mm")
                print("")

            #Set position value to be new measured voltage.
            self.position = current_position_voltage


    def move_to_end(self,end):
        '''
        Moves the piezo to the end of its range, either 'max' or 'min', as
        specified by the input. For rotator, both 'max' and 'min' have the
        effect of setting the device to position zero.

        Inputs:
            End (str)
        Returns:
            None  
        '''
        #Indicate piezo is being repositioned.
        if self.display_messages == 1:
            print("")
            if self.piezo_ID == 'R':
                print("Please wait while piezo is repositioned. May take several minutes, depending on travel distance.")
            else:
                print("Please wait while piezo is repositioned. May take up to a minute, depending on travel distance.")
            print("(Wait for new position to be displayed on screen before proceeding.)")
            print("")

        #For the linear piezos:
        if self.piezo_ID != 'R':
            #Determine direction in which piezo will be travelling, as well as the
            #theoretical endpoint (may not be exact endpoint due to measurement
            #noise, etc.).
            if end == 'max':
                direction = 1
                theoretical_new_position = self.vmax
            elif end == 'min':
                direction = -1
                theoretical_new_position = self.vmin

            #Determine current position voltage.
            current_position = self.read()

            #In order to avoid hitting the edge of the piezo's range, we'll first make
            #a jump to a location 0.1mm away.
            new_position = (abs((theoretical_new_position - self.intercept)/self.slope) - 0.1)*direction*self.slope + self.intercept
            #Move to new position.
            current_position = self.motion(new_position,current_position,0)

            # Set initial number of steps by which to move the attocube.
            steps = direction*1000

        #For the rotator:
        else:

            #Determine current position.
            current_position = self.read()

            #Both max and min have the effect of setting the rotator to zero.
            #They both go in the negative direction because I haven't yet written
            #up the code for moving in the other direction.
            if end == 'max':
                direction = -1
            if end == 'min':
                direction = -1

            # Set initial number of steps by which to move the positioner.
            if current_position < 0.01:
                steps = direction*100
            elif current_position < 0.02:
                steps = direction*1000
            else:
                steps = direction*3000

        #Now move to end.
        self.move_to_end_rough(current_position,steps)

        #Display new location based on measured voltage and fit line.
        if self.display_messages == 1:
            print("")
            print("New Location Based On Fit Data: "+str(round((voltage_new-self.intercept)/self.slope,7))+"mm")
            print("")

        #Set position value to be new measured voltage.
        self.position = voltage_new

    def move_to_end_rough(self,current_position,steps):
        '''
        Moves the piezo to the end of its range, either 'max' or 'min', as
        specified by the input. For rotator, both 'max' and 'min' have the
        effect of setting the device to position zero.

        Inputs:
            Current position voltage (float)
            Steps per move iteration (int)
        Returns:
            None  
        '''

        #Set up the variables which will check for
        voltage_prev1 = current_position
        voltage_prev2 = 0

        # Then continue to move piezo in negative direction until end of range is reached.
        while (steps != 0):
            #Move piezo.
            self.step_by(steps)
            #Wait for oscilloscope to register new voltage value.
            time.sleep(1.5) #in seconds
            #Read voltage value
            voltage_new = self.read()
            #For linear piezos, if the new voltage value is almost identical to the voltage
            #measured two loop iterations ago, exit loop.
            if self.piezo_ID != 'R' and abs(voltage_new - voltage_prev2)/(self.vmax - self.vmin) < 0.0039:
                steps = 0
            #For the rotator:
            elif self.piezo_ID == 'R':
                #If the measured voltage enters the region where the encoder is absent,
                #end loop.
                if voltage_new < 0.0029:
                    steps = 0
                #Alter step size if approaching zero position
                elif voltage_new < 0.004:
                    steps = direction*100
                elif voltage_new < 0.02:
                    steps = direction*1000
            #Update variables recording previous voltages.
            voltage_prev1 = voltage_new
            voltage_prev2 = voltage_prev1

        #For the rotator, once the end of it's range is reached
        if self.piezo_ID == 'R':
            while (steps != 0):
                #Move piezo.
                self.step_by(10)
                #Wait for oscilloscope to register new voltage value.
                time.sleep(1.5) #in seconds
                #Read voltage value
                voltage_new = self.read()
                #If the new voltage value is almost identical to the voltage
                #measured two loop iterations ago, exit loop.
                if new_voltage > 0.0029:
                    self.step_by(-10)
                    steps = 0

    def motion(self,new_position_voltage,current_position_voltage,precise):
        '''
        Moves piezo to new location, either the exact location or in the
        neighbourhood of the specified location.

        Inputs:
            New Position Voltage (float)
            Current Position Voltage (float)
        Returns:
            Position Voltage (float)
        '''
        #Set piezo controller voltage.
        self.piezo_controller.set_voltage(self.controller_axis,25)
        steps = 1
        while (steps != 0):
            #Number of steps the piezo moves is determined by the difference in the
            #current and desired position voltages.
            if abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.24:
                steps = 2000
            elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) > 0.12:
                steps = 1000
            elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.016:
                steps = 100
            elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.0032:
                steps = 10
            else:
                #If it is desired that the piezo move to a specific exact location,
                #such as for .set() and .move_by(), then start stepping in increments
                #of one step.
                if precise == 1: steps = 1
                #For cases such as .move_to_end(), precision is not required.
                else: steps = 10
            #Make sure direction of movement is correct.
            if new_position_voltage < current_position_voltage:
                steps = -steps
            
            #Move piezo.
            self.step_by(steps)
            #Wait for oscilloscope to register new voltage values
            time.sleep(1.5) #in seconds
            #Read voltage value
            current_position_voltage = self.read()

            #If constant updates of position are desired on-screen during movement,
            #uncomment the following line. (Presumably to be only used during testing.)
            #print("New Location: "+str((current_position_voltage-self.intercept)/self.slope)+"mm")

            #Exit loop once measured voltage is within measurement uncertainty
            #of new desired position.
            if precise == 1 and abs(new_position_voltage - current_position_voltage) < 0.0001:
                steps = 0
            elif precise == 0 and abs(new_position_voltage - current_position_voltage) < 0.0008:
                steps = 0

        #Return the current position value.
        return current_position_voltage
    
    def stop(self):
        self.piezo_controller.stop()
        
    
    def read(self):
        #Set switchers to proper ports if they aren't already there.
        if self.V_switcher.get_port() != self.port:
            self.V_switcher.set_port(self.port)
            self.read_switcher.set_port(self.port)
            time.sleep(1)
        #Read position voltage
        self.position = self.oscilloscopeRMS.read()
        time.sleep(0.1)
        #Return position value.
        return self.position

    def get_position(self):
        '''
        Retrives piezo position using current voltage value and position
        encoder fitting parameters.

        Inputs:
            None
        Returns:
            None
        '''
        #First, read current position voltage.
        current_position_voltage = self.read()
        # Determine position from fitting parameters.
        return round((current_position_voltage - self.intercept)/self.slope,7)
        
    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.position
        
        
    def gettype(self):
        #If this is a control channel, 'c' should be returned. If it is a
        #measurement channel, 'm' should be returned.
        return 'c'

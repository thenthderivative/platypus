'''
Copyright (c) 2019, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@Author: Aviv Padawer-Blatt
'''

import platypus
import pyvisa
import pyvisa
import time
from numpy import linspace
import math

import threading


class DACChannel(platypus.base.Channel):

    def __init__(self, dac, portNum, name, rangeLimit, safestepsize=0.020, delay=0.01666, saferate=0.3, *args, **kwargs):
        # Run the (mandatory) superclass constructor.
        super().__init__(*args, **kwargs)

        # Remember the port number this channel corersponds to.
        self.dac = dac
        self.portNum = portNum

        # Set the name of the channel.
        self.setname(name)

        # Set the range of the channel.
        self.rangeLimit = rangeLimit

        # Set parameters used to keep operation safe.
        self.setsafestepsize(safestepsize)
        self.setdelay(delay)
        self.setrate(saferate)
        self.tlast = time.monotonic()- delay

        # Don't let the user do anything until they specify limits!
        self.low = 0.0
        self.high = 0.0

        # Initialize power supply by reading its value.
        self.voltage = self.dac.getvoltage(self.portNum)

    ## Standard methods (see platypus documentation)

    def validate(self, value):
        # Check that voltage is within upper and lower limits.
        with self.lock:
            def within(v, l, h): return v>=l and v<=h
            return within(value, self.low, self.high)\
                   and within(value, *self.getrange())

    def stop(self):
        # Halt all processes where they are.
        self.stopped = True

    def gettype(self):
        # Control channel, so 'c' is returned.
        return 'c'


    
    
    def aread(self): return self.read()
    
    def read(self):
        # Updates stored voltage in channel and returns voltage.
        with self.lock:
            return self.getvalue()


    def set(self, V):
        # Steps in 'safestepsize' steps, waiting 'delay'
        # between steps, until the voltage reaches V.
        
        with self.lock:
            self.stopped = False
            if math.isclose(V, self.getvalue()): return
        while True:
            with self.lock:
                if self.stopped or self.steptowards(V): break
                platypus.update(self)
                self.wait()
        platypus.update(self)

    def getvalue(self):
        # Returns last read voltage.
        with self.lock:
            return self.voltage


    ## Extra non-standard methods

    def steptowards(self, V):
        # Take a single step toward the target voltage.
        
        with self.lock:
            if not self.validate(V):
                print("Target voltage invalid")
                raise ValueError
            if not self.validate(self.getvalue()):
                print("Current voltage invalid! Change or do a read() first.")
                raise ValueError
            def sign(v): return 1.0 if v >= 0 else -1.0
            Vold = self.getvalue()
            s = sign(V - Vold) # Sign of voltage difference.
            d = abs(V - Vold)  # Magnitude of voltage difference.
            doneness = True # Did we reach the voltage this step?
            # Don't try to make a bigger step than 'safestepsize'
            if d > self.safestepsize:
                d = self.safestepsize
                doneness = False
            self.voltage = Vold + s*d
            self.dac.setvoltage(self.portNum, self.voltage)
            self.tlast = time.monotonic() # Update last set operation
            return doneness
        

    # Set and get voltage limits for this channel.
    def setlimits(self, low, high):
        with self.lock:
            # Swap limits if necessary.
            if low > high:
                low, high = high, low
            # Check that desired limits are within range.
            if any(lim < -1.0*self.rangeLimit or lim > self.rangeLimit for lim in (low, high)):
                print("Invalid limit. The DAC can't output more than {}V!".format(self.rangeLimit))
                raise ValueError
            # Check that current voltage is within desired limits
            if self.getvalue() < low or\
               self.getvalue() > high:
                print("Can't set limits. Current value outside limits!")
                raise ValueError

            self.low = low
            self.high = high

    def getlimits(self):
        # Return current voltage limits.
        with self.lock:
            return (self.low, self.high)


    def getrange(self):
        # Retrieve voltage range of channel (specific to dac)
        # as a list of floats.
        with self.lock:
            return [-1.0*self.rangeLimit, self.rangeLimit]


    def wait(self):
        # Returns once 'delay' seconds have elapsed since the last
        # voltage change operation through this channel.
        # If 'delay' has already elapsed, returns immediately.
        
        with self.lock:
            waittime = self.tlast - time.monotonic() + self.delay
#           print(self.tlast, time.monotonic(), self.delay, waittime)
            if waittime > 0.0001:
#               print("W!")
                time.sleep(waittime)
        

    # Set and get maximum safe step size (in V).
    def setsafestepsize(self, safestepsize):
        with self.lock:
            self.safestepsize = abs(safestepsize)

    def getsafestepsize(self):
        with self.lock:
            return self.safestepsize
        

    # Set and get the delay time (in s) between operations.
    def setdelay(self, delay):
        with self.lock:
            self.delay = abs(delay)

    def getdelay(self):
        with self.lock:
            return self.delay

    # Set and get max safe ramp rate (in V/s).
    def setrate(self, rate):
        with self.lock:
            self.rate = abs(rate)

    def getrate(self):
        with self.lock:
            return self.rate
    



class DAC(object):

    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor.
        super().__init__(*args, **kwargs)

        # Create the instrument. Ensure that baud rate, data bits, and other
        # settings are correct for the instrument being used.
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                baud_rate = 115200,
                read_termination = '\r',
                write_termination = '\r')
        self.inst.timeout = 2500

        
    def serialNum(self):
        raise NotImplementedError
    

    '''
    # For debug. Returns a list of strings describing the active status bits.
    def stb(self, stb):
        stbdict = {
            1:"Triggered",
            2:"End of Trigger Sequence",
            4:"Ready",
            8:"Error",
            16:"Message Available",
            32:"Event Status Register Bit [Message Available, etc.]",
            64:"Service Request Bit",
            128:"DMA Underrun"
        }
        return tuple(stbdict[stb&1<<i] for i in range(8) if stb&1<<i)

    def geterrors(self):
        err = int(self.query("E?X")[1:])
        errdict = {
            #0:"No Error",
             1:"Invalid Device Dependent Command",
             2:"Invalid Device Dependent Command Option",
             4:"Command Conflict",
             8:"Calibration Enable",
            16:"Trigger Overrun",
            32:"Non-volatile Setup",
            64:"Non-volatile Calibration Constants",
           128:"DMA Underrun"
        }
        return [errdict[err & 1<<i] for i in range(8) if err & 1<<i]
        

    def read(self, block=True):
        time.sleep(0.05)
        stb = self.inst.stb
        while block and not stb & 16: #SPoll
            time.sleep(0.05)
            stb = self.inst.stb
#            print("Wait to read: ", DAC488HR.stb(stb))
        msg = self.inst.read()
#        print("Read: ", msg)
        return msg

    def write(self, message, block=True):
        time.sleep(0.05)
        stb = self.inst.stb
        while block and not stb & 4: #SPoll
            if stb & 8:
                errors = self.geterrors()
                raise Exception('Errors in DAC (address {}): [{}]'.format(self.inst.primary_address, errors))
            time.sleep(0.05)
            stb = self.inst.stb
#            print("Wait to write: ", DAC488HR.stb(stb))
#        print("Writing: ", message)
        self.inst.write(message)

    '''

    # Displays set-voltage on LCD screen.
    def writevoltage(self, portNum, voltage):
        raise NotImplementedError

    # Sends command and reads reply from device.
    def query(self, msg):
        back = self.inst.query(msg)
        if len(back) == 7 and back[0:5] == "ERROR":
            if back[5:7] == "01":
                return "Command not recognized."
            elif back[5:7] == "02":
                return "Channel number out of range."
            elif back[5:7] == "03":
                return "Channel voltage out of range."
        else:
            return back

    # Converts input voltage to scale factor.
    # for device or scale to voltage.
    def voltagetoscale(self, voltage):
        raise NotImplementedError
    def scaletovoltage(self, scale):
        raise NotImplementedError

    # Displays and sets voltage of port on DAC.
    def setvoltage(self, portNum, voltage):
        scale = self.voltagetoscale(voltage)        
        self.writevoltage(portNum, voltage)        
        msg = "{} CH0{} {:.7f}".format(self.serialNum(), portNum, scale)
#        print(msg)
        return self.query(msg)

    # Queries voltage for port.
    def getvoltage(self, portNum):
        msg = "{} U0{}".format(self.serialNum(), portNum)
        voltage = float(self.query(msg)[0:7].replace(',','.'))  # returns voltage as string
        return voltage

    # Spawns channel for specific DAC and port, with a given name.
    def getchannel(self, portNum, name, *args, **kwargs):
        raise NotImplementedError



## Two DACs differ in specific ways:
    # Serial numbers
    # Number of channels
    # Voltage range (+/-2 V and +/-5 V)
    # How to display set voltage on LCD screen
# Thus, two separate classes, one for each DAC, inherit from
# the DAC class, each of which spawn their own channels
class DACHV178(DAC):
    def serialNum(self):
        msg = "IDN"
        return self.query(msg)[0:5]
    
    def voltagetoscale(self, voltage):
        return (voltage + 2.0)/4.0

    def scaletovoltage(self, scale):
        return 4.0 * scale - 2.0

    def writevoltage(self, portNum, voltage):
        msg = "{} DIS L CH0{} {:.4f} V".format(self.serialNum(), portNum, voltage)
        return self.query(msg)

    def getchannel(self, portNum, name, *args, **kwargs):
        assert portNum in range(1, 5), "Invalid port number for this function"
        return DACChannel(self, portNum, name, 2.0, *args)


class DACHV179(DAC):
    def serialNum(self):
        msg = "IDN"
        return self.query(msg)[0:5]
    
    def voltagetoscale(self, voltage):
        return (voltage + 5.0)/10.0

    def scaletovoltage(self, scale):
        return 10.0 * scale - 5.0

    def writevoltage(self, portNum, voltage):
        msg = "{} DIS L CH0{} {:.3f} V".format(self.serialNum(), portNum, voltage)
        return self.query(msg)

    def getchannel(self, portNum, name, *args, **kwargs):
        assert portNum in range(1, 9), "Invalid port number for this function"
        return DACChannel(self, portNum, name, 5.0, *args)

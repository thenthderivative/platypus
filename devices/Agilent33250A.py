# -*- coding: utf-8 -*-
"""
Created on Wed OCt 21 10:27:47 2015

@author: GaudreauL
"""

from platypus import base
import pyvisa
import platypus


class Agilent33250A(object):


    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)


        # Create the instrument
        self.rm = pyvisa.ResourceManager()
        self.inst = self.rm.open_resource(address,
                baud_rate = 9600,
                data_bits = 8,
                stop_bits = pyvisa.constants.StopBits.one,
                parity=pyvisa.constants.Parity.none,
                read_termination = '\n',
                write_termination = '\r\n'
                )
      
        self.inst.timeout = 10000
    def apply_sine(self, frequency, Vpp):
        '''
        Documentation
        Applies a sine wave with 'Vpp' peak to peak voltage at 'frequency' in KHz
        Input:
        frequency in kHz (float)
        Vpp in volts (float)
        Return:
        None
        '''
        frequency = str(frequency)
        Vpp = str(Vpp)
        self.inst.write('APPL:SIN '+ frequency +' KHZ, '+Vpp+' VPP')


    def apply_square(self, frequency, Vpp):
        '''
        Documentation
        Applies a square pulse train (50% duty cycle) with 'Vpp' peak to peak voltage at 'frequency' in KHz
        Input:
        frequency in kHz (float)
        Vpp in volts (float)
        Return:
        None
        '''
        frequency = str(frequency)
        Vpp = str(Vpp)
        self.inst.write('APPL:SQU '+ frequency +' KHZ, '+Vpp+' VPP')


    def validate(self, frequency, Vpp):
        '''
        Documentation
        Veriifies that the frequency and Vpp are within the instruments limit.
        Input:
        frequency in Hz (float)
        Vpp in volts (float)
        Return:
        valid (boolean)
        '''
        valid = True
        if (frequency > 80000000 or Vpp > 10):
            valid = False
            print('INVALID: Maximum frequency:80MHz, Maximum Vpp:10V.')
        return valid

    def on(self):
        self.inst.write('output:state on')

    def off(self):
        self.inst.write('output:state off')

        

class Agilent33250A_CHAN(base.Channel):
    '''
    This class is to be able to create multiple channels from the same instrument.
    You need to create a child of this class for every channel you need.
    '''
    def __init__(self, name, awg, *args, **kwargs):
        self.setname(name)
        self.awg = awg
        self.last_value = None
        super().__init__(*args, **kwargs)
    def validate(self, value): return True
    def set(self, v): pass
    def stop(self):pass
    def getvalue(self):
        return self.last_value
    def gettype(self):
        #This is a control channel, so the type 'm' is returned.
        return 'c'

class Agilent33250A_freq(Agilent33250A_CHAN):
    '''
    This class can be used to create a channel that always
    shows the frequency of the awg. It is a child of Agilent33250A_CHAN
    One has to declare it in the platypus config file as follows:
        AWG = Agilent33250A('ASRL16::INSTR') #Declare the scope
        AWG_freq = Agilent33250A_freq('AWG_freq RMS', AWG) #Declare the scope_RMS channel
    '''
    def read(self):
        self.last_value = float(self.awg.inst.query("FREQ?"))/1000
        platypus.update()
        return self.last_value
    def set(self,v):
        self.awg.inst.write("FREQ {}KHZ".format(v))
        self.last_value = v
        platypus.update()
    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.last_value

class Agilent33250A_vpp(Agilent33250A_CHAN):
    '''
    This class can be used to create a channel that always
    shows the frequency of the awg. It is a child of Agilent33250A_CHAN
    One has to declare it in the platypus config file as follows:
        AWG = Agilent33250A('ASRL16::INSTR') #Declare the scope
        AWG_freq = Agilent33250A_vpp('AWG_vpp RMS', AWG) #Declare the scope_RMS channel
    '''
    def read(self):
        self.last_value = float(self.awg.inst.query("VOLT?"))
        platypus.update()
        return self.last_value
    def set(self,v):
        self.awg.inst.write("VOLT {}VPP".format(v))
        self.last_value = v
        platypus.update()
    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.last_value

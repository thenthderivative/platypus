# -*- coding: utf-8 -*-
"""
Created on April 13 2016

@author: Louis Gaudreau
"""

import platypus 
import pyvisa
import pyvisa

#Give a name to the device in the next line
class Keithley6517B(platypus.base.Channel):
    
    '''
    Documentation
    Controls the Keithley 6517B electrometer. Used to control a Thorlabs
    filter flipper. (Position1: 0V, Position2: 2V)
    
    Required Inputs: (name,address)
    '''    
    
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided channel name for this instrument
        self.setname(name)

        # Create the instrument. Ensure that baud rate, data bits, and other
        # settings are correct for the instrument being used.
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                baud_rate = 9600,
                data_bits = 8,
                stop_bits = pyvisa.constants.StopBits.one,
                parity=pyvisa.constants.Parity.none,
                read_termination = '\r',
                write_termination = '\r')
        
        #Turn on the voltage source
        self.source_on()
        #Initialize the voltage source by reading its value
        self.voltage = self.read()

    def validate(self):
        #No validation required
        return True
        
    def set(self, voltage):
        '''
        Documentation
        Sets the voltage of the source
        Input:
        voltage in V (float)
        Return:
        None
        '''
        self.inst.write(':SOURCE:VOLTAGE ' + str(voltage))
        self.voltage = voltage

        #Update platypus so the new value appears on the state monitor
        platypus.update()
        
        
    def stop(self): pass
        #No stop required
    
    
    def read(self):
        '''
        Documentation
        Reads the voltage of the source
        Input:
        None
        Return:
        voltage in V (float)
        '''
        self.voltage = float(self.inst.query(':SOURCE:VOLTAGE?'))
        return self.voltage

    def source_on(self):
        '''
        Documentation
        Turns the voltage source of the keithley 6517B on.
        Input: None
        Return: None
        '''
        #Output1 is the voltage source, 0=off, 1=on
        self.inst.write(':OUTPUT1 1')

    def source_off(self):
        '''
        Documentation
        Turns the voltage source of the keithley 6517B on.
        Input: None
        Return: None
        '''
        #Output1 is the voltage source, 0=off, 1=on
        self.inst.write(':OUTPUT1 0')    
        
        
    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.voltage
        
        
    def gettype(self):
        #If this is a control channel, 'c' should be returned. If it is a
        #measurement channel, 'm' should be returned.
        return 'c'

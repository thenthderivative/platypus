'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

#!/usr/bin/env python
import pyvisa
import time

import platypus

class IPS(platypus.base.Channel):
    def __init__(self, name, address, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Name the channel
        self.setname(name)

        # Defaults
        self.maxsweeprate = 0.1
        self.maxfield = 0.0

        # Override defaults with provided configuration
        if 'maxsweeprate' in kwargs: self.maxsweeprate = abs(kwargs['maxsweeprate'])
        if 'maxfield' in kwargs: self.maxfield = abs(kwargs['maxfield'])

        # Connect to the device
        rm = pyvisa.ResourceManager()
        self.instr = rm.open_resource(address, read_termination='\r', write_termination='\r')

        self._execute('$Q4') # Enter extended resolution
        self._execute('$C3') # Remote mode (unlocked)
        self._execute('$A0') # Hold mode
        self.read()
        # self._execute('$M9') # Display field in Tesla
        self.ap = False

    def _execute(self, message):
        '''
        Write a command to the device

        Input:
            message (str) : write command for the device

        Output:
            None
        '''
        if len(message) < 1: return
        #print("Sending {}".format(repr(message)))
        self.instr.write(message)
        time.sleep(50e-3) # wait for the device to be able to respond
        if message.find('$') < 0:
            result = self.instr.read().strip()
            time.sleep(50e-3)
            #print("Received {}".format(repr(result)))
            if result.find('?') >= 0:
                print("Error: Command '{}' not recognized".format(message))
            else:
                return result

    def autopersist(self, ap=True):
      with self.lock:
        self.ap = ap

    def validate(self, value):
      with self.lock:
        return abs(value) <= self.maxfield

    # Sets the rate currently stored in the device, validating it first.
    def setrate(self, r):
      with self.lock:
        r = abs(r)
        if r > self.maxsweeprate: raise ValueError
        self._execute("$T{:f}".format(r))

    # Returns the rate currently stored in the device.
    def getrate(self):
      with self.lock:
        return float(self._execute("R9")[1:])

    # Goto the given field using either currently set rate (rate=None)
    # Or the provided rate (a float) in T/min.
    def set(self, v, rate = None):
        if not self.validate(v): raise ValueError('Field Too Large')
        if abs(self.getvalue() - v) < 0.0001:
            return # That was easy!
        # We will toggle persistent mode if and only if we have been asked to and the magnet is currently in persistent mode.
        ap = self.ap and self.getstatus()['H'] in [0, 2]
        if ap: self.on() # Come out of persistent mode.
        # Use goto() for the initial setup.
        self.goto(v, rate)
        self.wait()
        if ap: self.off() # Go into persistent mode.

    def getsetpoint(self):
      with self.lock:
        return float(self._execute("R8")[1:])

    def getstatus(self):
      with self.lock:
        '''Returns the status variables by letter.'''
        X = self._execute("X")
        return {'X':int(X[1:3]), 'A':int(X[4]), 'C':int(X[6]), 'H':int(X[8]), 'M':int(X[11:12]), 'P':int(X[14:16])}

    def isdone(self):
        with self.lock:
            return self.getstatus()['M'] == 0

    # Wait until the magnet field reaches the setpoint or stop is pressed.
    def wait(self):
        # Query repeatedly until done.
        self.running = True
        while True:
            time.sleep(0.05)
            with self.lock:
                if not self.running or self.isdone(): # The stop button was pressed OR the system is done sweeping.
                    self.hold()
                    platypus.read(self)
                    return
            platypus.read(self)

    # Start the magnet sweeping, like set, but return immediately.
    def goto(self, v, rate = None):
      with self.lock:
        H = self.getstatus()['H']
        if not H in [1, 8]:
            raise ValueError('This magnet has a heat switch. Use IPS.on() to activate the heat switch.')
        # Validate the target field
        if not self.validate(v): raise ValueError('Field Too Large')

        # Validate and set the sweep rate
        if not rate is None: self.setrate(rate)

        # Set the target and enter sweepto mode.
        self._execute("$J{:f}".format(v))
        self._execute("$A1")

    def hold(self):
      with self.lock:
        self._execute("$A0") # Hold mode

    def on(self, activate = True, duration=45.0):
      with self.lock:
        X = self.getstatus()
        if X['H'] == 8: return # There is no heat switch, so there is nothing to do.
        if activate == True and X['H'] in [0, 2]: # The magnet is in persistent mode and we want it not to be.
            # Goto the set point if we're not already there.
            self._execute("$J{:f}".format(float(self._execute('R18')[1:])))
            self._execute("$A1") # Goto setpoint
            self.wait() # Wait until the system comes to rest.
            self.hold()
            # Activate the heater.
            self._execute("$H1")
            time.sleep(duration)
        elif activate == False and X['H'] == 1: # The magnet is not in persistent mode and we want it to be.
            self.hold()
            self._execute("$H0") # Heater off.
            time.sleep(duration)
            self._execute("$A2") # Goto Zero
            self.wait()
            self.hold()
        else:
            print("Switch Heater Current Low")
            #raise ValueError('Switch Heater Current Low')

    def off(self, duration=45.0): self.on(False, duration)
    def off_noblock(self):
      # Turns off the magnet heater and does not wait.
      # Warning: for people who know what they're doing ONLY.
      with self.lock:
        X = self.getstatus()
        if X['H'] == 8: return # There is no heat switch, so there is nothing to do.
        if X['H'] == 1: # The magnet is not in persistent mode and we want it to be.
            self.hold()
            self._execute("$H0") # Heater off.
        else:
            print("Switch Heater Current Low")
            #raise ValueError('Switch Heater Current Low')

    # Read the value of the channel (may involve a hardware read).
    def read(self):
      with self.lock:
        H = self.getstatus()['H']
        if H == 0 or H == 2: # The heater is off (persistent mode)
            self.v = float(self._execute('R18')[1:]) # Return the persistent magnet current.
        elif H == 1 or H == 8: # The heater is on or doesn't exist.
            self.v = float(self._execute('R7')[1:]) # Get demand field.
        else:
            print('Switch Heater Current Low')
            #raise ValueError('Switch Heater Current Low')
        return self.getvalue()
    
    def getvalue(self):
      with self.lock:
          try:
              with open(r'C:\Users\194cdata_acquisition\Desktop\Magnetic Field.txt','w') as f:
                  f.write(str(round(self.v,2)))
          except:
              pass
      return self.v
    def stop(self):
        with self.lock:
            self.running = False
            self.hold()
    def gettype(self): return 'c'

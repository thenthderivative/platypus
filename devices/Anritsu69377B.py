'''

Copyright (c) 2016, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import platypus
import pyvisa
import pyvisa
import time
from math import isclose, inf

class AnritsuBase(platypus.base.Channel):
    def __init__(self, parent, name, channel, limits = (-inf, inf)):
        self.cnum = channel
        self.setname(name)
        self.parent = parent
        self.v = 0.0
        self.limits = limits
    def validate(self, v):
        low, high = min(self.limits), max(self.limits)
        return (v >= low and v <= high)
    
    def stop(self): pass
    def getvalue(self): return self.v
    def gettype(self): return 'c'

class AnritsuPower(AnritsuBase):
    def validate(self, v):
        return (v >= -110 and v <= +20) and super().validate(v)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_power(v, self.cnum)
        else:
            raise ValueError('Invalid Power')
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_power(self.cnum)
        return self.getvalue()

class AnritsuFreq(AnritsuBase):
    def validate(self, v):
        return v >= 1E3 and v <= 70E9
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_frequency(v, self.cnum)
        else:
            raise ValueError('Invalid Frequency')
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_frequency(self.cnum)
        return self.getvalue()

class AnritsuModFreq(AnritsuBase):
    def validate(self, v):
        return v >= 0.1 and v <= 1E6
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_modulation_frequency(v)
        else:
            raise ValueError('Invalid Frequency')
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_modulation_frequency()
        return self.getvalue()

class AnritsuMW(object):
    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address)
    def _execute(self, msg):
        self.inst.write(msg)
        if msg.strip()[0] != 'O': return None
#        time.sleep(1)
        return self.inst.read()
    def F(self, name = "Anritsu Frequency (Hz)", channel=1, limits=(-inf, inf)):
        '''
        Return a frequency control channel with 'name'
        and 'channel number' specified as positional arguments.
        Units are in MHz.
        Defaults to F1.
        '''
        return AnritsuFreq(self, name, channel, limits)
    def MF(self, name = "Anritsu Modulation Frequency (Hz)", limits=(-inf, inf)):
        '''
        Return a frequency control channel with 'name'
        and 'channel number' specified as positional arguments.
        Units are in MHz.
        Defaults to F1.
        '''
        return AnritsuModFreq(self, name, limits)
    def L(self, name = "Anritsu Power (dBm)", channel=1, limits=(-inf, inf)):
        '''
        Return a power control channel with 'name'
        and 'channel number' specified as positional arguments.
        Setting is either in dBm or in mV depending on the mode.
        Defaults to L1.
        '''
        return AnritsuPower(self, name, channel, limits)
    def get_frequency(self, cnum=1):
        '''Read the frequency on cnum channel (default 1)'''
        msg = "OF{}".format(cnum)
        f = self._execute(msg)
        return float(f)*1E6
    def get_power(self, cnum=1):
        '''Read the power level on cnum channel (default 1)'''
        msg = "OL{}".format(cnum)
        p = self._execute(msg)
        return float(p)
    def set_frequency(self, freq, cnum=1):
        '''Change the frequency on cnum channel (default 1)'''
        msg = "F{}{:f}MH".format(cnum, freq/1E6)
        self._execute(msg)
    def get_modulation_frequency(self):
        '''Get the internal amplitude modulation frequency'''
        msg = "OAR"
        f = self._execute(msg)
        return float(f)
    def set_modulation_frequency(self, freq):
        '''Change the internal amplitude modulation frequency'''
        msg = "AMR{:f}MH".format(freq/1E6)
        self._execute(msg)
    def set_power(self, power, cnum=1):
        '''Change the power level on cnum channel (default 1)'''
        msg = "L{}{:f}DM".format(cnum, power)
        self._execute(msg)
    def on(self, status=True):
        '''Turn on MW output or, if the argument is false, turn it off'''
        msg = "RF1" if status else "RF0"
        self._execute(msg)
    def off(self):
        '''Turn off MW output'''
        self.on(False)

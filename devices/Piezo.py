# -*- coding: utf-8 -*-
"""
Created on March 22 2016

@author: Louis Gaudreau
         & Jason Phoenix
"""

import platypus
from platypus import base
import pyvisa
import pyvisa
import time
from numpy import loadtxt, linspace

import tkinter as tk

class MonitorMotionWindow():
    def __init__(self,stop_var):
        self.window = tk.Tk()
        self.window.geometry('300x300')
        self.window.title('Piezo Motion')
        self.standard_colour = 'SystemButtonFace'
        self.window.configure(background=self.standard_colour)
        self.window.columnconfigure(0,minsize = 300)

        self.movement_label = tk.StringVar()
        self.movement_display = tk.Label(self.window,textvariable = self.movement_label,bg = self.standard_colour)
        self.movement_display.grid(row=0,column = 0,sticky = tk.W)

        self.stop_button = tk.Button(self.window, text="STOP", command = lambda:self.stop_motion(), bg = self.standard_colour)
        self.stop_button.grid(row=1,column = 0,sticky = tk.W+tk.E)

        self.stop_var = stop_var

    def stop_motion(self):
        self.stop_var[0] = 1

#Give a name to the device in the next line
class Piezo(base.Channel):
    
    '''
    Controls the position of the specified nano positioning stage.

    This requires that the ANC300, voltmeter (even better: RMS oscilloscope) channel and the
    Pathway7334 be declared in the config file:

    Switcher1 = Pathway7334('ASRL17::INSTR')
    Switcher2 = Pathway7334('ASRL17::INSTR')
    Piezo_Controller = ANC300('ASRL3::INSTR')

    To declare it: Piezo_x = Piezo(name, ID, read_instrument, position_v_instr, Switcher1,
                                   Switcher2, Piezo_Controller)

    Piezo ID must be 'X', 'Y', 'Z' or 'R'. This will allow the program to identify
    the device more easily.    
    '''    
    
    def __init__(self, name, piezo_ID, read_instr, position_v_instr, read_switcher, V_switcher, piezo_controller, *args, mc_temp = None, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided channel name for this instrument
        self.setname(name)

        # For every channel, write self.mychannel = mychannel
        self.piezo_controller = piezo_controller
        self.read_instr = read_instr
        self.V_switcher = V_switcher
        self.read_switcher = read_switcher
        self.piezo_ID = piezo_ID
        self.position_v_instr = position_v_instr

        #Set the port to use for both switchers and note the ANC300 axis
        #(x=['A',4], y=['B',3], z=['C',2], theta = ['D',1])
        #Also specify the file containing the calibration data.
        if self.piezo_ID == 'X':
            self.controller_axis = 4
            self.port = 'A'
            self.data_file = r'C:\Data\2016\20160708\20160708_015\20160708_015.dat' ###1V - Voltage applied to resistive encoder during calibration.
            if mc_temp == None or mc_temp.read() == 0:
                self.vmin = 0.12992 #Room-temp, volts
                self.vmax = 0.74461 #Room-temp, volts
            else:
                self.vmin = 135.258E-3 #Low-temp, volts
                self.vmax = 754.76E-3 #Low-temp, volts
            self.slope_read = (self.vmax-self.vmin)/5
        elif self.piezo_ID == 'Y':
            self.controller_axis = 3
            self.port = 'B'
            self.data_file = r'C:\Data\2016\20160628\20160628_024\20160628_024.dat' ###0.2V
            if mc_temp == None or mc_temp.read() == 0:
                self.vmin = 0.1577 #Room-temp, volts (with 1V applied)
                self.vmax = 0.7812 #Room-temp, volts (with 1V applied)
            else:
                self.vmin = 0 #Low-temp, volts
                self.vmax = 0 #Low-temp, volts
            self.vmin = 0
            self.vmax = 0
            self.slope_read = 1
        elif self.piezo_ID == 'Z':
            self.controller_axis = 2
            self.port = 'C'
            self.data_file = r'C:\Data\2016\20160708\20160708_017\20160708_017.dat' ###1V
            if mc_temp == None or mc_temp.read() == 0:
                self.vmin = 0.37445 #Room-temp, volts
                self.vmax = 0.976645 #Room-temp, volts
            else:
                self.vmin = 0.37041 #Low-temp, volts
                self.vmax = 0.97366 #Low-temp, volts
            self.slope_read = (self.vmax-self.vmin)/5
        elif self.piezo_ID == 'R':
            self.controller_axis = 1
            self.port = 'D'
            self.data_file = r'C:\Data\2016\20160715\20160715_017\20160715_017.dat' ###1V
            self.vmin = 0
            self.vmax = 0
            self.slope_read = 1
            self.end_angle = 0

        # Open .dat files and retrieve voltage position data. Note that the last four
        # data points are deleted because they are approximately identical and used in
        # the calibration program to verify that the end of the range has been reached.
        data = loadtxt(self.data_file, delimiter= ', ')
        self.position_data_volts = data[:,1]
        self.vmax = max(self.position_data_volts)
        self.vmin = min(self.position_data_volts)
        self.position_data_distance = data[:,0]
        # Record the location where the resistive encoder for the rotator
        # ends. It is the angle in degrees where the measured voltage drops to ~0.
        if self.piezo_ID == 'R':
##            index_1 = [i for i,j in enumerate(self.position_data_volts) if j == self.vmax][0]
##            stop,index_2 = 0,0
##            for i in range(index_1,len(self.position_data_distance)):
##                if self.position_data_volts[i] < 0.1 and stop == 0:
##                    index_2 = i - 1
##                    stop = 1
##            self.end_angle = self.position_data_distance[index_2]
            self.vmin = self.position_data_volts[0]
            vals = [round(i) for i in data[:,0]]
            max_ind = [i for i,j in enumerate(vals) if j == 341][0]
            self.vmax = self.position_data_volts[max_ind]
            self.end_angle = data[max_ind,0]
            self.slope_read = (self.vmax-self.vmin)/self.end_angle
            

        #Initialize the position to 0.0
        self.position = 0.0

        #This variable is used only in the self.set and self.move_by functions.
        #It is used to prevent messages being displayed on screen when these
        #functions are used in experiments. (Set to zero in experiment code if
        #on-screen updates stating new piezo position are not desired.)
        self.display_messages = 0

        #Set standard voltage value to be used as step amplitude by ANC300
        self.control_voltage = 35
        if self.piezo_ID == 'Z':
            self.control_voltage = 34
        if self.piezo_ID == 'X':
            self.control_voltage = 24
        #Set standard frequency value to be used as step amplitude by ANC300
        self.control_frequency = 500 #350

        #Set averaging and PLC values of position encoder multimeter.
        self.read_instr.filter_on(10,1)
        self.read_instr.volt_range(2)

        #Set bias voltage of position encoder.
        self.position_v_instr.set(1)

	#These are the maximum temperature above which the piezo will not move (when fridge is on),
        #and the value below which the temperature must drop for the piezo to continue moving.
        if 3.5<mc_temp.read()<4.5:
            self.max_temp, self.min_temp = 4.5,4
        elif mc_temp.read() < 1:
            self.max_temp, self.min_temp = 0.49,0.3
        else:
            self.max_temp, self.min_temp = 300,299
        self.mc_temp = mc_temp

        #Note the precision within which the device should accept it has reached the proper location (in volts).
        if mc_temp == None or mc_temp.read() == 0:
            if self.piezo_ID == 'Z':
            	self.linear_precision = 0.0004
            else:
            	self.linear_precision = 0.0002
        else:
            self.linear_precision = 0.0002

	#Create variables for number of steps and associated voltage ratios, to be used in the
        #.set function.
        self.low_temp_ratios = []
        self.low_temp_steps = []
        self.high_temp_ratios = []
        self.high_temp_steps = []

        if mc_temp == None or mc_temp.read() == 0:
            self.piezo_step_iteration_limit = 100
            self.stuck_steps = 200
        elif self.piezo_ID == 'R':
            self.piezo_step_iteration_limit = 150
            self.stuck_steps = 450
        else:
            self.piezo_step_iteration_limit = 50
            self.stuck_steps = 450

        self.set_lock_on = 0

    def validate(self,value):
        return True
        #This function ensures that the values input by the user are valid.

    def location_to_voltage(self,location):
        '''
        Translates a location value (mm or degrees) into a voltage value.

        Inputs:
            Location (float)
        Returns:
            Voltage (float)
        '''
        if self.piezo_ID != 'R' and -2.5<=location<=2.5:
            return self.slope_read*(location+2.5)+self.vmin
        elif self.piezo_ID != 'R' and -2.5>location:
            return self.vmin
        elif self.piezo_ID != 'R' and 2.5<location:
            return self.vmax
        elif self.piezo_ID != 'R':
            return 0
        elif self.piezo_ID == 'R' and 0 <= location <= self.end_angle:
            return self.slope_read*location+self.vmin
##        else:
##            #First, find the index of distance calibration data value closest to
##            #the given location.
##            diffs = [abs(i-location) for i in self.position_data_distance]
##            index = [i for i,j in enumerate(diffs) if j == min(diffs)][0]
##            #Then, if the given location value is slightly less than the
##            #nearest calibration data point:
##            if location <= self.position_data_distance[index]:
##                #If the nearest calibration data point is not the first data
##                #point:
##                if index != 0:
##                    #Find the values of the calibration voltages and distances
##                    #on either side of the specified location
##                    point1 = self.position_data_distance[index - 1]
##                    volt1 = self.position_data_volts[index - 1]
##                    point2 = self.position_data_distance[index]
##                    volt2 = self.position_data_volts[index]
##                    #Perform a caluclation to determine voltage. (Add to the lower
##                    #calibration voltage data point the fraction of the way between
##                    #the two calculated calibration points the specified location
##                    #should be.)
##                    voltage = volt1+(volt2-volt1)*((location - point1)/(point2 - point1))
##                #If the nearest calibration point is the first data point, then
##                #return a voltage value equal to the lowest calibration data value.
##                else:
##                    voltage = self.position_data_volts[index]
##            #Same for when the given location is greater than the nearest
##            #calibration data point.
##            elif location > self.position_data_distance[index]:
##                if index != len(self.position_data_volts)-1:
##                    point1 = self.position_data_distance[index]
##                    volt1 = self.position_data_volts[index]
##                    point2 = self.position_data_distance[index + 1]
##                    volt2 = self.position_data_volts[index + 1]
##                    voltage = volt1+(volt2-volt1)*((location - point1)/(point2 - point1))
##                else:
##                    voltage = self.position_data_volts[index]
##            if self.piezo_ID == 'R':
##                if location > self.end_angle and location != 360:
##                    voltage = 'Invalid Location'
##            #Return calculated voltage value.
##            return voltage

    def voltage_to_location(self,voltage):
        '''
        Translates a voltage value into a location value (mm or degrees).

        Inputs:
            Voltage (float)
        Returns:
            Location (float)
        '''

        if self.piezo_ID != 'R' and self.vmin<=voltage<=self.vmax:
            return (voltage-self.vmin)/self.slope_read-2.5
        elif self.piezo_ID != 'R' and self.vmin>voltage:
            return -2.5
        elif self.piezo_ID != 'R' and self.vmax<voltage:
            return 2.5
        elif self.piezo_ID != 'R':
            return 0
        elif self.piezo_ID == 'R':
            return (voltage-self.vmin)/self.slope_read
##        else:
##            #Same procedure as in location_to_voltage.
##            diffs = [abs(i-voltage) for i in self.position_data_volts]
##            index = [i for i,j in enumerate(diffs) if j == min(diffs)][0]
##            if voltage <= self.position_data_volts[index]:
##                if index != 0:
##                    point2 = self.position_data_volts[index]
##                    point1 = self.position_data_volts[index - 1]
##                    dist1 = self.position_data_distance[index - 1]
##                    dist2 = self.position_data_distance[index]
##                    location = dist1+(dist2-dist1)*((voltage - point1)/(point2 - point1))
##                else:
##                    location = self.position_data_distance[index]
##            elif voltage > self.position_data_volts[index]:
##                if index != len(self.position_data_distance)-1:
##                    point1 = self.position_data_volts[index]
##                    point2 = self.position_data_volts[index + 1]
##                    dist1 = self.position_data_distance[index]
##                    dist2 = self.position_data_distance[index + 1]
##                    location = dist1+(dist2-dist1)*((voltage - point1)/(point2 - point1))
##                else:
##                    location = self.position_data_distance[index]
##            return location

    def step_by(self,steps,settings_check = True):
        '''
        Moves the piezo by a specific number of steps. Step size determined
        by ANC300 voltage. Does not update position value. Optional keyword
        argument allows user to determine if step voltage and frequency
        should be set/checked before stepping.

        Inputs:
            Number Of Steps (int)
        Optional Keyword Inputs:
            settings_check (True or False, default = True)
        Returns:
            None
        '''
        if self.mc_temp != None and self.mc_temp.read() > self.max_temp:
            print('Temperature too hot. Waiting to cool down to '+str(self.min_temp)+'K before continuing to move piezo.')
            stop = 0
            while stop != 1:
                if self.mc_temp.read() < self.min_temp:
                    stop = 1
                else:
                    time.sleep(10)
                    

        if settings_check == True:
            #Set piezo controller voltage.
            self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
            #Set piezo controller frequency.
            self.piezo_controller.set_frequency(self.controller_axis,int(self.control_frequency))
        #Step piezo by a certain number of steps.
        self.piezo_controller.step_by(self.controller_axis,steps,settings_check = settings_check)

    def offset(self, voltage):
        '''
        Documentation:
        Moves the piezo of a given axis using a DC offset voltage (range ~800nm at 4K).
        Input:
            voltage (float)
        Return:
            None
        '''
        self.piezo_controller.offset(self.controller_axis,voltage)

    def get_offset(self):
        '''
        Documentation:
        Gets the piezo DC offset voltage (0V to 150V).
        Input:
        None
        Return:
        Offset voltage
        '''
        return self.piezo_controller.get_offset(self.controller_axis)

        
    def set(self, new_position):
        '''
        Moves the piezo to the specific desired position.

        Linear Piezos: Positions given in mm, as measured from the center
                       of the range of motion. Total range of motion is
                       approximately -2.5mm to 2.5mm.

        Rotating Piezo: Positions given in degrees. Ranges between 0 and
                        ~335 degrees due to position encoder length.

        Inputs:
            New Position (float)
        Returns:
            None  
        '''

        #Set piezo controller voltage.
        self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
        #Set piezo controller frequency.
        self.piezo_controller.set_frequency(self.controller_axis,int(self.control_frequency))

        #Reset temperature controls, in case termperature has changed since
        #Platypus started.
        if 3.5<mc_temp.read()<4.5:
            self.max_temp, self.min_temp = 4.5,4
        elif mc_temp.read() < 1:
            self.max_temp, self.min_temp = 0.49,0.3
        else:
            self.max_temp, self.min_temp = 300,299
        self.mc_temp = mc_temp

        if self.set_lock_on == 1:
            print('Cannot move while self.set_lock_on == 1')
        elif new_position > 2.5 and self.piezo_ID != 'R':
            print('Cannot set to position greater than 2.5.')

        elif new_position < -2.5 and self.piezo_ID != 'R':
            print('Cannot set to position less than -2.5.')
            
        #Otherwise, for the linear piezos:
        elif self.piezo_ID != 'R':

            #Display a message indicating that piezo is currently in motion.
            self.print_message(1,0)

            #Read current position.
            current_position_voltage = self.read()
            #Determine from calibration data the position encoder voltage value of the desired position.
            new_position_voltage = self.location_to_voltage(new_position)

            #Now move piezo into desired position.
            current_position_voltage = self.motion(new_position_voltage,current_position_voltage,1)

            #Display new location based on measured voltage and calibration data.
            self.print_message(2,current_position_voltage)

            #Set position value to be new measured voltage.
            self.position = current_position_voltage

        #Otherwise, for the rotator, if the specified position is between 0 and 360 degrees:
        elif 0 <= new_position <= 360:

            #If the specified position is in the range which the position does not cover,
            #display message indicating that the rotator cannot be set to that position.
            if self.end_angle < new_position < 360:
                print('')
                print('Input invalid. Resistive encoder exists only between 0 and '+str(round(self.end_angle,2))+' degrees.')
                print('')
            else:
                #If the new position is 360, set it to be zero.
                if new_position == 360:
                    new_position = 0

                #Display message indicating that the rotator is in motion.
                self.print_message(1,0)

                #Read current position.
                current_position_voltage = self.read()
                #Determine position encoder voltage value of new position from calibration data.
                new_position_voltage = self.location_to_voltage(new_position)

                #Now move piezo into desired position.
                current_position_voltage = self.motion(new_position_voltage,current_position_voltage,1)

                #Display new location based on measured voltage and calibration data.
                self.print_message(2,current_position_voltage)

        #If, for rotator, the specified angle is outside the 0-360 degree range, display error message.
        #(Only allowing a 0-360 range is easiest, due to gap in encoder.)
        else:
            print('')
            print('Input invalid. Specified rotator position must be between 0 and '+str(round(self.end_angle2))+' degrees.')
            print('')
            

    def move_by(self,distance):
        '''
        Moves the piezo by the specified distance in mm. (Total range
        of motion is approximately -2.5mm to 2.5mm.)

        NOTE: If specified distance is outside range of piezo, programme will
              move piezo to maximum/minimum.

        Inputs:
            Distance In mm (float)
        Returns:
            None  
        '''

        #Set piezo controller voltage.
        self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
        #Set piezo controller frequency.
        self.piezo_controller.set_frequency(self.controller_axis,int(self.control_frequency))

        #First, read current position.
        current_position_voltage = self.read()
        #Find location in mm or degrees:
        current_position_location = self.voltage_to_location(current_position_voltage)
        #Determine new position after movement is made.
        new_position = distance+current_position_location

        #For linear piezos, if specified distance is greater than available range of motion,
        #display message indicating this and move piezo to max position.
        if new_position > 2.5 and self.piezo_ID != 'R':
            self.print_message(3,0)
            self.move_to_end('max')
        #For linear piezos, if specified distance is less than -2.5mm (outside motion range),
        #display message indicating this and move piezo to min position.
        elif new_position < -2.5 and self.piezo_ID != 'R':
            self.print_message(3,0)
            self.move_to_end('min')
        #For rotator, if specified distance is too large, request a smaller distance (this will
        #prevent the rotator from just moving continuously for very long periods if a very large
        #value is input).
        elif self.piezo_ID == 'R' and new_position > 360 or self.piezo_ID == 'R' and new_position < 0:
            print('')
            print('Specified angle not accepted. Please specify an angle between 0 and 360 degrees.')
            print('')
        #If input value is within acceptable limits:
        elif self.end_angle < new_position < 360:
            print('')
            print('Specified angle is within region where resistive encoder is absent. Calibration\ndata unavailable for region '+str(round(self.end_angle,3))+' to 360 degrees.')
            print('')
        else:

            #Determine voltage value of new position from calibration curve.
            new_position_voltage = self.location_to_voltage(new_position)            

            #Display message indicating piezo is moving.
            self.print_message(1,0)
            
            #Move piezo desired distance.
            current_position_voltage = self.motion(new_position_voltage,current_position_voltage,1)

            #Display new location based on measured voltage and calibration data.
            self.print_message(2,current_position_voltage)

            #Set position value to be new measured voltage.
            self.position = current_position_voltage


    def move_to_end(self,end):
        '''
        Moves the piezo to the end of its range, either 'max' or 'min', as
        specified by the input. For rotator, both 'max' and 'min' have the
        effect of setting the device to position zero.

        Inputs:
            End (str)
        Returns:
            None  
        '''

        #Set piezo controller voltage.
        self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
        #Set piezo controller frequency.
        self.piezo_controller.set_frequency(self.controller_axis,int(self.control_frequency))
        
        #Indicate piezo is being repositioned.
        self.print_message(1,0)

        #For the linear piezos:
        if self.piezo_ID != 'R':
            #Determine direction in which piezo will be travelling, as well as the
            #theoretical endpoint (may not be exact endpoint due to measurement
            #noise, etc.).
            if end == 'max':
                direction = 1
                theoretical_new_position = self.vmax
            elif end == 'min':
                direction = -1
                theoretical_new_position = self.vmin

            #Determine current position voltage.
            current_position = self.read()

            #In order to avoid hitting the edge of the piezo's range, we'll first make
            #a jump to a location 0.2mm away.
            new_position = self.location_to_voltage((abs(self.voltage_to_location(theoretical_new_position)) - 0.1)*direction)
            #Move to new position.
            current_position = self.motion(new_position,current_position,0)

            # Set initial number of steps by which to move the attocube.
            steps = direction*400

        #For the rotator:
        else:

            #Determine current position.
            current_position = self.read()

            #Both max and min have the effect of setting the rotator to zero.
            #They both go in the negative direction because I haven't yet written
            #up the code for moving in the other direction.
            if current_position > self.position_data_volts[len(self.position_data_volts)/2]:
                direction  = 1
            else:
                direction = -1

            # Set initial number of steps by which to move the positioner.
            if current_position < self.vmin + 0.01: ########
                steps = direction*100
            elif current_position < self.vmin + 0.02:#########
                steps = direction*1000
            else:
                steps = direction*3000

        #Now move to end.
        self.move_to_end_rough(current_position,steps)
        #Read new position.
        voltage_new = self.read()

        #Display new location based on measured voltage and calibration data.
        self.print_message(2,voltage_new)

        #Set position value to be new measured voltage.
        self.position = voltage_new

    def move_to_end_rough(self,current_position,steps):
        '''
        Moves the piezo to the end of its range, either 'max' or 'min', as
        specified by the input. For rotator, both 'max' and 'min' have the
        effect of setting the device to position zero.

        Inputs:
            Current position voltage (float)
            Steps per move iteration (int)
        Returns:
            None  
        '''

        #Set piezo controller voltage.
        self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
        #Set piezo controller frequency.
        self.piezo_controller.set_frequency(self.controller_axis,int(self.control_frequency))

        if steps < 0:
            direction = -1
        else:
            direction = 1

        #Set up the variables which will check for
        voltage_prev1 = current_position
        voltage_prev2 = 0

        # Then continue to move piezo in negative direction until end of range is reached.
        while (steps != 0):
            #Move piezo.
            self.step_by(steps)
            #Wait for oscilloscope to register new voltage value.
            time.sleep(0.5) #in seconds
            #Read voltage value
            voltage_new = self.read()
            print(voltage_new)
            #For linear piezos, if the new voltage value is almost identical to the voltage
            #measured two loop iterations ago, exit loop.
            if self.piezo_ID != 'R' and abs(voltage_new - voltage_prev2)/(self.vmax - self.vmin) < 0.004:
                steps = 0
            #For the rotator:
            elif self.piezo_ID == 'R':
                #If the measured voltage enters the region where the encoder is absent,
                #end loop.
                if self.vmin-0.0005 < voltage_new < self.vmin+0.0005:
                    print('thing')
                    steps = 0
                #Alter step size if approaching zero position
                elif voltage_new < self.vmin+0.005:
                    print(voltage_new, self.vmin+0.0005)
                    steps = direction*30
                elif voltage_new < self.vmin+0.01:
                    print('asdf')
                    steps = direction*100
                elif voltage_new < self.vmin+0.03:
                    steps = direction*1000
            #Update variables recording previous voltages.
            voltage_prev1 = voltage_new
            voltage_prev2 = voltage_prev1
            print('')

        #For the rotator, once the end of it's range is reached
        steps = 10
        if self.piezo_ID == 'R':
            while (steps != 0):
                print('here')
                #Move piezo.
                self.step_by(steps)
                #Wait for oscilloscope to register new voltage value.
                time.sleep(0.5) #in seconds
                #Read voltage value
                voltage_new = self.read()
                #If the new voltage value is almost identical to the voltage
                #measured two loop iterations ago, exit loop.
                if voltage_new > self.vmin+0.0005:
                    print('ici')
                    self.step_by(-10)
                    steps = 0

    def motion(self,new_position_voltage,current_position_voltage,precise):
        '''
        Moves piezo to new location, either the exact location or in the
        neighbourhood of the specified location.

        Inputs:
            New Position Voltage (float)
            Current Position Voltage (float)
        Returns:
            Position Voltage (float)
        '''

        count = 0
        stop_motion = [0]

        monitor_window = MonitorMotionWindow(stop_motion)

        if self.piezo_ID != 'R':
            #Set piezo controller voltage.
            self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
            #Set piezo controller frequency.
            self.piezo_controller.set_frequency(self.controller_axis,int(self.control_frequency))
            steps = 1
            steps_taken = []
            while steps != 0 and count < 200:

		#If already in correct position before it starts moving
                if precise == 1 and abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) < 0.0002/(self.vmax-self.vmin): #0.003:    
                    break
                elif precise == 0 and abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) < 0.004/(self.vmax-self.vmin): #0.02:
                    break

                self.low_temp_ratios = [0.114*1.5,0.0611*1.5,0.0254*1.5,0.0129*1.5,0.00633*1.5,0.00296*1.6,0.00143*2,0.000392*3,0.000392*2/2]
                self.low_temp_steps = [4000,2500,1000,500,250,100,50,10,5]
                self.high_temp_ratios = [0.3*1.5,0.133*1.5,0.06*1.5,0.03*1.5,0.015*1.5,0.006*1.5,0.003*1.2,0.003*1.2/2]
                self.high_temp_steps = [2500,1000,500,250,100,50,10,5]

                ratio = abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin)
                if self.mc_temp == None or self.mc_temp.read() == 0:
                    ratio_vals = self.high_temp_ratios
                    step_vals = self.high_temp_steps
                else:
                    ratio_vals = self.low_temp_ratios
                    step_vals = self.low_temp_steps

                ind = [i for i,j in enumerate(ratio_vals) if ratio > j]
                if ind != []:
                    steps = step_vals[ind[0]]
                    if steps < 100 and precise != 1:
                        steps == 100
                elif precise == 1:
                    steps = 1
                else:
                    steps = 100
                
                #Number of steps the piezo moves is determined by the difference in the
                #current and desired position voltages.
##                if abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.114*1.5:
##                    steps = 4000
####                if abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.3*1.5:#0.0611*1.5: #
####                    steps = 2500
####                elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) > 0.133*1.5:#0.0254*1.5: #
####                    steps = 1000
####                elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) > 0.06*1.5:#0.0129*1.5: #
####                    steps = 500
####                elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) > 0.03*1.5:#0.00633*1.5: #
####                    steps = 250
####                elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) > 0.015*1.5:#0.00296*1.6: #
####                    steps = 100
####                else:
####                    #If it is desired that the piezo move to a specific exact location,
####                    #such as for .set() and .move_by(), then start stepping in smaller
####                    #increments.
####                    if precise == 1:
####                        if abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.006*1.5:#0.00143*2: #
####                            steps = 50
####                        elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.003*1.2:#0.000392*3: #
####                            steps = 10
####                        elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.003*1.2/2:#0.000392*2/2:
####                            steps = 5
####                        else:
####                            steps = 1
####                    #For cases such as .move_to_end(), precision is not required.
####                    else: steps = 100
          	
                #Make sure direction of movement is correct.
                if new_position_voltage < current_position_voltage:
                    steps = -steps

##                diff = self.voltage_to_location(new_position_voltage)-self.voltage_to_location(current_position_voltage)
##                steps = int(diff*188/0.03*0.8)

                #If it is stuck bouncing on either side of the desired location
                steps_taken.append(steps)
                if len(steps_taken)>7:
                    if (steps_taken[-1] < 0 and steps_taken[-2] > 0 and steps_taken[-3] < 0 and steps_taken[-4] > 0 and steps_taken[-5] < 0 and steps_taken[-6] > 0) or (steps_taken[-1] > 0 and steps_taken[-2] < 0 and steps_taken[-3] > 0 and steps_taken[-4] < 0 and steps_taken[-5] > 0 and steps_taken[-6] < 0):
                        steps = -self.stuck_steps*steps_taken[0]/abs(steps_taken[0])
                        steps_taken = []

                print(steps)
                #monitor_window.movement_label.set(steps)
                
                
                #Move piezo.
                self.step_by(steps)
                #Wait for oscilloscope to register new voltage values
                time.sleep(1) #in seconds
                #Read voltage value
                current_position_voltage = self.read()

                #If constant updates of position are desired on-screen during movement,
                #uncomment the following line. (Presumably to be only used during testing.)
                #print("New Location: "+str(voltage_to_location(current_position_voltage))+"mm")

                #Exit loop once measured voltage is within measurement uncertainty
                #of new desired position.
                if precise == 1 and abs(new_position_voltage - current_position_voltage) < self.linear_precision:
                    steps = 0
                elif precise == 0 and abs(new_position_voltage - current_position_voltage) < 0.002: #0.02:
                    steps = 0
                elif stop_motion == [1]:
                    steps == 0

##                print(stop_motion)
                    
                count += 1

        else:
            #Set piezo controller voltage.
            self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
            #Set piezo controller frequency.
            self.piezo_controller.set_frequency(self.controller_axis,int(self.control_frequency))
            steps = 1
            while steps != 0 and count < 200:
                #Number of steps the piezo moves is determined by the difference in the
                #current and desired position voltages.
                if abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.02675*2:
                    steps = 2500
                elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) > 0.01194*2:
                    steps = 1000
                elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.00575*2:
                    steps = 500
                elif abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) >  0.00364*2:
                    steps = 250
                else:
                    steps = 100
                #Make sure direction of movement is correct.
                if new_position_voltage < current_position_voltage:
                    steps = -steps
                
                #Move piezo.
                self.step_by(steps)
                #Wait for oscilloscope to register new voltage values
                time.sleep(0.5) #in seconds
                #Read voltage value
                current_position_voltage = self.read()

                #If constant updates of position are desired on-screen during movement,
                #uncomment the following line. (Presumably to be only used during testing.)
                #print("New Location: "+str(voltage_to_location(current_position_voltage))+"mm")

                #Exit loop once measured voltage is within measurement uncertainty
                #of new desired position.
                if abs(new_position_voltage - current_position_voltage)/(self.vmax - self.vmin) < 0.0004:#0.00106:
                    steps = 0

                count += 1

        #Return the current position value.
        if count > self.piezo_step_iteration_limit:
            error
        else:
            return current_position_voltage

        

    #Print a pre-set message
    def print_message(self,message_type,voltage):
        #If message display function has not been disabled:
        if self.display_messages == 1:
            print("")
            if message_type == 1:
                if self.piezo_ID == 'R':
                    print("Please wait while piezo is repositioned. May take several minutes, depending on travel distance.")
                else:
                    print("Please wait while piezo is repositioned. May take up to a minute, depending on travel distance.")
                print("(Wait for new position to be displayed on screen before proceeding.)")
            elif message_type == 2:
                if self.piezo_ID != 'R':
                    print("New Piezo "+self.piezo_ID+" Location Based On Calibration Data: "+str(round(self.voltage_to_location(voltage),7))+"mm")
                else:
                    print("New Piezo "+self.piezo_ID+" Location Based On Calibration Data: "+str(round(self.voltage_to_location(voltage),7))+" deg")
            elif message_type == 3:
                print('Piezo motion range exceeded. Piezo will move to edge of motion range.')
            print("")

    
    def stop(self):
        self.piezo_controller.stop()
        
    
    def read(self):
        #Set switchers to proper ports if they aren't already there.
        if self.V_switcher.get_port() != self.port:
            self.V_switcher.set_port(self.port)
            self.read_switcher.set_port(self.port)
            time.sleep(0.5)
        #Read position voltage.
        time.sleep(0.5)
        self.position = self.read_instr.read()
        platypus.update()
##        self.V_switcher.set_port('E')
##        self.read_switcher.set_port('E')
##        time.sleep(0.1)
        #Return position value.
        return self.position

    def get_position(self):
        '''
        Retrives piezo position using current voltage value and position
        encoder calibration data.

        Inputs:
            None
        Returns:
            Position (float)
        '''
        #First, read current position voltage.
        current_position_voltage = self.read()
        # Determine position from calibration data.
        return round(self.voltage_to_location(current_position_voltage),7)
        
    def getvalue(self):
        #Returns the last read value stored in memory.
        return round(self.voltage_to_location(self.position),6)
        
        
    def gettype(self):
        #If this is a control channel, 'c' should be returned. If it is a
        #measurement channel, 'm' should be returned.
        return 'c'

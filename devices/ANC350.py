# -*- coding: utf-8 -*-
"""
Created July 27, 2018
@author: Jason Phoenix
"""
import sys
sys.path.insert(0, 'C:\\Users\\194C\\Desktop\\platypus\\devices')
sys.path.insert(0, 'C:\\Users\\194C\\Desktop\\platypus\\devices\\ANC350_Library')
import ANC350lib as ANC
from ANC350_base import Positioner
import ctypes, math
from platypus import base
import time
from numpy import linspace

class ANC350(object):
    
    '''
    Documentation:
        Module that controls each axis of the ANC350.
    '''    
    
    def __init__(self, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Create the instrument
        self.inst = Positioner()

        self.name = 'ANC350'

        # Range around the target position where the target is considered to be reached
        # when moving to a specific position.
        self.target_range = 400 #nm
        for i in range(0,3):
            self.set_target_range(i, self.target_range)

    def validate(self):
        return True

    def set_frequency(self, axis, frequency):
        '''
        Documentation:
            Sets the frequency of the piezo of a given axis.
        Input:
            axis (0, 1, or 2) (int)
            frequency (int: 1Hz to 2kHz in Hz)
        Return:
            None
        '''
        self.inst.setFrequency(axis, frequency)

    def get_frequency(self, axis):
        '''
        Documentation:
            Gets the frequency of the piezo of a given axis.
        Input:
            axis (0, 1, or 2) (int)
        Return:
            frequency in Hz (float)
        '''
        return self.inst.getFrequency(axis)
        
    def set_voltage(self, axis, voltage):
        '''
        Documentation:
            Sets the voltage amplitude of the piezo of a given axis.
        Input:
            axis (0, 1, or 2) (int)
            voltage (float: 0V to 70V in volts)
        Return:
            None
        '''
        self.inst.setAmplitude(axis,voltage)        

    def get_voltage(self, axis):
        '''
        Documentation:
            Gets the voltage amplitude of the piezo of a given axis.
        Input:
            axis (0, 1, or 2) (int)
        Return:
            voltage in Volts (float)
        '''
        return self.inst.getAmplitude(axis)
    
    def step_by(self, axis, number_of_steps):
        '''
        Documentation:
            Moves the piezo of a given axis by a given number of steps.
        Input:
            axis (0, 1, or 2) (int)
            number_of_steps (int)
        Return:
            None
        '''
        if number_of_steps > 0:
            #Do the stepping in the forwards direction
            for i in range(0,number_of_steps):
                self.inst.startSingleStep(axis,0)
        elif number_of_steps == 0:
            pass
        else :
            #Do the stepping in the backwards direction
            for i in range(0,abs(number_of_steps)):
                self.inst.startSingleStep(axis,1)

    def offset(self, axis, voltage,current_voltage):
        '''
        Documentation:
            Moves the piezo of a given axis using a DC offset voltage (range ~400nm at 4K).
        Input:
            axis (0, 1, or 2) (int)
            destination voltage (float: 0V to 60V)
            current voltage(float: 0V to 60V)
        Return:
            None
        '''
        if 70 >= voltage >= 0:
            if abs(current_voltage-voltage) > 0.25:
                vals = linspace(current_voltage,voltage,abs(current_voltage-voltage)*4+1)
            else:
                vals = [voltage]
            for i in vals:
                self.inst.setDcVoltage(axis,i)
                time.sleep(0.05)
        else:
            print('Offset voltage must be between 0V and 70V.')
            error

    def get_position(self, axis):
        '''
        Documentation:
        Returns position of piezo in mm.
        Input:
            axis (0, 1, or 2) (int)
        Return:
            position in mm (float)
        '''
        return self.inst.getPosition(axis)*1000

    def set_target_range(self, axis, target_range):
        '''
        Documentation:
            Defines the range around the target position where the target is considered to be reached.
        Input:
            axis (0, 1, or 2) (int)
            target range in nm (float)
        Return:
            None
        '''
        self.target_range = target_range
        #The range is defined in metres in setTargetRange, so divide by 1E9
        self.inst.setTargetRange(axis,target_range/1e9)

    def set_to_position(self, axis, target):
        '''
        Documentation:
            Move piezo to target position.
        Input:
            axis (0, 1, or 2) (int)
            target position in mm (float: 0 to 5)
        Return:
            None
        '''
        self.inst.setTargetPosition(axis,target/1e3)
        self.inst.startAutoMove(axis,1,0)
        err = 0
        if abs(self.inst.getPosition(axis)-target/1e3) > self.target_range/1e9:
            start = time.monotonic()
            while True:
                time.sleep(0.2)
                if abs(self.inst.getPosition(axis)-target/1e3) < self.target_range/1e9:
                    break
                elif time.monotonic()-start > 60*2:
                    print('set timeout --> positioner probably stuck')
                    err = 1
                    break
        self.inst.startAutoMove(axis,0,0)
        #self.stop()
        #If stuck, create an error to abort experiment
        if err != 0:
            error

    def move_by_distance(self, axis, distance):
        '''
        Documentation:
            Move piezo by amount. Forward: positive distance; backwards: negative distance.
        Input:
            axis (0, 1, or 2) (int)
            distance in mm (float)
        Return:
            None
        '''
        self.inst.setTargetPosition(axis,distance/1e3)
        self.inst.startAutoMove(axis,1,1)

    def ground(self):
        '''
        Documentation:
            Grounds all the attocube piezos controlled by the ANC350
        Input:
            None
        Return:
            None
        '''
        pass

    def stop(self):
        '''
        Documentation:
            Stops all mouvement of all the attocube piezos controlled by the ANC350
        Input:
            None
        Return:
            None
        '''
        self.inst.startContinuousMove(0,0,0)
        self.inst.startContinuousMove(1,0,0)
        self.inst.startContinuousMove(2,0,0)


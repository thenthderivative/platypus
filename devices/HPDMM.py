'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import platypus
import pyvisa,threading
import pyvisa
from math import nan

class HPDMM(platypus.base.Channel):
    cfgcmds = ("INBUF ON",)
#    cfgcmds = ("PRESET NORM",
#               "INBUF ON",
#               )
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided name for this channel
        self.setname(name)

        # Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                                     read_termination='\r\n',
                                     write_termination='\r\n',
                                     timeout=6000)

        # Configure for read-on-demand.
        for x in self.cfgcmds: self.inst.write(x)
        self.v = 0.0
    def validate(self, value):
        return True
    def set(self, v): pass
    def stop(self): pass

    def mem(self, on=False): self.inst.write("MEM 2" if on else "MEM 0")

    def setnplc(self, NPLC):
        with self.lock:
            self.inst.write("NPLC {}".format(NPLC))

    def nrdgs(self, mode, N=1):
      with self.lock:
        cmd = "NRDGS"
        modes = {'AUTO':1, 'EXTSYN':2, 'SYN':5, 'TIMER':6, 'LEVEL':7, 'LINE':8}
        modes_inv = {v: k for k, v in modes.items()}
        if N <= 1: N = 1
        if mode in modes.values():
            self.inst.write("{} {}, {}".format(cmd, N, mode))
        elif mode in modes:
            self.inst.write("{} {} {}".format(cmd, N, modes[mode]))
        else:
            raise ValueError('Invalid mode selected.')

    def trig(self, mode):
      with self.lock:
        cmd = "TRIG"
        modes = {'AUTO':1, 'EXT':2, 'SGL':3, 'HOLD':4, 'SYN':5, 'LEVEL':7, 'LINE':8}
        modes_inv = {v: k for k, v in modes.items()}
        if mode in modes.values():
            self.inst.write("{} {}".format(cmd, mode))
        elif mode in modes:
            self.inst.write("{} {}".format(cmd, modes[mode]))
        else:
            raise ValueError('Invalid mode selected.')

    def tarm(self, mode, N=1):
      with self.lock:
        cmd = "TARM"
        modes = {'AUTO':1, 'EXT':2, 'SGL':3, 'HOLD':4, 'SYN':5}
        modes_inv = {v: k for k, v in modes.items()}

        if N <= 1: N = 1
        if mode in modes.values():
            if mode == modes['SGL']:
                self.inst.write("{} {}, {}".format(cmd, mode, N))
            else:
                self.inst.write("{} {}".format(cmd, mode))
        elif mode in modes:
            if mode == 'SGL':
                self.inst.write("{} {}, {}".format(cmd, modes[mode], N))
            else:
                self.inst.write("{} {}".format(cmd, modes[mode]))
        else:
            raise ValueError('Invalid mode selected.')

     # Only read on demand.

    # Read the value of the channel (may involve a hardware read).
    def read(self):
      with self.lock:
        msg = self.inst.read().strip()
        try:
            self.v = float(msg)
        except ValueError:
            if len(msg) > 2 and msg[0] == '-' and msg[1] == '-':
                try:
                    self.v = float(msg[1:])
                except:
                    print("__DMM: {}".format(msg))
                    self.v = nan # Give up. This thing is borked.
            else:
                print("__DMM: {}".format(msg))
                self.v = nan # Give up. This thing is borked.
        platypus.update()
        return self.getvalue()

    def getvalue(self): return self.v

    def gettype(self): return 'm'

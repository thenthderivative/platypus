# -*- coding: utf-8 -*-
"""
Created on Thu May 14 09:46:45 2015

@author: Phoenixj
"""

from platypus import base
import pyvisa
import pyvisa
import time

class ANC150(base.Channel):
    
    '''
    This module controls the ANC150 Attocube rotary positioner, allowing the
    user to rotate the device through a certain angle.
    
    Initial Required Inputs: (name,address)
    
    The set function rotates the ANC150 by translating an angle value, in
    degrees, into the required number of steps.

    .set Required Inputs: (angle)
    '''    
    
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided name for this channel
        self.setname(name)

        # Create the instrument
        rm = pyvisa.ResourceManager()
        lib = rm.visalib
        self.inst = rm.open_resource(address,
                baud_rate = 38400,
                data_bits = 8,
                stop_bits = pyvisa.constants.StopBits.one,
                parity=pyvisa.constants.Parity.none,
                read_termination = '\r',
                write_termination = '\r\n')
        
        #The approximate number of steps required to rotate the device by one
        #degree, as determined through calibration tests, is declared here.
        self.steps_per_degree = 145
        #Set the frequency.
        self.frequency = 860
        #Set the voltage. Voltage MUST remain between 0 and 60V inclusive.
        self.voltage = 30
        
        ###NOTE: The following settings were found to result in nearly perfect
        ###      360º rotation: Voltage = 30
        ###                     Frequency = 860
        ###                     Steps Per Degree = 139
        
        #Set the controller axis to which the rotator is connected.
        self.axis = 1
        #The controller is set to stepping mode.
        self.inst.write('setm %d stp' %(self.axis))
        #The controller is configured so that it has the same settings as when
        #it was calibrated.
        self.inst.write('setv %d %d' %(self.axis,self.voltage))
        self.inst.write('setf %d %d' %(self.axis,self.frequency))
        self.inst.write('setpu %d 0' %(self.axis))
        self.inst.write('setpd %d 1' %(self.axis))
        
        self.relative_position = 0
    def validate(self,angle): return True
    def set(self, new_position):
        #This command rotates the ANR240 by the number of degrees in the input
        #argument 'angle'.
    
        if self.relative_position == 360:
            self.relative_position = 0
    
        angle = new_position - self.relative_position
        #The total number of steps needed to rotate the device to the desired
        #angle is calculated.
        total_steps = angle*self.steps_per_degree
        #ANR240 is rotated by the desired amount, and may be rotated clockwise
        #(negative angle values) or counterclockwise (positive angle values).
        if total_steps > 0:
            self.inst.write('stepd %d %d' %(self.axis,total_steps))
        elif total_steps < 0:
            self.inst.write('stepu %d %d' %(self.axis,-total_steps))
        #Not possible to check whether the device is in motion. Frequency is
        #preset, then the program waits for the amount of time required for
        #the required number of steps to be completed.
        time.sleep((1/self.frequency)*abs(total_steps*1.5))
        self.relative_position = new_position
    def stop(self):
        #The rotator is instructed to cease all movement.
        self.inst.write('stop %d' %(self.axis))
    def read(self): return self.getvalue()
        #The ANR240 does not have position values, so there is nothing to read.
    def getvalue(self): return self.relative_position
    def gettype(self):
        #This is a control channel, so the type 'c' is returned.
        return 'c'

# -*- coding: utf-8 -*-
"""
Created on Tue May 12 12:01:47 2015

@author: Phoenixj
"""

from platypus import base
import time, threading
import pyvisa
import pyvisa

class NSC200(base.Channel):

    '''
    This is an electronic controller for a rotator from Newport.

    !!!!
    Important note: After every write command immediately followed by a read 
    command, you should wait for 50ms between the two commands
    !!!!
    
    Required Inputs: (name,address)
    '''  

    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided name for this channel
        self.setname(name)

        # Create the instrument
        rm = pyvisa.ResourceManager()
        lib = rm.visalib
        self.inst = rm.open_resource(address,
                baud_rate = 19200,
                data_bits = 8,
                stop_bits = pyvisa.constants.StopBits.one,
                parity=pyvisa.constants.Parity.none,
                read_termination = '\r',
                write_termination = '\r\n')

        #Ask the rotator what position it is in. The information returned by the 
        #device is a string with the position value at the end. Extract
        #the position, turn it into a float and set self.position to be that
        #value.

        #Ask position
        self.inst.write("1TP?")
        #Sleep
        time.sleep(0.05)
        #Attempt:
        try:
            #Read position value.
            position_query = self.inst.read()
            #Set position variable to value given by device
            self.position = float(position_query[5:-1])
        #An error will occur if the computer has been restarted, or if the
        #device has been reconnected to the computer. Therefore the position
        #is requested a second time, and no error should be encountered.
        except:
            time.sleep(0.05)
            #Ask for position
            self.inst.write("1TP?")
            time.sleep(0.05)
            #Read and save position.
            position_query = self.inst.read()
            self.position = float(position_query[5:-1])

    def validate(self, angle):
        #No validation required
        return True
        
    def set(self, angle):
      with self.lock:
        #This command requires an angle to be input. The as there are 64 positions
        #per degree, the given angle is translated into a position value (with
        #position zero at 0º).
        self.position = angle*64
        #The NSC200 command is constructed using the position value, and it is
        #written to the device.
        set_position_command = '1PA'+str(self.position)
        self.inst.write(set_position_command)
        #There is an issue with the NSC200 where a command which is sent too 
        #soon after the previous one will cause a timeout error. Therefore, a
        #brief delay of about 0.05 seconds is required between all write/query
        #commands.
        time.sleep(0.05)
        #In order to ensure that the program waits for the rotator to stop moving
        #before it proceeds, a small while loop is used. The device is continuously
        #asked if it has ceased to rotate. If the motor is in motion, the value
        #80 is returned. If motion has ceased, the value 81 is returned. The
        #program continues once the rotator has stopped rotating.
        Move_Query = '000'
        while Move_Query[-2] == '0':
            Move_Query = self.inst.query('1TS?')
            time.sleep(0.05)
            
            
    def stop(self):
      with self.lock:
        #The rotator is instructed to cease all movement.
        self.inst.write('1ST')
        
        
    def read(self):
      with self.lock:
        #Ask the rotator what position it is in. The information returned by the 
        #device is a string with the desired position value at the end. Extract
        #the position, turn it into a float and set self.position to be that
        #value. The position is translated into degrees in getvalue(), and it
        #is the angle which is returned to the user.
        position_query = self.inst.query("1TP?")
        self.position = float(position_query[5:-1])
        return self.getvalue()
        
        
    def getvalue(self):
      with self.lock:
        #There are 64 positions per degree, so taking position zero to be 0º,
        #the angle in degrees will be the position number divided by 64.
        return self.position/64
        
        
    def gettype(self):
        #This is a control channel, so the string 'c' is returned.
        return 'c'

'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

#!/usr/bin/env python
import pyvisa
import pyvisa
import time
import os

import platypus

import numbers
import struct
import datetime

class AWGWaveformParameter70002A(platypus.base.Channel):
    def __init__(self, name, value):
        self.value = value
        self.setname(name)
        self.updated = False
    def set(self, value):
        if math.isclose(value, self.value): return
        self.value = value
        self.updated = True
    def read(self): return self.value
    def getvalue(self): return self.value
    def validate(self, v): return True
    def gettype(self): return 'c'

class AWGWaveformFile70002A(object):
    def __call__(self, *args):
        # Overload this, or provide a function in the initialization.
        if not hasattr(self, "function"): raise NotImplementedError
        return self.function(self.clock.getvalue(), *args)
    def __init__(self, awg, fname, parameters, clock="AWG_Clock (Hz)", function = None):
        '''
            'fname'
                The name of the target waveform file on the AWG hard drive.
            'parameters' ::=[(name, value)]
                Specifies the name and initial value of each parameter channel.
                These will be accessible in list form by the .parameters attribute.
            'clock'
                Specifies the name of the clock channel, accessible as the .clock attribute.
            'function'
                Optional callable which accepts positional arguments in the same number
                and order as the parameters are given.
                Returns a sequence of numbers (ideally a numpy array).
        '''
        if function: self.function = function
        self.awg = awg
        self.fname = fname
        self.parameters = [AWGWaveformParameter70002A(name, v) for name,v in parameters]
        self.clock = platypus.core.Dummy(clock)
        self.clock.set(100E6) # 100 MHz clock by default (can be changed).
    def force_update(self): self.update(True)
    def update(self, force=False):
        # Only update if there's been a change, or if forced.
        if not (force or any([p.updated for p in self.parameters])): return
        # Mark each parameters as unchanged.
        for p in self.parameters: p.updated = False
        # Update each channel in the platypus status monitor.
        platypus.update(self.parameters)
        # Generate the waveform.
        data = self(*[p.getvalue() for p in self.parameters])
        # Convert to binary, with (trivial) marker data.
        num_points = len(data)
        data = b''.join([struct.pack("f", datum) for datum in data])
      
        # Generate the file header.
        file_prefix_main = ""\
            +'<DataSetsCollection xmlns="http://www.tektronix.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.tektronix.com file:///C:\\Program%20Files\\Tektronix\\AWG70000\\AWG\\Schemas\\awgDataSets.xsd">\n'\
            +'<DataSets version="1" xmlns="http://www.tektronix.com">\n'\
            +'<DataDescription>\n'\
            +'<NumberSamples>'+str(int(num_points))+'</NumberSamples>\n'\
            +'<SamplesType>AWGWaveformSample</SamplesType>\n'\
            +'<MarkersIncluded>false</MarkersIncluded>\n'\
            +'<NumberFormat>Single</NumberFormat>\n'\
            +'<Endian>Little</Endian>\n'\
            +'<Timestamp>'+datetime.datetime.today().isoformat()+'-07:00</Timestamp>\n'\
            +'</DataDescription>\n'\
            +'<ProductSpecific name="">\n'\
            +'<ReccSamplingRate units="Hz">NaN</ReccSamplingRate>\n'\
            +'<ReccAmplitude units="Volts">NaN</ReccAmplitude>\n'\
            +'<ReccOffset units="Volts">NaN</ReccOffset>\n'\
            +'<SerialNumber />\n'\
            +'<SoftwareVersion>1.0.0903</SoftwareVersion>\n'\
            +'<UserNotes />\n'\
            +'<OriginalBitDepth>Floating</OriginalBitDepth>\n'\
            +'<Thumbnail />\n'\
            +'<CreatorProperties name="" />\n'\
            +'</ProductSpecific>\n'\
            +'</DataSets>\n'\
            +'</DataSetsCollection>\n'\
            +'<Setup />\n'\
            +'</DataFile>'
        # This is the cleanest way I could think of for calculating the length of a header that, itself, contains the header length (stupid). -- Alex
        file_prefix_offset = '<DataFile offset="{:09}" version="0.1">\n'.format(0)
        offset = len(file_prefix_offset)+len(file_prefix_main)
        file_prefix_offset = '<DataFile offset="{:09}" version="0.1">\n'.format(offset)
        head = file_prefix_offset+file_prefix_main

        # Upload to AWG.
        print("__LOADING")
        self.awg.load_file_from_memory(self.fname, head.encode()+data)
        print("__SELECTING")
        self.awg.select_file(self.fname)
        print("__DONE:", self.awg.instr.query("SYST:ERROR?"))

def convert(v):
    if isinstance(v, numbers.Number): return v
    v = v.lower()
    v2 = v.replace("mhz", "")
    if v2 != v: return float(v2)*1000000
    v2 = v.replace("khz", "")
    if v2 != v: return float(v2)*1000
    v2 = v.replace("hz", "")
    return float(v2)

class AWG70002ABase(platypus.base.Channel):
    def __init__(self, name, instrument, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setname(name)
        self.inst = instrument
        self.v = 0
    def stop(self): pass
    
    def getvalue(self): return self.v
    def gettype(self): return 'c'

class AWG70002AVoltage(AWG70002ABase):
    def validate(self, v):
        return (v <= 2 and v >= 0.02)
    def read(self):
        self.v = float(self.inst.get_voltage())
        return self.v
    def set(self, v):
        self.v = v
        self.inst.set_voltage(v)

class AWG70002AFrequency(AWG70002ABase):
    def validate(self, v):
        v = convert(v)
        return (v >= convert("1Hz") and v <= convert("400MHz"))
    def read(self):
        self.v = float(self.inst.get_frequency())
        return self.v
    def set(self, v):
        self.v = convert(v)
        self.inst.set_frequency(v)

class AWG70002ADutyCycle(AWG70002ABase):
    def validate(self, v):
        return (v >= 0.1 and v <= 99.9)
    def read(self):
        self.v = float(self.inst.get_duty())
        return self.v
    def set(self, v):
        self.v = v
        self.inst.set_duty(v)

class AWG70002AFileNumber(AWG70002ABase):
    def validate(self, v): return True
    def read(self):
        self.v = self.inst.get_current_filename()
        return self.v
    def set(self, v):
        self.inst.select_file(v)
        self.v = v
        
class AWG70002A(object):
    SINE = "SINE"
    TRI = "TRI"
    SQUARE = "SQU"
    RAMP_RISE = "RAMP_R"
    RAMP_FALL = "RAMP_F"
    GAUSSIAN = "GAUS"
    EXP_RISE = "EXPR"
    EXP_DECAY = "EXPD"
    PULSE = "PULS"
    DC = "DC"
    NOISE = "NOIS"
    def __init__(self, address, channel):
        # Connect to the device
        rm = pyvisa.ResourceManager()
        self.instr = rm.open_resource(address)
        if type(self.instr) is pyvisa.resources.gpib.GPIBInstrument:
            self.instr.write_termination = None
            self.instr.read_termination  = None
        if type(self.instr) is pyvisa.resources.TCPIPInstrument:
            self.instr.write_termination = '\n'
            self.instr.read_termination  = '\n'
        self.mode = self.get_mode()
        self.channel = channel
        self.directory = r'C:\Program Files\Tektronix\AWG70000\Samples' #r'C:\Users\OEM\Documents'
        self.set_directory(self.directory)
    def _execute(self, msg):
        #print(msg)
        if msg[0:10] == 'MMEM:DATA ':
            self.instr.write_binary_values(msg.split('</DataFile>')[0]+'</DataFile>',msg.split('</DataFile>')[1],datatype = 'b',is_big_endian=False)
        else:
            self.instr.write(msg)
        ret = self.instr.read() if '?' in msg.strip() else None
        #print(ret)
        self.instr.write(":SYST:ERROR?")
        error = self.instr.read()
        #print(error)
        if error[0] != '0' and 'No Error' not in error and 'No error' not in error:
            raise Exception(error)
        return ret
    def _write_raw(self, msg):
        self.instr.write_raw(msg)
        self.instr.write(":SYST:ERROR?")
        error = self.instr.read()
        if error[0] != '0':
            raise Exception(error)
    def on(self, state=True):
        self._execute(":AWGCONTROL:RUN" if state else ":AWGCONTROL:STOP")
    def off(self): self.on(False)
    def set_mode(self,mode):
        '''
        Switch instrument mode between function generator and arbitrary waveform generator.

        Inputs:
            mode ('FGEN' or 'AWG')
        Returns:
            NONE
        '''
        self._execute("INST:MODE "+mode)
        self.mode = mode
    def get_mode(self):
        '''
        Retrieve instrument mode, which may be either function generator ('FGEN') or arbitrary waveform generator ('AWG').

        Inputs:
            NONE
        Returns:
            mode (str)
        '''
        self.mode = self._execute("INST:MODE?").split('\n')[0]
        return self.mode
    def set_frequency(self, freq):
        'in Hz'
        if self.mode == 'FGEN':
            self._execute("FGEN:CHANNEL"+str(self.channel)+":FREQUENCY "+str(freq))
        else:
            self._execute("CLOCK:SRATE "+str(freq))
    def get_frequency(self):
        if self.mode == 'FGEN':
            return float(self._execute("FGEN:CHANNEL"+str(self.channel)+":FREQUENCY?"))
        else:
            return float(self._execute("CLOCK:SRATE?"))
    def set_amplitude(self, amp):
        '''
        in volts, peak-to-peak, 0.25V-0.5V
        '''
        if self.mode == 'FGEN':
            self._execute("FGEN:CHANNEL"+str(self.channel)+":AMPLITUDE " + str(amp))
        else:
            if amp > 0.5 or amp < 0.25:
                raise ValueError('Invalid amplitude. Amplitude range is between 250mV_pp and 500mV_pp.')
            else:
                self._execute(':VOLT:AMPL '+str(amp))#self._execute('WLIS:WAV:AMPL "' + self.get_waveform() + '",' + str(amp))
    def awg_set_amplitude(self, waveform, amp):
        '''
        no file extension
        in volts, peak-to-peak, 0.25V-0.5V
        '''
        if amp > 0.5 or amp < 0.25:
            raise ValueError('Invalid amplitude. Amplitude range is between 250mV_pp and 500mV_pp.')
        self._execute('WLIS:WAV:AMPL "' + waveform + '",' + str(amp))
    def get_amplitude(self):
        if self.mode == 'FGEN':
            return float(self._execute("FGEN:CHANNEL"+str(self.channel)+":AMPLITUDE?"))
        else:
            return self._execute('WLIS:WAV:AMPL? "' + self.get_waveform() +'"')
    def awg_get_amplitude(self, waveform):
        '''
        no file extension
        '''
        return self._execute('WLIS:WAV:AMPL? "' + waveform +'"')
    def add_waveform_to_list(self, filename):
        self._execute('MMEM:OPEN "{}"'.format(self.directory+'\\'+filename))
        time.sleep(0.01)
    def set_waveform(self, waveform):
        '''
        Set the waveform being output by the device.
        FGEN mode:
            Input: waveform name (str)
        AWG mode:
            Input: filename of waveform, including file extension. Note that the file must be in
                   the current directory. (str)
        '''
        if self.mode == 'FGEN':
            symmetry = None
            if waveform == 'RAMP_R': waveform,symmetry = 'TRI',100
            elif waveform == 'RAMP_F': waveform,symmetry = 'TRI',0.0001
            elif waveform == 'TRI': symmetry = 50
            self._execute("FGEN:CHANNEL"+str(self.channel)+":TYPE "+waveform)
            if symmetry: self._execute("FGEN:CHANNEL"+str(self.channel)+":SYMM "+str(symmetry))
        else:
            self.add_waveform_to_list(waveform)
            self._execute('SOURCE'+str(self.channel)+':WAVEFORM "'+waveform.split('.')[0]+'"')
    def get_waveform(self):
        '''
        If in FGEN mode, returns the name of the waveform. If in AWG mode, returns the filename of
        the waveform.
        '''
        if self.mode == 'FGEN':
            function = (self._execute("FGEN:CHANNEL"+str(self.channel)+":TYPE?").strip())
            symmetry = None
            if function == 'TRI': symmetry = self._execute("FGEN:CHANNEL"+str(self.channel)+":SYMM?")
            if symmetry == '100': function = 'RAMP_R'
            elif symmetry == '0': function = 'RAMP_F'
        else:
            #Returns the current file's name (or a blank string).
            function = self._execute('SOURCE'+str(self.channel)+':WAVEFORM?')[1:-1]
        return function
    def clear_waveform(self,filename):
        '''
        Clear file from AWG waveform list.
        Input: filename, without extension (str) OR
               'ALL', which will clear entire waveform list
        '''
        if filename == 'ALL':
            self._execute('WLIS:WAV:DEL '+filename)
        else:
            self._execute('WLIS:WAV:DEL "'+filename+'"')
    def set_duty(self, duty):
        assert(duty <= 99.9 and duty >= 0.1)
        self._execute(":AWGCONTROL:FG:PULSE:DCYCLE {}".format(duty))
    def get_duty(self):
        return float(self._execute(":AWGCONTROL:FG:PULSE:DCYCLE?"))
    def select_file(self, file_name):
        '''
            Input 'file_name': A number or string-encoded number which, when rounded to an integer, corresponds to a file (ex:<N>.WFM) or a string which is the literal file name.
            Output: None
            Ex: awg.select_file("EXAMPLE") # Select file "EXAMPLE.WFMX"
        '''
        self._execute('SOURCE:WAV "'+file_name.split('.')[0]+'"')
    def load_files(self, dir_or_files):
        '''
            Loads a set of files from the disk to the AWG, placing them in the root of the mass storage device.
            File names are preserved. Path information is discarded.
            Input 'dir_or_files': Either a sequence of file paths to be loaded into mass storage on the AWG OR a directory whose direct children (files only) should be loaded.
            Output: None
        '''
        if isinstance(dir_or_files, str) and os.path.isdir(dir_or_files):
            dir_or_files = [os.path.join(dir_or_files, i) for i in next(os.walk(dir_or_files))[2]]
        for file in dir_or_files:
            _, fname = os.path.split(file)
            with open(file, 'r') as f:
                data = f.read()
                data0 = data.split('</DataFile>')[0] + '</DataFile>'
                data1 = data.split('</DataFile>')[1]
                N = str(len(data1))
                data = data0 + "#" + str(len(N)) + N + data1
                print('MMEM:DATA "{}",{}'.format(fname, data).encode())
                self._write_raw('MMEM:DATA "{}",{}'.format(fname, data).encode())
                #self._execute('MMEM:DATA "{}",{}'.format(fname, data))
    def load_file_from_memory(self, fname, data):
        '''
            Loads a set of files from the disk to the AWG, placing them in the root of the mass storage device.
            File names are preserved. Path information is discarded.
            Input 'fname': String name of the destination file.
            Input 'data': Bytes the file is to contain.
            Output: None
        '''
        N = str(len(data))
        head = "#" + str(len(N)) + N
        self._write_raw(':MMEM:DATA "{}",{}'.format(fname,head).encode()+data)
                
    def delete_files(self, files = None):
        '''
            Deletes a given list of files OR deletes them all if none are specified.
        '''
        if not files: # Delete everything. EVERYTHING!
            files = self.list_files()
            if not files: return
        for file in files:
            self._execute(':MMEM:DEL "{}"'.format(file))
    def set_directory(self,directory):
        '''
            Select current directory of the file system on the AWG.
            Input: Directory path string. This method assumes path starts from C: drive, so input string
                   should start from subfolder in C:
        '''
        if directory[0:3] == 'C:\\':
            directory =  directory[3:]
        self.directory = directory
        self._execute('MMEM:CDIR "\\'+directory+'"')
    def get_directory(self):
        '''
            Retrieve current directory of the file system on the AWG.
            Output: Directory path string.
        '''
        directory = self._execute('MMEM:CDIR?')[1:-1]
        self.directory = directory
        if directory[0] == '\\':
            directory =  'C:'+directory
        return directory
    def list_files(self):
        '''
            Return a list of file-names stored in mass storage on the device.
        '''
        files = self._execute(":MMEM:CAT?").split(',')[2:] # Discard size information
        # Condition the list by taking the first token of every string of the form: "<fname>, [<dir?>,] <size>"
        files = [y for x,y in zip(files, [i.lstrip("'\"") for i in files]) if x != y]
        return files

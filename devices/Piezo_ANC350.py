# -*- coding: utf-8 -*-
"""
Created on July 31, 2018

@author: Jason Phoenix
"""

import platypus
from platypus import base
import pyvisa
import pyvisa
import time
import collections as co
from numpy import loadtxt, linspace

class Nudge(platypus.base.IteratedProcedure):
    def __init__(self, channel, start, end, step_target, pulses_per_step=10, voltage_limits=[20,50], PID=(1,0,0), max_history=10):
        self.channel = channel
        self.start = start
        self.end = end
        self.target_bite = (end-start)/step_target
        self.N = pulses_per_step
        self.voltage_limits = (min(voltage_limits), max(voltage_limits))
        self.PID = PID

        self.observed_bites = co.deque([],maxlen=max_history)
        self.V = channel.get_voltage()
        self.done = True
    def start(self):
        self.channel.set(self.start)
        self.done = False
    def advance(self):
        P,I,D = self.PID
        P = P*self.observed_bites[0] if len(self.observed_bites)>0 else 0.0
        I = I*np.sum(x)
        D = np.sum(np.diff(x))
        dV = P + I + D

        V = self.V - dV # Calculate the target voltage (negative feedback).
        self.V = max(voltage_limits[0], min(voltage_limits[1], V)) # Clamp the target voltage to the allowed range.
        direction = sign(self.end-self.start)

        self.set_voltage(self.V)
        self.channel.step_by(direction*self.N, settings_check=False) # Override automatic voltage reset.

        p0 = self.channel.read()
        time.sleep(0.1)
        p1 = self.channel.read()
        self.observed_bites.append(abs(p1-p0)-abs(self.target_bite))
    def done(self):
        if direction * (p1-self.end) > 0:
            self.done=True
        return self.done
    def end(self): pass

class Piezo_ANC350(base.Channel):
    
    '''
    Controls the position of the specified nano positioning stage, specifically
    using the ANC350 controller.

    To declare it: Piezo_x = Piezo(name, ID, Piezo_Controller)

    Piezo ID must be 'X', 'Y', or 'Z'. This will allow the program to identify
    the device more easily.    
    '''    
    def __init__(self, name, piezo_ID, piezo_controller, *args, mc_temp = None, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided channel name for this instrument
        self.setname(name)

        # For every channel, write self.mychannel = mychannel
        self.piezo_controller = piezo_controller
        self.piezo_ID = piezo_ID

        #Note the ANC300 axis. Set the LUT file which will be used to set position.
        #LUT file is unique for each device. Check serial numbers to ensure correct
        #file is used for each. Set type of piezo as well (i.e. x, z)
        if self.piezo_ID == 'X':
            self.controller_axis = 0
            file = r'C:\platypus\devices\ANC350_Library\LUT and aps Files\ANPx101-A3-917.LUT'
            self.piezo_controller.inst.loadLUT(0,file)
            self.piezo_controller.inst.selectActuator(0,3)
        elif self.piezo_ID == 'Y':
            self.controller_axis = 1
            file = r'C:\platypus\devices\ANC350_Library\LUT and aps Files\ANPx101-A3-924.LUT'
            self.piezo_controller.inst.loadLUT(1,file)
            self.piezo_controller.inst.selectActuator(1,3)
        elif self.piezo_ID == 'Z':
            self.controller_axis = 2
            file = r'C:\platypus\devices\ANC350_Library\LUT and aps Files\ANPz101-A4-911.LUT'
            self.piezo_controller.inst.loadLUT(2,file)
            self.piezo_controller.inst.selectActuator(2,4)

        #Read current position
        self.read()

        #Set standard voltage value to be used as step amplitude by ANC300
        self.control_voltage = 28#34
        if self.piezo_ID == 'Z':
            self.control_voltage = 34
        if self.piezo_ID == 'X':
            self.control_voltage = 24
        #Set standard frequency value to be used as step amplitude by ANC300
        self.control_frequency = 350 #500

        self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
        self.piezo_controller.set_frequency(self.controller_axis,self.control_frequency)

	#These are the maximum temperature above which the piezo will not move (when fridge is on),
        #and the value below which the temperature must drop for the piezo to continue moving.
        if mc_temp != None and 3.5<mc_temp.read()<4.5:
            self.max_temp, self.min_temp = 4.5,4
        else:
            self.max_temp, self.min_temp = 0.49,0.3
        self.mc_temp = mc_temp

        target_range = 500 #in nm
        self.piezo_controller.set_target_range(self.controller_axis,target_range)

        #Prevent the use of the .set() function by setting this variable to 1
        self.set_lock_on = 0

        self.display_messages = 0

        # Since there is no function for reading the offset voltage directly from the
        # ANC350, we will need to record the value in a variable, which assumes the
        # initial value is zero.
        self.offset_v = 0
        # Use this variable to determine the first time the offset is used after
        # startup. The user will be prompted to double check that the offset
        # voltage is actually the same as the value recorded by self.offset_v.
        self.first_offset = 1

    def validate(self,value):
        return True
        #This function ensures that the values input by the user are valid.

    def step_by(self,steps,settings_check = True):
        '''
        Moves the piezo by a specific number of steps. Step size determined
        by ANC300 voltage. Does not update position value. Optional keyword
        argument allows user to determine if step voltage and frequency
        should be set/checked before stepping.

        Inputs:
            Number Of Steps (int)
        Optional Keyword Inputs:
            settings_check (True or False, default = True)
        Returns:
            None
        '''
        if self.mc_temp != None and self.mc_temp.read() > self.max_temp:
            print('Temperature too hot. Waiting to cool down to '+str(self.min_temp)+'K before continuing to move piezo.')
            while True:
                if self.mc_temp.read() < self.min_temp:
                    time.sleep(10)
                else:
                    break

        if settings_check == True:
            #Set piezo controller voltage.
            self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
            #Set piezo controller frequency.
            self.piezo_controller.set_frequency(self.controller_axis,int(self.control_frequency))
        #Step piezo by a certain number of steps.
        self.piezo_controller.step_by(self.controller_axis,steps)

    def offset(self, voltage):
        '''
        Documentation:
        Moves the piezo of a given axis using a DC offset voltage (range ~800nm at 4K).
        Input:
            voltage (float)
        Return:
            None
        '''
        if self.first_offset == 1:
            print('The offset voltage cannot be read directly from the ANC350.\nIt is instead stored in the attribute offset_v. Please ensure\nthe current value of offset_v is equal to the actual offset voltage being output.\noffset_v = '+str(self.offset_v))
            self.first_offset = 0
        else:
            self.piezo_controller.offset(self.controller_axis,voltage,self.offset_v)
            self.offset_v = voltage

    def get_offset(self):
        '''
        Documentation:
        Gets the piezo DC offset voltage (0V to 60V).
        ***Note: This function does not read the offset from the instrument, but
                 merely returns the most recent voltage set by the user through
                 Platypus. It will show a value of zero when Platypus first starts,
                 regardless of the actual voltage being output.
        Input:
        None
        Return:
        Offset voltage in V
        '''
        return self.offset_v

    def set(self, new_position, settings_check = True):
        '''
        Moves the piezo to the specific desired position.

        Linear Piezos: Positions given in mm, as measured from the center
                       of the range of motion. Total range of motion is
                       approximately 0mm to 5mm.

        Rotating Piezo: Positions given in degrees. Ranges between 0 and
                        ~335 degrees due to position encoder length.

        Inputs:
            New Position (float)
        Returns:
            None  
        '''

        if settings_check == True:
            #Set piezo controller voltage.
            self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
            #Set piezo controller frequency.
            self.piezo_controller.set_frequency(self.controller_axis,int(self.control_frequency))

        if self.set_lock_on == 1:
            print('Cannot move while self.set_lock_on == 1')
        elif new_position > 5:
            print('Cannot set to position greater than 5mm.')

        elif new_position < 0:
            print('Cannot set to position less than 0mm.')

        #Otherwise:
        else:
            #Display a message indicating that piezo is currently in motion.
            self.print_message(1,0)
            #Now move piezo into desired position.
            self.piezo_controller.set_to_position(self.controller_axis,new_position)
            time.sleep(0.2)
            #Set position value to be new measured voltage.
            self.read()

    def move_by(self,distance, settings_check = True):
        '''
        Moves the piezo by the specified distance in mm. (Total range
        of motion is approximately 0mm to 5mm.)

        NOTE: If specified distance is outside range of piezo, programme will
              move piezo to maximum/minimum.

        Inputs:
            Distance in mm (float)
        Returns:
            None  
        '''

        if settings_check == True:
            #Set piezo controller voltage.
            self.piezo_controller.set_voltage(self.controller_axis,self.control_voltage)
            #Set piezo controller frequency.
            self.piezo_controller.set_frequency(self.controller_axis,int(self.control_frequency))

        #Display message indicating piezo is moving.
        self.print_message(1,0)
        
        #Move piezo desired distance.
        self.piezo_controller.move_by_distance(self.controller_axis,distance)

        #Read current position.
        self.read()

    #Print a pre-set message
    def print_message(self,message_type,voltage):
        #If message display function has not been disabled:
        if self.display_messages == 1:
            print("")
            if message_type == 1:
                if self.piezo_ID == 'R':
                    print("Please wait while piezo is repositioned. May take several minutes, depending on travel distance.")
                else:
                    print("Please wait while piezo is repositioned. May take up to a minute, depending on travel distance.")
                print("(Wait for new position to be displayed on screen before proceeding.)")
            elif message_type == 2:
                if self.piezo_ID != 'R':
                    print("New Piezo "+self.piezo_ID+" Location Based On Calibration Data: "+str(round(self.voltage_to_location(voltage),7))+"mm")
                else:
                    print("New Piezo "+self.piezo_ID+" Location Based On Calibration Data: "+str(round(self.voltage_to_location(voltage),7))+" deg")
            elif message_type == 3:
                print('Piezo motion range exceeded. Piezo will move to edge of motion range.')
            print("")

    def stop(self):
        self.piezo_controller.stop()

    def read(self):
        '''
        Retrives piezo position in mm.

        Inputs:
            None
        Returns:
            Position (float)
        '''
        #Read current position voltage.
        self.position = self.piezo_controller.get_position(self.controller_axis)
        #If the value has too many decimal point, round to 7 decimal points.
        if len(str(self.position).split('.')[-1]) > 7:
            self.position = round(self.position,7)
        #Update platypus state monitor.
        platypus.update()
        #Return read value.
        return self.position

    def Nudge(self, *args, **kwargs): return Nudge(self, *args, **kwargs)

    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.position

    def gettype(self):
        #If this is a control channel, 'c' should be returned. If it is a
        #measurement channel, 'm' should be returned.
        return 'c'

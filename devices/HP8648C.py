'''

Copyright (c) 2019, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import platypus
import pyvisa
import pyvisa
import time
from math import isclose, inf


class HP8648CBase(platypus.base.Channel):
    def __init__(self, parent, name, limits=(-inf, inf)):
        self.setname(name)
        self.parent = parent
        self.limits = limits
        self.v = 0.0

    def validate(self, v):
        low, high = min(self.limits), max(self.limits)
        return (v >= low and v <= high)
    
    def stop(self): pass
    def getvalue(self): return self.v
    def gettype(self): return 'c'
    
    def setlimits(self, low, high):
        self.limits = (low, high)
    def getlimits(self):
        return self.limits


class HP8648CFreq(HP8648CBase):
    def validate(self, v):
        return (v >= 0.1 and v <= 3200)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_frequency(v)
        else:
            raise ValueError("Invalid Frequency")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_frequency()
        return self.getvalue()
        


class HP8648CPower(HP8648CBase):
    #def validate(self, v):
    #    return (v >= -100 and v <= 0) and super().validate(v)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_power(v)
        else:
            raise ValueError("Invalid Power")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_power()
        return self.getvalue()
        



class HP8648C(object):
    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        #Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                                     write_termination="\n",
                                     read_termination="\n")
        #print(self.inst.read())


    def write(self, msg):
        self.inst.write(msg)
        return None

    def query(self, msg):
        self.inst.write(msg)
        return self.inst.read()
    

    def set_frequency(self, freq):
        '''Change the frequency in MHz'''
        msg = "SOUR:FREQ:CW {}MHZ".format(freq)
        self.write(msg)

    def get_frequency(self):
        '''Read the frequency in MHz'''
        msg = "SOUR:FREQ:CW?"
        return float(self.query(msg))/1000000
        

    def set_power(self, power):
        '''Change the power level in dBm'''
        msg = "SOUR:POW:LEV:IMM:AMPL {}DBM".format(power)
        self.write(msg)

    def get_power(self):
        '''Read the power level in dBm'''
        msg = "SOUR:POW:LEV:IMM:AMPL?"
        return float(self.query(msg))

    

    def on(self, status=True):
        '''Turn on rf output or, if the argument is false, turn it off'''
        msg = "OUTP:STAT 1" if status else "OUTP:STAT 0"
        self.write(msg)

    def off(self):
        '''Turn off rf output'''
        self.on(False)

    


    def F(self, name="HP8648C Frequency (MHz)", limits=(-inf, inf)):
        '''
        Return a frequency control channel with 'name'
        specified as positional argument.
        Units are in MHz.
        '''
        return HP8648CFreq(self, name, limits)

    def L(self, name="HP8648C Power (dBm)", limits=(-100, 0)):
        '''
        Return a power control channel with 'name'
        specified as positional argument.
        Setting is in dBm.
        '''
        return HP8648CPower(self, name, limits)

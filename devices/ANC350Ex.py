import time

import platypus
from platypus.channelmixins import RangeLimited

class NudgeAuto(platypus.base.IteratedProcedure):
    def __str__(self): return "Move {} from {} to {} in approximately {} points.".format(self.channel.getname(), self.s, self.e, self.step_target)
    def __init__(self, channel, start, end, step_target):
        self.channel = channel
        self.s = start
        self.e = end
        self.step_target = step_target
        self.target_bite = (end-start)/(step_target-1)
        print(self.target_bite)
        self.d = True
        self.first = False
    def start(self):
        self.channel.set(self.s, stop=True)
        self.d = False
        self.first = True
    def advance(self):
        assert(not self.d)
        if self.first == True:
            self.first = False
        else:
            self.channel.move_by(self.target_bite)
    def done(self):
        if (self.e-self.s) * (self.channel.getvalue()-self.e) > 0:
            self.d=True
        return self.d
    def end(self): pass

class NudgeFixed(platypus.base.IteratedProcedure):
    def __str__(self): return "Move {} from {} to {} in single Piezo steps.".format(self.channel.getname(), self.s, self.e)
    def __init__(self, channel, start, end, steps=1):
        self.channel = channel
        self.s = start
        self.e = end
        sign = 1 if end-start >= 0 else -1
        self.steps = sign*abs(steps)
        self.d = True
        self.first = False
    def start(self):
        V = self.channel.get_voltage()
        self.channel.set_voltage(V+10)
        try:
            self.channel.set(self.s)
            self.channel.stop()
        finally:
            self.channel.set_voltage(V)
        self.d = False
        self.first = True
    def advance(self):
        assert(not self.d)
        if self.first:
            self.first = False
        else:
            self.channel.step(self.steps) # Override automatic voltage reset.
    def done(self):
        if (self.e-self.s) * (self.channel.getvalue()-self.e) > 0:
            self.d=True
        return self.d
    def end(self): pass

class ANC350_DCChannel(RangeLimited, platypus.base.Channel):
    '''
    Controls the DC voltage of the specified piezo positioning stage, specifically
    using the ANC350 controller.

    Inputs:
        - name: The desired name of the channel.
        - axis: 0,1,2 (X,Y,Z)
        - controller: an ANC350_base object.
        - target_range: tolerance in nm.
        - limits (optional): tuple of 2 numbers representing the allowed range of positions
    '''
    def __init__(self, name, axis, controller, limits=None, **kwargs):
        if limits is None: limits = [0.0, 70.0]

        # Use the provided channel name for this instrument
        self.setname(name)

        self.axis = axis
        self.controller = controller

        self.v = 0.0
        self.read()

        super().__init__(limits = limits, **kwargs)
    def read(self): return self.getvalue()
    def getvalue(self): return self.v
    def set(self, v):
        self.controller.offset(self.axis, v, self.getvalue())
        self.v = v
    def stop(self):
        self.controller.stop()
    def gettype(self):
        #If this is a control channel, 'c' should be returned. If it is a
        #measurement channel, 'm' should be returned.
        return 'm'

class ANC350_AutoMover(RangeLimited, platypus.base.Channel):
    '''
    Controls the position of the specified nano positioning stage, specifically
    using the ANC350 controller.

    Inputs:
        - name: The desired name of the channel.
        - axis: 0,1,2 (X,Y,Z)
        - controller: an ANC350_base object.
        - target_range: tolerance in nm.
        - limits (optional): tuple of 2 numbers representing the allowed range of positions
    '''
    def __init__(self, name, axis, controller, target_range=500, limits=None, **kwargs):
        if limits is None: limits = [0.0, 5.0]

        # Use the provided channel name for this instrument
        self.setname(name)

        self.axis = axis
        self.controller = controller

        self.target_range = target_range

        self.auto_off = False

        self.read()

        super().__init__(limits = limits, **kwargs)

    def set_target_range(self, target_range):
        if abs(target_range) > 1000: print("Are you sure you meant to set this to more than a micron?")
        self.target_range = target_range
    def get_target_range(self, target_range): return self.target_range

    def set_voltage(self, v): self.controller.set_voltage(self.axis, v)
    def get_voltage(self): return self.controller.get_voltage(self.axis)

    def step(self, n_steps):
        '''
        Moves the piezo by a specific number of steps. Step size determined
        by ANC300 voltage. Does not update position value. Optional keyword
        argument allows user to determine if step voltage and frequency
        should be set/checked before stepping.

        Inputs:
            Number Of Steps (int)
        Returns:
            None
        '''
        self.controller.step_by(self.axis,n_steps)
    def set(self, pos, stop=None):
        '''
        Moves the piezo to the specific desired position.

        Linear Piezos: Positions given in mm, as measured from the center
                       of the range of motion. Total range of motion is
                       approximately 0mm to 5mm.

        Rotating Piezo: Positions given in degrees. Ranges between 0 and
                        ~335 degrees due to position encoder length.

        Inputs:
            New Position (float)
        Returns:
            None  
        '''

        #Now move piezo into desired position.
        self.controller.inst.setTargetPosition(self.axis,pos*1E-3) # Set target position (in mm).
        self.controller.inst.startAutoMove(self.axis,1,0) # Engage automatic absolute motion.
        err = 0
        while True:
            platypus.read(self)
            time.sleep(0.1)
            status = self.controller.inst.getAxisStatus(self.axis)
            if status[3]: break # This bit indicates target reached
            if status[4] or status[5]: # These bits indicate end of travel detected (forward and reverse direction respectively).
                self.controller.inst.startAutoMove(self.axis,0,0)
                raise Exception('End of travel detected (or else the positioner is stuck).')
        #Set position value to be new measured voltage.
        if stop is None: stop = self.auto_off
        if stop: self.controller.inst.startAutoMove(self.axis,0,0) # Disable automatic motion.
        self.read()
    def move_by(self,pos, stop=None):
        '''
        Moves the piezo by the specified distance in mm. (Total range
        of motion is approximately 0mm to 5mm.)

        NOTE: If specified distance is outside range of piezo, programme will
              move piezo to maximum/minimum.

        Inputs:
            Distance in mm (float)
        Returns:
            None  
        '''
        self.controller.inst.setTargetPosition(self.axis,pos*1E-3) # Set target position (in mm).
        self.controller.inst.startAutoMove(self.axis,1,1) # Engage automatic absolute motion.
        #Move piezo desired distance.
        while True:
            platypus.read(self)
            time.sleep(0.1)
            status = self.controller.inst.getAxisStatus(self.axis)
            if status[3]: break # This bit indicates target reached
            if status[4] or status[5]: # These bits indicate end of travel detected (forward and reverse direction respectively).
                self.controller.inst.startAutoMove(self.axis,0,0)
                raise Exception('End of travel detected (or else the positioner is stuck).')
        if stop is None: stop = self.auto_off
        if stop: self.controller.inst.startAutoMove(self.axis,0,0) # Disable automatic motion.
        #Read current position.
        self.read()
    def read(self):
        '''
        Retrives piezo position in mm.

        Inputs:
            None
        Returns:
            Position (float)
        '''
        #Read current position voltage.
        self.position = self.controller.get_position(self.axis)
        #If the value has too many decimal points, round to 7 decimal points.
        if len(str(self.position).split('.')[-1]) > 7:
            self.position = round(self.position,7)
        #Update platypus state monitor.
        platypus.update(self)
        #Return read value.
        return self.position
    def NudgeAuto(self, *args, **kwargs): return NudgeAuto(self, *args, **kwargs)
    def NudgeFixed(self, *args, **kwargs): return NudgeFixed(self, *args, **kwargs)
    def stop(self):
        self.controller.stop()
    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.position
    def gettype(self):
        #If this is a control channel, 'c' should be returned. If it is a
        #measurement channel, 'm' should be returned.
        return 'm'

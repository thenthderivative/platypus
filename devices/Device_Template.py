# -*- coding: utf-8 -*-
"""
Created on April 13 2016

@author: Louis Gaudreau
"""

import platypus
import pyvisa
import pyvisa

#Give a name to the device in the next line
class Name_of_Device(platypus.base.Channel):
    
    '''
    Documentation
    
    Required Inputs: (name,address)
    '''    
    
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided channel name for this instrument
        self.setname(name)

        # Create the instrument. Ensure that baud rate, data bits, and other
        # settings are correct for the instrument being used.
        rm = pyvisa.ResourceManager()
        lib = rm.visalib
        self.inst = rm.open_resource(address,
                baud_rate = 9600,
                data_bits = 8,
                stop_bits = pyvisa.constants.StopBits.one,
                parity=pyvisa.constants.Parity.none,
                read_termination = '\r',
                write_termination = '\r\n')
        
        #Any other important initial settings should be configured here. For
        #example, if the device is required to start in a specific position, it
        #should be instructed to move to that position here.
        #Examples: self.inst.write('initial setting that you need')
        #          self.inst.read('initial setting that you need')
        #          self.inst.query('initial setting that you need')

    def validate(self): return True
        #This function ensures that the values input by the user are valid.
        
    def set(self, setting):
        #This function allows one to alter the settings of the device.
        #May be used to adjust internal, digital settings of device, or to
        #induce mechanical motion.
        #for example:
        self.inst.write('setting')
        #Update platypus so that the new value shows in the state monitor
        platypus.update()
        
        
    def stop(self): pass
        #Instructs the device to cease all operations in progress. For example:
        #self.inst.write('stop')
    
    
    def read(self):
        #Reads the value from the channel. May require a query statement
        #instead of read, depending on the instrument and how it is being used.
        #Updates the attribute.
        self.value = self.inst.read()
        return self.getvalue()
        
        
    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.value
        
        
    def gettype(self):
        #If this is a control channel, 'c' should be returned. If it is a
        #measurement channel, 'm' should be returned.
        return 'c'

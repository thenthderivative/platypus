# -*- coding: utf-8 -*-
"""
Created on Friday Jan 15 16:34:02 2016

@author: Louis Gaudreau
@author: Alex Bogan
"""

import platypus
import pyvisa
import numpy as np
import time


class TSG4102A(platypus.base.Channel):
    '''
    Controls the TSG 4102A RF Signal Generator.
    
    Inputs:
        name (str)
        address (str)
    '''
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address)

        self.setname(name)
        
        self.power = self.get_power()
        self.frequency = self.get_frequency()

    def validate(self,val):
        return True

    def stop(self): pass

    def RF_on(self):
        '''
        Enable the RF output.

        Inputs:
            None
        Outputs:
            None
        '''
        self.inst.write('ENBR1')

    def RF_off(self):
        '''
        Disable the RF output.

        Inputs:
            None
        Outputs:
            None
        '''
        self.inst.write('ENBR0')

    def get_state(self):
        '''
        Get the RF output state (i.e. ON or OFF).

        Inputs:
            None
        Outputs:
            State (str)
        '''
        val = int(self.inst.query('ENBR?').replace('\r\n',''))
        if val == 0:
            return 'OFF'
        elif val == 1:
            return 'ON'
        else:
            return 'Unknown Error'

    def set_power(self,amplitude):
        '''
        Set the amplitude of the output RF signal.

        Inputs:
            Amplitude in dBm (float)
        Outputs:
            None
        '''
        self.inst.write('AMPR '+str(amplitude))
        self.power = amplitude
        platypus.update(self)

    def get_power(self):
        '''
        Get the amplitude of the output RF signal.

        Inputs:
            None
        Outputs:
            Amplitude in dBm (float)
        '''
        amp = float(self.inst.query('AMPR?'.replace('\r\n','')))
        self.power = amp
        platypus.update(self)
        return amp

    def set_frequency(self,frequency):
        '''
        Set the frequency of the output RF signal.

        Inputs:
            Frequency in Hz (float)
        Outputs:
            None
        '''
        self.inst.write('FREQ '+str(frequency))
        self.frequency = frequency

    def get_frequency(self):
        '''
        Get the frequency of the output RF signal.

        Inputs:
            None
        Outputs:
            Frequency in Hz (float)
        '''
        freq = float(self.inst.query('FREQ?'.replace('\r\n','')))
        self.frequency = freq
        return freq

    def set(self,val):
        self.set_power(val)

    def read(self):
        return self.get_power()

    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.power
        
    def gettype(self):
        #Control channel, so 'c' is returned.
        return 'c'

'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import pyvisa
import time
import platypus
import math

import threading

class DAC488HRChannel(platypus.base.Channel):
    rangedict = {
        0:(  0.0,  0.0),
        1:(- 1.0,  1.0),
        2:(- 2.0,  2.0),
        3:(- 5.0,  5.0),
        4:(-10.0, 10.0),
        5:(  0.0,  1.0),
        6:(  0.0,  2.0),
        7:(  0.0,  5.0),
        8:(  0.0, 10.0)
    }
    def __init__(self, dac, name, pnum, safestepsize=0.010, delay=0.1, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Remember the dac device and port number this channel corresponds to.
        self.dac = dac
        self.pnum = pnum
        
        # Set the name of the channel.
        self.setname(name)

        # Set parameters used keep operation safe.
        self.setsafestepsize(safestepsize)
        self.setdelay(delay)
        self.tlast = time.monotonic()-delay

        # Don't let the user do anything until they specify limits!
        self.low = 0.0
        self.high = 0.0

        # Perform a hardware read
        self.read()

    ## Standard methods (see Platypus documentation)

    def read(self):
      with self.lock:
        self.v = self.dac.getvoltage(self.pnum)
        return self.getvalue()

    
    def aread(self): return self.read()

    # Steps in 'safestepsize' steps, waiting 'delay'
    # between steps, until the voltage reaches V.
    def set(self, V):
        with self.lock:
            self.stopped = False
            if math.isclose(V, self.getvalue()): return
        while True:
            if self.stopped or self.steptowards(V): break
            platypus.update(self)
            self.wait()
        platypus.update(self)
    def getvalue(self):
      with self.lock:
        return self.v
    def validate(self, value):
      with self.lock:
        def within(v, l, h): return v>=l and v<=h
        return within(value, self.low, self.high)\
           and within(value, *self.getrange())
    def stop(self):
      with self.lock:
        self.stopped = True
    def gettype(self): return 'c'

    ## Extra non-standard methods.

    # Take a single step toward the target voltage.
    def steptowards(self, V):
      with self.lock:
        if not self.validate(V):
            print("Target voltage invalid")
            raise ValueError
        if not self.validate(self.getvalue()):
            print("Current voltage invalid! Change or do a read() first.")
            raise ValueError
        def sign(v): return 1.0 if v >= 0 else -1.0
        Vold = self.getvalue()
        s = sign(V - Vold) # Sign of voltage difference.
        d = abs(V - Vold)  # Magnitude of voltage difference.
        doneness = True # Did we reach the voltage this step?
        # Don't try to make a bigger step than 'safestepsize'
        if d > self.safestepsize:
            d = self.safestepsize
            doneness = False
        self.v = Vold + s*d
        self.dac.setvoltage(self.pnum, self.v)
        self.tlast = time.monotonic() # Update last set operation
        return doneness
        
    # Returns once 'delay' seconds have elapsed since the last
    # voltage change operation through this channel.
    # If 'delay' has already elapsed, returns immediately.
    def wait(self):
      with self.lock:
        waittime = self.tlast - time.monotonic() + self.delay
#        print(self.tlast, time.monotonic(), self.delay, waittime)
        if  waittime > 0.0001:
#            print("W!")
            time.sleep(waittime)

    # Sets the maximum safe step size (in V).
    def setsafestepsize(self, safestepsize):
      with self.lock:
        self.safestepsize = abs(safestepsize)
    def getsafestepsize(self):
      with self.lock:
        return self.safestepsize

    # Sets the delay time (in seconds) between operations.
    def setdelay(self, delay):
      with self.lock:
        self.delay = abs(delay)
    def getdelay(self):
      with self.lock:
        return self.delay

    # Get and set the voltage limits for this channel.
    def getlimits(self):
      with self.lock:
        return (self.low, self.high)
    def setlimits(self, low, high):
      with self.lock:
        # Swap them if necessary
        if low > high:
            low, high = high, low
        if any(lim < -10.0 or lim > 10.0 for lim in (low, high)):
            print("Invalid limit. The DAC can't output more than 10V!")
            raise ValueError
        if self.getvalue() < low or self.getvalue() > high:
            print("Can't set limits. Current value outside limits!")
            raise ValueError
        self.low = low
        self.high = high

    # Set the range (takes an integer, which can be one of
    # DAC488HR.[
    # GROUNDED, BIP1V, BIP2V, BIP5V, BIP10V, POS1V, POS2V, POS5V, POS10V]
    def setrange(self, range):
      with self.lock:
        if range in self.rangedict:
            self.dac.setrange(self.pnum, range)
    # Retrieve the range as a tuple of floats (lowV, highV).
    def getrange(self):
      with self.lock:
        return self.rangedict[self.dac.ranges[self.pnum]]

class DAC488HR(object):
    GROUNDED = 0
    BIP1 = 1
    BIP2 = 2
    BIP5 = 3
    BIP10 = 4
    POS1 = 5
    POS2 = 6
    POS5 = 7
    POS10 = 8
    # For debug. Returns a list of strings describing the active status bits.
    def stb(self, stb):
        stbdict = {
            1:"Triggered",
            2:"End of Trigger Sequence",
            4:"Ready",
            8:"Error",
           16:"Message Available",
           32:"Event Status Register Bit [Message Available, etc.]",
           64:"Service Request Bit",
         128:"DMA Underrun"
        }
        return tuple(stbdict[stb&1<<i] for i in range(8) if stb&1<<i)

    def __init__(self, addr, reset=False, *args, **kwargs):
        super().__init__(*args, **kwargs)

        rm = pyvisa.ResourceManager()

        self.inst = rm.open_resource(addr)
        self.inst.timeout=2500

        if reset:
            self.write('*RX', False)
            time.sleep(2.0) # Communication doesn't work for about 1 second after reset.
        self.write('F0X')
        self.getranges()


    def read(self, block=True):
        time.sleep(0.05)
        stb = self.inst.stb
        while block and not stb & 16: #SPoll
            time.sleep(0.05)
            stb = self.inst.stb
#            print("Wait to read: ", DAC488HR.stb(stb))
        msg = self.inst.read()
#        print("Read: ", msg)
        return msg

    def write(self, message, block=True):
        time.sleep(0.05)
        stb = self.inst.stb
        while block and not stb & 4: #SPoll
            if stb & 8:
                errors = self.geterrors()
                raise Exception('Errors in DAC (address {}): [{}]'.format(self.inst.primary_address, errors))
            time.sleep(0.05)
            stb = self.inst.stb
#            print("Wait to write: ", DAC488HR.stb(stb))
#        print("Writing: ", message)
        self.inst.write(message)

    def query(self, message):
        self.write(message)
        return self.read()

    def setvoltage(self, pnum, v):
        if abs(v) < 1E-6: v = 0
        msg = "P{:d}V{:f}X".format(pnum+1, v)
        self.write(msg)
    def getvoltage(self, pnum):
        msg = "P{:d}V?X".format(pnum+1)
        return float(self.query(msg)[1:])

    def setrange(self, pnum, r):
        # Ensure that it's a valid port.
        # and that V = 0.0V!
        assert pnum in range(4), "Invalid port number"
        assert abs(self.getvoltage(pnum)) <= 0.0001, "Must zero voltage first!"
        msg = "P{}R{}X".format(pnum+1, r)
        self.ranges[pnum] = r
        self.write(msg)
    def getranges(self):
        ranges = self.query("P1R?P2R?P3R?P4R?X")
        self.ranges = [int(range) for range in ranges.split('R') if range]

    # Accepts an arbitrary name and a port number from 1 to 4 inclusive.
    # Returns a channel which controls the port so numbered on the front
    # panel of the device.
    def getchannel1to4(self, name, pnum, *args, **kwargs):
        assert pnum in range(1, 5), "Invalid port number.for this function"
        return DAC488HRChannel(self, name, pnum-1, *args)

    # Same as getchannel1to4, but indexes ports from 0 to 3.
    # This is, of course, the the proper way to number things ;).
    def getchannel(self, name, pnum, *args, **kwargs):
        assert pnum in range(4), "Invalid port number for this function"
        return DAC488HRChannel(self, name, pnum, *args)

    def geterrors(self):
        err = int(self.query("E?X")[1:])
        errdict = {
            #0:"No Error",
             1:"Invalid Device Dependent Command",
             2:"Invalid Device Dependent Command Option",
             4:"Command Conflict",
             8:"Calibration Enable",
            16:"Trigger Overrun",
            32:"Non-volatile Setup",
            64:"Non-volatile Calibration Constants",
           128:"DMA Underrun"
        }
        return [errdict[err & 1<<i] for i in range(8) if err & 1<<i]

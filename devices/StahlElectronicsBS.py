'''
Copyright (c) 2019, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@Author: Aviv Padawer-Blatt
'''

import platypus
import pyvisa
import pyvisa
import time
from numpy import linspace
import math

import threading


class DACChannel(platypus.base.Channel):
    def __init__(self, dac, portNum, name, safestepsize=0.020, delay=0.01, saferate=None, limits=(0.0,0.0), *args, **kwargs):
        '''
            Initialize a DAC channel.
            Parameters:
            - dac (DAC): instance of an instrument controller
            - portNum (int): the port number (starting from 1)
            - name (str): the name of the resulting Platypus channel
            - safestepsize (float): the maximum allowed step per delay interval
            - delay (float): number of seconds to wait between steps
            - saferate (float): volts per second. Overrides the delay parameter.
        '''

        # Run the (mandatory) superclass constructor.
        super().__init__(*args, **kwargs)

        # Remember the port number this channel corersponds to.
        self.dac = dac
        self.portNum = portNum

        # Set the name of the channel.
        self.setname(name)

        # Set parameters used to keep operation safe.
        self.setsafestepsize(safestepsize)
        self.setdelay(delay)
        if saferate: self.setrate(saferate) # Potentially overrides delay but not safestepsize.
        self.tlast = time.monotonic()- delay

        # Don't let the user do anything until they specify limits!
        self.low, self.high = sorted(limits)

        # Initialize power supply by reading its value.
        self.voltage = self.dac.getvoltage(self.portNum)

    ## Standard methods (see platypus documentation)

    def validate(self, value):
        # Check that voltage is within upper and lower limits.
        with self.lock:
            def within(v, l, h): return v>=l and v<=h
            return within(value, self.low, self.high)\
                   and within(value, *self.getrange())

    def stop(self):
        # Halt all processes where they are.
        self.stopped = True

    def gettype(self):
        # Control channel, so 'c' is returned.
        return 'c'
    
    def read(self):
        # Updates stored voltage in channel and returns voltage.
        with self.lock:
            return self.getvalue()

    def set(self, V):
        # Steps in 'safestepsize' steps, waiting 'delay'
        # between steps, until the voltage reaches V.
        
        with self.lock:
            self.stopped = False
            if math.isclose(V, self.getvalue()): return
        while True:
            with self.lock:
                if self.stopped or self.steptowards(V): break
                platypus.update(self)
                self.wait()
        platypus.update(self)

    def getvalue(self):
        # Returns last read voltage.
        with self.lock:
            return self.voltage

    ## Extra non-standard methods

    def steptowards(self, V):
        # Take a single step toward the target voltage.
        
        with self.lock:
            if not self.validate(V):
                print("Target voltage invalid")
                raise ValueError
            if not self.validate(self.getvalue()):
                print("Current voltage invalid! Change or do a read() first.")
                raise ValueError
            def sign(v): return 1.0 if v >= 0 else -1.0
            Vold = self.getvalue()
            s = sign(V - Vold) # Sign of voltage difference.
            d = abs(V - Vold)  # Magnitude of voltage difference.
            doneness = True # Did we reach the voltage this step?
            # Don't try to make a bigger step than 'safestepsize'
            if d > self.safestepsize:
                d = self.safestepsize
                doneness = False
            self.voltage = Vold + s*d
            self.dac.setvoltage(self.portNum, self.voltage)
            self.tlast = time.monotonic() # Update last set operation
            return doneness

    # Set and get voltage limits for this channel.
    def setlimits(self, low, high):
        with self.lock:
            # Swap limits if necessary.
            if low > high:
                low, high = high, low
            # Check that desired limits are within range.
            if any(lim < -1.0*self.dac.Vrange or lim > self.dac.Vrange for lim in (low, high)):
                print("Invalid limit. The DAC can't output more than {}V!".format(self.dac.Vrange))
                raise ValueError
            # Check that current voltage is within desired limits
            if self.getvalue() < low or\
               self.getvalue() > high:
                print("Can't set limits. Current value outside limits!")
                raise ValueError

            self.low = low
            self.high = high

    def getlimits(self):
        # Return current voltage limits.
        with self.lock:
            return (self.low, self.high)

    def getrange(self):
        # Retrieve voltage range of channel (specific to dac)
        # as a list of floats.
        with self.lock:
            return [-1.0*self.dac.Vrange, self.dac.Vrange]

    def wait(self):
        # Returns once 'delay' seconds have elapsed since the last
        # voltage change operation through this channel.
        # If 'delay' has already elapsed, returns immediately.
        
        with self.lock:
            waittime = self.tlast - time.monotonic() + self.delay
#           print(self.tlast, time.monotonic(), self.delay, waittime)
            if waittime > 0.0001:
#               print("W!")
                time.sleep(waittime)
        
    # Set and get maximum safe step size (in V).
    def setsafestepsize(self, safestepsize):
        with self.lock:
            self.safestepsize = max(1E-3, safestepsize) # At least 1 mV

    def getsafestepsize(self):
        with self.lock:
            return max(1E-3, self.safestepsize) # At least 1 mV

    # Set and get the delay time (in s) between operations.
    def setdelay(self, delay):
        with self.lock:
            self.delay = max(0.01, delay) # At least 10 ms.

    def getdelay(self):
        with self.lock:
            return max(0.01, self.delay) # At least 10 ms.

    # Set and get max safe ramp rate in V/s.
    def setrate(self, rate):
        with self.lock:
            self.setdelay(self.getsafestepsize()/abs(rate))

    def getrate(self):
        with self.lock:
            return self.getsafestepsize()/self.getdelay()

class DAC(object):
    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor.
        super().__init__(*args, **kwargs)

        # Create the instrument. Ensure that baud rate, data bits, and other
        # settings are correct for the instrument being used.
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                baud_rate = 115200,
                read_termination = '\r',
                write_termination = '\r')
        self.inst.timeout = 2500

        # Figure out what type of box this is.
        IDN = self.query("IDN")
        self.serial, self.Vrange, self.Nchs, *_ = IDN.split()
        self.Vrange = float(self.Vrange)
        self.Nchs = int(self.Nchs)
    # Sends command and reads reply from device.
    def query(self, msg):
        self.inst.write(msg)
        back = self.inst.read()
        if len(back) == 7 and back[0:5] == "ERROR":
            if back[5:7] == "01":
                return "Command not recognized."
            elif back[5:7] == "02":
                return "Channel number out of range."
            elif back[5:7] == "03":
                return "Channel voltage out of range."
        else:
            return back

    # Displays and sets voltage of port on DAC.
    def setvoltage(self, portNum, voltage):
        scale = self.voltagetoscale(voltage)
        msg = "{} CH{:02d} {:.6f}".format(self.serial, portNum, scale)
        return self.query(msg)
    # Queries voltage for port.
    def getvoltage(self, portNum):
        msg = "{} U{:02d}".format(self.serial, portNum)
        voltage = float(self.query(msg)[:-1].replace(',','.'))  # returns voltage as string
        return voltage
    # Spawns channel for specific DAC and port, with a given name.
    def getchannel(self, portNum, name, *args, **kwargs):
        if portNum > 0 and portNum <= self.Nchs:
            return DACChannel(self, portNum, name, *args, **kwargs)
        else:
            raise ValueError('Requested port # {} from a {} port DAC'.format(portNum, self.Nchs))
    # Convert to scale from voltage
    def voltagetoscale(self, voltage):
        return (voltage/self.Vrange/2 + 1/2)
    # Convert to voltage from scale
    def scaletovoltage(self, scale):
        return 2*self.Vrange * (scale - 1/2)
    '''
    # For debug. Returns a list of strings describing the active status bits.
    def stb(self, stb):
        stbdict = {
            1:"Triggered",
            2:"End of Trigger Sequence",
            4:"Ready",
            8:"Error",
            16:"Message Available",
            32:"Event Status Register Bit [Message Available, etc.]",
            64:"Service Request Bit",
            128:"DMA Underrun"
        }
        return tuple(stbdict[stb&1<<i] for i in range(8) if stb&1<<i)

    def geterrors(self):
        err = int(self.query("E?X")[1:])
        errdict = {
            #0:"No Error",
             1:"Invalid Device Dependent Command",
             2:"Invalid Device Dependent Command Option",
             4:"Command Conflict",
             8:"Calibration Enable",
            16:"Trigger Overrun",
            32:"Non-volatile Setup",
            64:"Non-volatile Calibration Constants",
           128:"DMA Underrun"
        }
        return [errdict[err & 1<<i] for i in range(8) if err & 1<<i]
        

    def read(self, block=True):
        time.sleep(0.05)
        stb = self.inst.stb
        while block and not stb & 16: #SPoll
            time.sleep(0.05)
            stb = self.inst.stb
#            print("Wait to read: ", DAC488HR.stb(stb))
        msg = self.inst.read()
#        print("Read: ", msg)
        return msg

    def write(self, message, block=True):
        time.sleep(0.05)
        stb = self.inst.stb
        while block and not stb & 4: #SPoll
            if stb & 8:
                errors = self.geterrors()
                raise Exception('Errors in DAC (address {}): [{}]'.format(self.inst.primary_address, errors))
            time.sleep(0.05)
            stb = self.inst.stb
#            print("Wait to write: ", DAC488HR.stb(stb))
#        print("Writing: ", message)
        self.inst.write(message)

    '''

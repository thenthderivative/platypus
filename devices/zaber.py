'''

Copyright (c) 2019, Fatemeh Khoshsiar and Alexander Bogan

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import threading
import platypus
import time

class ZaberPositioner(platypus.base.Channel):
    def __init__(self, name, address, device_number=None, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided name for this channel
        self.setname(name)

        # Create a connection.

        self.read()
        
        self.stop_event = threading.Event()
    def set(self, x):
        # TODO: Start the positioner.

        # Wait until the end of the motion (or until STOP is pressed) to return.
        self.stop_event.clear()
        platypus.addevent(self)
        
        # Wait until the positioner is done.
        while not self.stop_event.is_set():
            if not zaber_is_in_motion: break # FILL IN THIS TEST
            platypus.read(self)
            time.sleep(0.1)
        self.stop_event.clear()
        platypus.removeevent(self)

        # Update the value in the state monitor.
        platypus.read(self)
    def stop(self): self.stop_event.set()
    def read(self):
        self.value = ???
        return self.value
    def getvalue(self): return self.value
    def gettype(self): return 'c'

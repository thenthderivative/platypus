'''
Driver for ITC503 temperature controller

February 2021
author: Sophia Devinyak
'''

import platypus
import pyvisa

class ITC503Temperature(platypus.base.Channel):
    def __init__(self, name, instr, *args, channel=3, **kwargs):
        super().__init__(*args, **kwargs)
        self.setname(name)
        self.instr = instr
        self.channel = channel
        self.value = 0.0
    def set(self, value):
        if self.channel == 0:
            self.value = value
            self.instr.set_temp(value)
            platypus.update(self)
            return
        else:
            raise Exception('Cannot set sensor value. That is not how sensors work.')
    def read(self):
        self.value = self.instr.get_sensor_temp(self.channel)
        return self.value
    def getvalue(self): return self.value
    def gettype(self): return 'm'

class ITC503SensorTemperature(ITC503Temperature):
    def __init__(self, *args, channel=2, **kwargs):
        super().__init__(*args, channel=channel, **kwargs)

class ITC503SetpointTemperature(ITC503Temperature):
    def __init__(self, *args, channel=0, **kwargs):
        super().__init__(*args, channel=channel, **kwargs)

class ITC503(platypus.base.Channel):
    def __init__(self, name, address, *args, **kwargs):

        super().__init__(*args, **kwargs) # run the mandatory superclass constructor
        self.setname(name)                # use the provided name for this channel

        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                                     read_termination = '\r',
                                     write_termination = '\r',
                                     timeout=60000)

        self.value = 0.0
        self.set_control(3)

    # Alex removed "__del__" method that just causes errors in my experience.

    def set(self, value):
        self.set_temp(value)

    def read(self):
        self.value = self.get_temp()
        platypus.update(self)
        return self.getvalue()

    def getvalue(self):
        # returns the last read value stored in memory
        return self.value

    def gettype(self):
        # since this is a control device, this function returns 'c'
        return 'c'


    ######## PID commands

    def set_p(self, val):
        self.inst.query('P' + str(val))

    def get_p(self):
        return float(self.inst.query('R8')[1:])

    def set_i(self,val):
        self.inst.query('I' + str(val))

    def get_i(self):
        return float(self.inst.query('R9')[1:])

    def set_d(self,val):
        self.inst.query('D' + str(val))

    def get_d(self):
        return float(self.inst.query('R10')[1:])

    def auto_pid(self,switch):
        # Untested
        if str(switch).lower() in {'on', '1'}:
            self.inst.query('L1')
        elif str(switch).lower() in {'off', '0'}:
            self.inst.query('L0')
        else:
            raise ValueError("Valid inputs are 'ON'/1 to enable autoPID or 'OFF'/0 to disable autopid")

    def get_pid(self):
        return print("P = " + str(self.get_p()) +
                     " I = " + str(self.get_i()) +
                     " D = " + str(self.get_d()))

    ######## heater commands
    def set_htr_output(self, val):
        # set heater output as percentage of current heater limit
        if val >= 0 and val <100:
            self.inst.query('O' + str(val))
        else:
            raise ValueError("Output is set as a percentage of maximum heater output. Theresore, allowed values are in the range from 0 to 99.9")

    def set_max_htr(self, val):
        # set maximum heater value
        self.inst.query('M' + str(val))

    def get_htr_percent_output(self):
        # returns heater output as percentage of current limit
        return float(self.inst.query("R5")[1:])

    def get_htr_v_output(self):
        # returns heater output in volts (approximate value)
        return float(self.inst.query("R6")[1:])

    def set_htr_sensor(self, channel):
        # defines the sensor to be used for automatic control
        if channel in {1,2,3}:
            self.inst.query('H' + str(channel))
        else:
            raise ValueError("Valid channels to use for automatic heater sensor control are 1, 2 and 3")
    


    ######## sweep function
    def sweep(self,switch):
        if str(switch).lower() in {'on', '1', "start"}:
            self.inst.query('S1')
        elif str(switch).lower() in {'off', '0', "stop"}:
            self.inst.query('S0')
        else:
            raise ValueError("Valid inputs are 'ON'/'START'/1 to start sweep or 'OFF'/'STOP'/0 to stop sweep")


    ######## temperature functions
    def set_temp(self, value):
        self.inst.query('T' + str(value))

    def get_temp(self):
        # returns the current temperature setpoint
        return float(self.inst.query('R0')[1:])

    def get_sensor_temp(self,sensor):
        # reads the temperature on the channel
        if sensor in {0,1,2,3}:
            return float(self.inst.query('R' + str(sensor))[1:])
        else:
            raise ValueError("Valid sensor numbers are 0, 1, 2 and 3")

    def get_temp_error(self):
        return float(self.inst.query("R4")[1:])

    ######## other commands

    def get_frequency(self, channel):
        if channel in {1,2,3}:
            return float(self.inst.query('R1' + str(channel))[1:])
        else:
            raise ValueError("Valid frequency channels are 1, 2 and 3")

    ######## control commands
    def set_control(self, switch):
        if switch in {0,1,2,3}:
            self.inst.query('C' + str(switch))
        else:
            raise ValueError("Valid control inputs are 0 for local and locked (default state) \n1 for remote and locked (front panel disabled) \n2 for local and unlocked and \n3 for remote and unlocked (front panel active)") 
    

    def set_com_protocol(self,switch):
        if switch in {0,2}:
            self.inst.query('Q' + str(switch))
        else:
            raise ValueError("Valid communitation protocols are 0 - normal, default state; 2 - Sends <LF> after each <CR>")

    

        

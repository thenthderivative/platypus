'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import platypus
import pyvisa,threading
import pyvisa
import time
import numbers
from math import nan, isclose

def convert(v):
    if isinstance(v, numbers.Number): return v
    v = v.lower()
    v2 = v.replace("ghz", "")
    if v2 != v: return float(v2)*1000000000.0
    v2 = v.replace("mhz", "")
    if v2 != v: return float(v2)*1000000.0
    v2 = v.replace("khz", "")
    if v2 != v: return float(v2)*1000.0
    v2 = v.replace("hz", "")
    return float(v2)

class DTGBase(platypus.base.Channel):
    def __init__(self, name, instrument, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setname(name)
        self.instr = instrument
        self.v = 0
    def stop(self): pass
    
    def getvalue(self): return self.v
    def gettype(self): return 'c'
    
class DTGAmplitude(DTGBase):
    def __init__(self, *argv, **kwargs):
        super().__init__(*argv)
        self.chan = 1
        if 'channel' in kwargs:
            self.chan = int(kwargs['channel'])
            del kwargs['channel']
        for key in kwargs.keys(): raise Exception('Unknown keyword argument: {}'.format(key))
    def validate(self, v):
        return (v <= 3.5 and v >= 0.1)
    def read(self):
        self.v = self.instr.get_level(self.chan)
        return self.v
    def set(self, v):
        if isclose(self.v, v): return
        self.v = v
        self.instr.set_level(self.chan, v)

class DTGFrequency(DTGBase):
    def validate(self, v):
        return (v <= 3.35e9 and v >= 5.0e4)
    def read(self):
        self.v = self.instr.get_frequency()
        return self.v
    def set(self, v):
        if isclose(self.v, v): return
        self.v = convert(v)
        self.instr.set_frequency(v)

class DTGPeriod(DTGBase):
    def validate(self, v):
        return (v >= 1/3.35e9 and v <= 1/5.0e4)
    def read(self):
        self.v = self.instr.get_period()
        return self.v
    def set(self, v):
        if isclose(self.v, v): return
        self.v = v
        self.instr.set_period(v)

class DTG(object):
    def __init__(self, address, *args, **kwargs):
        # Create the instrument
        rm = pyvisa.ResourceManager()
        self.instr = rm.open_resource(address)
    def _execute(self, msg):
        self.instr.write(msg)
        if self.instr.stb & 4: # Error!
            raise Exception('Error in DTG5000: ', self.instr.query("SYSTEM:ERROR?"))
        if msg.strip()[-1] != '?': return None
        # Recover message
        while True:
            stb = self.instr.stb
            if stb & 4: # Error!
                raise Exception('Error in DTG5000: ', self.instr.query("SYSTEM:ERROR?"))
            if stb & 16: # Message available!
                return self.instr.read()
            time.sleep(0.001)
    def set_level(self, chan, level):
        self._execute("PGENA:CH{}:AMPL {}".format(chan, level))
    def get_level(self, chan):
        return float(self._execute("PGENA:CH{}:AMPL?".format(chan)))
    def set_frequency(self, frequency):
        self._execute("TBAS:FREQ {}".format(frequency))
    def get_frequency(self):
        return float(self._execute("TBAS:FREQ?"))
    def set_period(self, period):
        self._execute("TBAS:PER {}".format(period))
    def get_period(self):
        return float(self._execute("TBAS:PER?"))
    def run(self, state=True):
        self._execute("TBAS:RUN {}".format("ON" if state else "OFF"))
    def stop(self): self.run(False)
    def on(self, chan=1, state=True):
        self._execute("PGENA:CH{}:OUTPut {}".format(chan, "ON" if state else "OFF"))
    def off(self, chan=1): self.on(chan, False)
    
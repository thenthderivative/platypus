# -*- coding: utf-8 -*-
"""
Created on April 13 2016

@author: Louis Gaudreau
"""

import platypus
import pyvisa
import pyvisa
from devices import Keithley6517B

#Give a name to the device in the next line
class Laser(platypus.base.Channel):
    
    '''
    Documentation
    Virtual device that uses the keithley6517B to control a flip mount
    to turn laser on or off. (0V=off, 2V=on)
    
    Required Inputs: (name)
    '''    
    
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Use the provided channel name for this instrument
        self.setname(name)


        #Create a Keithley6517B channel
        #(but don't add it to the state monitor)
        self.Keithley6517B = Keithley6517B('Keithley6517B', address)
        
        #Initialize the state by reading it
        self.state = self.read()

    def validate(self):
        #No validation required
        return True
        
    def set(self, voltage):pass
        #No set required
        
        
    def stop(self): pass
        #No stop required
    
    
    def read(self):
        '''
        Documentation
        Gets the state of the virtual laser (0=off, 1=on), by getting
        the voltage from the Keithley 6517B
        Input:None
        Return:
        laser state (0=off, 1=on)
        '''
        #Read the voltage on the keithley source and map it to a laser state
        voltage = self.Keithley6517B.read()
        if (voltage==0): self.state = 0
        if (voltage==2): self.state = 1

        return self.state

    def on(self):
        '''
        Documentation
        Switches the laser on by applying 2V from the keithley source.
        Input: None
        Return: None
        '''
        #Set the keithley source to 2 V
        self.Keithley6517B.set(2)
        #Change the state of the laser
        self.state = 1
        #Update platypus so that it shows on the state monitor
        platypus.update()

    def off(self):
        '''
        Documentation
        Switches the laser off by applying 0V from the keithley source.
        Input: None
        Return: None
        '''
        #Set the keithley source to 0 V
        self.Keithley6517B.set(0)
        #Change the state of the laser
        self.state = 0
        #Update platypus so that it shows on the state monitor
        platypus.update()    
        
        
    def getvalue(self):
        #Returns the last laser state value stored in memory.
        return self.state
        
        
    def gettype(self):
        #If this is a control channel, 'c' should be returned. If it is a
        #measurement channel, 'm' should be returned.
        return 'c'

# -*- coding: utf-8 -*-
"""
Created on Thursday Jan 15 16:34:02 2016

@author: Louis Gaudreau
"""

import platypus
import pyvisa
import numpy as np
import time


class AgilentE5071C(platypus.base.Channel):
    def __init__(self, name, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        # Create the instrument
        rm = pypyvisa.ResourceManager()
        self.inst = rm.open_resource(address)

        self.setname(name)

        self.power = self.read()

    def validate(self): return True

    def stop(self): pass

    def set_s_param(self, trace_number, s_param):
        '''
        Documentation:
        Sets the S parameter of the specified trace.
        Input:
        trace_number (int)
        s_param (string: 'S11', 'S12', 'S21', or 'S22')
        Return:
        None
        '''
        self.inst.write(':CALC1:PAR'+str(trace_number)+':DEF '+s_param)

    def set_format(self, trace_number, format):
        '''
        Documentation:
        Sets the format of the specified trace.
        Input:
        trace_number (int)
        format (string: 'MLOG', 'MLIN','PHAS')
        Return:
        None
        '''
        #Select trace number 
        self.inst.write(':CALC1:PAR'+str(trace_number)+':SEL')
        #set the format
        self.inst.write(':CALC1:SEL:FORM '+format)
        
    def get_spectrum(self, trace_number, format = 'MLOG'):
        '''
        Documentation:
        Triggers the VNA to get the spectrum from trace
        number 'trace_number'. Waits for the spectrum to be
        completed before returning it. Note: only y values will
        be returned, for x values use get_freq_array.
        Input:
        trace_number (int)
        Return:
        spectrum (float array)
        '''
        #Set the encoding to ASCII
        self.inst.write(':FORM:DATA ASCii')
        #Set the trace number to a string
        trace_number = str(trace_number)

        #Trigger the measurement...
        self.inst.write(':INITiate1:CONTinuous 1')
        self.inst.write(':TRIGger:SEQuence:SOURce BUS')
        self.inst.write(':TRIGger:SINGle')
        
##        #and wait for the frequency sweep to finish
        self.inst.write('*OPC') #opc required for esr to return a one when pending operation completes otherwise always returns 0
##        print('piou')
        time.sleep(1) #wait 1 sec for the OPC command
        is_sweep_done = 0
        while (is_sweep_done != 1):
            is_sweep_done = float(self.inst.query('*ESR?'))
##        print('vbmn')
##        #Select trace number 
##        self.inst.write(':CALC1:PAR'+trace_number+':SEL')
        
        self.set_format(trace_number, format)
        #Acquire trace
        spectrum = self.inst.query(':CALC1:SEL:DATA:FDAT?')
        #The VNA sends a loooong ASCII string for all datapoints in two values (2nd one always = 0)
        #so we have to split it by ',' and skip every other element and transform it into float
        spectrum = [float(x) for i,x in enumerate(spectrum.split(',')) if not i%2]
        #Transform the python list into an array
        spectrum = np.asarray(spectrum)

        self.inst.write(':TRIGger:SEQuence:SOURce INTernal')
        
        return spectrum
		
    def get_freq_array(self):
        '''
        Documentation:
        Gets the frequency array currently chosen in the VNA
        Input:
        None
        Return:
        freq_array (float array)
        '''
        #Set the encoding to ASCII
        self.inst.write(':FORM:DATA ASCii')
        #Acquire the frequency array (x-axis)
        frequency_array = self.inst.query(':SENS1:FREQ:DATA?')
        #The VNA sends a loooong ASCII string for all data
        #so we have to split it by ',' and skip every other element and transform it into float
        frequency_array = [float(x) for i,x in enumerate(frequency_array.split(','))]
        #Transform the python list into an array
        frequency_array = np.asarray(frequency_array)
        return frequency_array

    def set_frequency_range(self,start,stop):
        '''
        Documentation:
        Sets the frequency range.
        Input:
            start frequency (float)
            stop frequency (float)
        Return:
            None
        '''
        from decimal import Decimal
        #Convert floats to proper string format
        start = '%.11E' % Decimal(str(start))
        stop = '%.11E' % Decimal(str(stop))
        #Write to instrument
        self.inst.write(':SENS1:FREQ:STAR '+str(start))
        self.inst.write(':SENS1:FREQ:STOP '+str(stop))

    def get_frequency_range(self):
        '''
        Documentation:
        Gets the frequency range.
        Input:
            None
        Return:
            start frequency (float)
            stop frequency (float)
        '''
        start = self.inst.query(':SENS1:FREQ:STAR?')
        stop = self.inst.query(':SENS1:FREQ:STOP?')
        start = float(start[:-1])
        stop = float(stop[:-1])
        return start,stop

    def set_power(self,amplitude):
        '''
        Documentation:
        Sets the power of the specified trace.
        Input:
            power amplitude in dBm (float)
        Return:
            None
        '''
        #Set power of specified trace
        self.inst.write(':SOUR1:POW '+str(amplitude))
        self.power = amplitude
        platypus.update(self)

    def get_power(self):
        '''
        Documentation:
        Sets the power of the specified trace.
        Input:
            None
        Return:
            power amplitude in dBm (float)
        '''
        #Set power of specified trace
        val = self.inst.query(':SOUR1:POW?')
        self.power = float(val.replace('\n',''))
        platypus.update(self)
        return float(val.replace('\n',''))

    def set(self,val):
        self.set_power(val)

    def read(self):
        return self.get_power()

    def getvalue(self):
        #Returns the last read value stored in memory.
        return self.power
        
    def gettype(self):
        #Control channel, so 'c' is returned.
        return 'c'

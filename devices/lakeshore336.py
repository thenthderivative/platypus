'''

Copyright (c) 2023, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

#!/usr/bin/env python
import pyvisa
import time

import platypus
class LakeShore336(object):
    def __init__(self, address):
        self.instr = pyvisa.ResourceManager().open_resource(address, baud_rate =  57600, data_bits=7, stop_bits=pyvisa.constants.StopBits.one, parity=pyvisa.constants.Parity.odd, write_termination="\n", read_termination="\r\n")
    def query_temperature(self, channel='A'):
        msg = "KRDG? " + channel
        return self.instr.query(msg)

class LakeShore336Temperature(platypus.base.Channel):
    def __init__(self, name, instr, ch='A', *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Name the channel
        self.setname(name)

        # Connect to the device
        self.controller = instr
        self.ch = ch
        self.read()

    def validate(self, value): return True

    def set(self, v): pass

    # Read the value of the channel (may involve a hardware read).
    def read(self):
        self.v = float(self.controller.query_temperature(self.ch))
        return self.v
    
    def getvalue(self):
      return self.v
    def stop(self): pass
    def gettype(self): return 'm'

'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

import sys
import math
import time
import pyvisa
import pyvisa
import threading
import warnings

import platypus

def query_yes_No(question):
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    while True:
        sys.stdout.write(question + " [y/N] ")
        choice = input().lower()
        if choice == '':
            return False
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

# Delft IVVI rack device.
class DelftIVVI(object):
    NEG = -1
    BIP = 0
    POS = 1
    poldict = {NEG:-4000,BIP:-2000,POS:0}
    def __init__(self, address="COM1", maxstep = 0.002):
        self.channels = [DelftIVVIChannel(i, self) for i in range(16)]
        for i, ch in enumerate(self.channels): ch.setname("Delft {}".format(i))
        self.polarities = [self.BIP]*4 # Default to bipolar -2.0V <-> 2.0V

        # Initialize VISA and connect to the device
        self.rm = pyvisa.ResourceManager()
        self.lib = self.rm.visalib
        self.session = self.rm.session
        self.device = self.lib.open(self.session, address)[0]
        self.maxstep = maxstep
        self.lib.issue_warning_on = []
        self.lib.set_attribute(self.device, pyvisa.constants.VI_ATTR_ASRL_BAUD, 115200)
        self.lib.set_attribute(self.device, pyvisa.constants.VI_ATTR_ASRL_DATA_BITS, 8)
        self.lib.set_attribute(self.device, pyvisa.constants.VI_ATTR_ASRL_STOP_BITS, pyvisa.constants.VI_ASRL_STOP_ONE)
        self.lib.set_attribute(self.device, pyvisa.constants.VI_ATTR_ASRL_PARITY, pyvisa.constants.VI_ASRL_PAR_ODD)
        self.lib.set_attribute(self.device, pyvisa.constants.VI_ATTR_ASRL_END_IN, pyvisa.constants.VI_ASRL_END_NONE)

        self.rate = 0.1 # Volts per second.

        self.read()

    ## Private Methods

    def query(self, message):
        self.lib.write(self.device, message)
        data1 = self.lib.read(self.device, 2)[0]
        data2 = self.lib.read(self.device, data1[0]-2)[0]
        return [s for s in data1 + data2]

    ## Private friends of other classes in this file.

    # Convert a Vb to 2 bytes.
    def voltage_to_bytes(self, Vb):
        bytevalue = int(round(Vb/4000.0*65535))
        dataH = int(bytevalue/256)
        dataL = bytevalue - dataH*256
        return (dataH, dataL)
    # Convert a list of 2*16 bytes to a list of 16 Vbs, one for each channel.
    def bytes_to_voltages(self, bytes):
        values = [0.0]*16
        for i in range(16):
            values[i] = ((bytes[2 + 2*i]*256 + bytes[3 + 2*i])/65535.0*4000.0)
        return values

    # Immediately sets the specified dac to the specified voltage (in 0-4000mV full scale)
    def set_voltage(self, cnum, Vb):
        (DataH, DataL) = self.voltage_to_bytes(Vb)
        message = bytes([7, 0, 2, 1, cnum+1, DataH, DataL])
        reply = self.query(message)
        self.channels[cnum].v = Vb
        return reply

    ## Public

    def setrate(self, rate = 0.1):
        self.rate = rate

    def getrate(self):
        return self.rate

    def getmaxstep(self):
        return self.maxstep

    def setmaxstep(self, v):
        assert (float(v) > 0)
        self.maxstep = float(v)

    # Set the polarity of the given channel (and its siblings).
    def setpolarity(self, i, pol):
        assert(pol in self.poldict)
        assert(i in range(16))
        i = i//4
        print("This will set the ranges of channels")
        print("{} through {} to {} - {} instantly.".format(i*4, i*4+3, 0+self.poldict[pol], 4000+self.poldict[pol]))
        print("Make sure they are disconnected.")
        if query_yes_No("Change polarity?"):
            old = self.polarities[i]
            self.polarities[i] = pol
            try:
                for ch in self.getch():
                    if not ch.validate(ch.getvalue()):
                        raise ValueError
            except:
                print("Polarity change unsuccessful: at least"
                      " one channel's current voltage is invalid"
                      " in that range.")
                self.polarities[i] = old
            platypus.read(self.getch())

    # Reads from device and returns all dac voltages in a list (in 0-4000mV full scale)
    def read(self):
        message = bytes([4, 0, 16*2+2, 2])
        reply = self.query(message)
        Vs = self.bytes_to_voltages(reply)
        for i, v in enumerate(Vs):
            self.channels[i].v = v

    # Return a COPY of the channel list.
    def getch(self): return self.channels[:]

class DelftIVVIChannel(platypus.base.Channel):
    def __init__(self, cnum, instrument, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Private attributes
        self.cnum = cnum
        self.instrument = instrument

        # Limit Attributes
        self.low = 0.0
        self.high = 0.0

    ## Private methods

    # Get an integer whose sign indicates the polarity (DelftIVVI.[NEG/BIP/POS]).
    def getpol(self):
        with self.lock:
            return self.instrument.polarities[self.cnum//4]
    # Get the offset in mV for that polarity.
    def getoffset(self):
        with self.lock:
            return DelftIVVI.poldict[self.instrument.polarities[self.cnum//4]]
    # Translate from the floating-point to range-adjusted voltage in mV.
    def translate_Vf_to_Vb(self, Vf):
        return Vf*1000.0 - self.getoffset()
    # Translate from range-adjusted mV to floating-point voltage.
    def translate_Vb_to_Vf(self, Vb):
        return (Vb + self.getoffset())/1000.0

    ## Public methods

    # Make sure this voltage is within the allowed range.
    def validate(self, Vf):
        Vb = self.translate_Vf_to_Vb(Vf)
        return (Vb <= 4000 and Vb >= 0) and (Vf >= self.low and Vf <= self.high)
    # Make sure the limits make sense given the current polarity setting.
    def relimit(self):
      with self.lock:
        if not self.validate(self.low):
            self.low = 0 + self.getoffset()
        if not self.validate(self.high):
            self.high = 4.0 + self.getoffset()

    # Return limits.
    def getlimits(self):
        return (self.low, self.high)

    # Set the voltage limits to be 'low' and 'high' (which are voltages).
    def setlimits(self, low, high):
      with self.lock:
        # Swap them if incorrectly specified.
        if low > high:
            t = low
            low = high
            high = t
        Olow = self.low
        Ohigh = self.high
        self.low = low
        self.high = high
        if not self.validate(low):
            warnings.warn("Low value out of range. Limits unchanged.")
            self.low = Olow
            self.high = Ohigh
            return
        if not self.validate(high):
            print(self.cnum)
            warnings.warn("High value out of range. Limits unchanged.")
            self.low = Olow
            self.high = Ohigh
            return

    # Step until you're there!
    def set(self, V, delay=None):
        with self.lock:
            self.stopped = False
            if not self.validate(self.getvalue()): raise ValueError
            if not self.validate(V): raise ValueError
        def steptoward(V):
            tbegin = time.monotonic()
            v_i = self.getvalue()
            v_f = V
            d = v_f - v_i

            sign = lambda x: math.copysign(1, x)

            if abs(d) >= self.instrument.maxstep:
                v_f = v_i + self.instrument.maxstep * sign(d)

            self.v = self.translate_Vf_to_Vb(v_f)
            self.instrument.set_voltage(self.cnum, self.v)
            if self in platypus.getch(): platypus.update(self)
            while (time.monotonic() - tbegin < self.instrument.maxstep / self.instrument.rate): time.sleep(0.001)
            return abs(d) > self.instrument.maxstep
        while (True):
            if not steptoward(V): break
            if self.stopped: break

    # No hardware reads (those are done at the device level, if at all).
    def getvalue(self):
        return round(self.translate_Vb_to_Vf(self.v), 5)
    def read(self): return self.getvalue()

    def gettype(self): return 'c'

    def stop(self):
        with self.lock: self.stopped = True

'''

Copyright (c) 2019, Aviv Padawer-Blatt
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@Author: Aviv Padawer-Blatt
'''

import platypus
import pyvisa
import pyvisa
import time
from math import isclose, inf


class KeysightBase(platypus.base.Channel):
    def __init__(self, parent, name, limits=(-inf, inf)):
        self.setname(name)
        self.parent = parent
        self.limits = limits
        self.v = 0.0

    def validate(self, v):
        low, high = min(self.limits), max(self.limits)
        return (v >= low and v <= high)
    
    def stop(self): pass
    def getvalue(self): return self.v
    def gettype(self): return 'c'


class KeysightFreq(KeysightBase):
    def validate(self, v):
        return (v >= 0.0001 and v <= 20)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_frequency(v)
        else:
            raise ValueError("Invalid Frequency")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_frequency()
        return self.getvalue()
    '''
    def sweep(self, stepsize, rate, start, stop):
        #self.set(start)
        self.parent.setsweepdirection("UP")
        self.parent.setfreqsweepstart(start)
        self.parent.setfreqsweepstop(stop)
        self.parent.setstepsize(stepsize)
        self.parent.setsweeprate(rate)
        self.parent.startfreqsweep()
        #while 
        #self.parent.stopfreqsweep()
    '''
        


class KeysightPower(KeysightBase):
    def validate(self, v):
        return (v >= -130 and v <= 0) and super().validate(v)
    def set(self, v):
        if isclose(v, self.v): return
        self.v = v
        if (self.validate(v)):
            self.parent.set_power(v)
        else:
            raise ValueError("Invalid Power")
        platypus.update(self)
    def read(self):
        self.v = self.parent.get_power()
        return self.getvalue()



class KeysightMW(object):
    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)

        #Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address,
                                     write_termination="\n",
                                     read_termination="\n")


    def write(self, msg):
        self.inst.write(msg)
        return None

    def query(self, msg):
        self.inst.write(msg)
        return self.inst.read()
    

    def set_frequency(self, freq):
        '''Change the frequency'''
        msg = ":FREQ {}GHZ".format(freq)
        self.write(msg)

    def get_frequency(self):
        '''Read the frequency'''
        msg = ":FREQ?"
        return float(self.query(msg))

    def set_power(self, power):
        '''Change the power level'''
        msg = ":POW:AMPL {}".format(power)
        self.write(msg)

    def get_power(self):
        '''Read the power level'''
        msg = ":POW:AMPL?"
        return float(self.query(msg))


    '''
    def setfreqsweepstart(self, freq):
        msg = "SOUR:FREQ:STAR {}GHZ".format(freq)
        self.write(msg)

    def getfreqsweepstart(self):
        msg = "SOUR:FREQ:STAR?"
        return float(self.query(msg)) * 1e-9

    def setfreqsweepstop(self, freq):
        msg = "SOUR:FREQ:STOP {}GHZ".format(freq)
        self.write(msg)

    def getfreqsweepstop(self):
        msg = "SOUR:FREQ:STOP?"
        return float(self.query(msg)) * 1e-9

    def setsweepdirection(self, direction):
        msg = ":SOUR:LIST:DIR {}".format(direction)
        self.write(msg)

    def getsweepdirection(self):
        msg = "SOUR:LIST:DIR?"
        return self.query(msg)

    def setnumsweeppoints(self, points):
        msg = ":SOUR:SWE:POIN {}".format(points+1)
        self.write(msg)

    def getnumsweeppoints(self):
        msg = ":SOUR:SWE:POIN?"
        return float(self.query(msg))

    def setstepsize(self, stepsize):
        start = self.getfreqsweepstart()
        stop = self.getfreqsweepstop()
        points = abs(stop - start)/stepsize
        self.setnumsweeppoints(points)

    def getstepsize(self):
        start = self.getfreqsweepstart()
        stop = self.getfreqsweepstop()
        points = self.getnumsweeppoints()
        stepsize = abs(stop - start)/points
        return stepsize

    def setdwelltime(self, dwell):
        msg = ":SOUR:SWE:DWEL {}S".format(dwell)
        self.write(msg)

    def getdwelltime(self):
        msg = ":SOUR:SWE:DWEL?"
        return float(self.query(msg))

    def setsweeptime(self, time):
        points = self.getnumsweeppoints()
        dwell = time/points
        self.setdwelltime(dwell)

    def getsweeptime(self):
        points = self.getnumsweeppoints()
        dwell = self.getdwelltime()
        time = points * dwell
        return time

    def setsweeprate(self, rate):
        start = self.getfreqsweepstart()
        stop = self.getfreqsweepstop()
        time = abs(stop - start)/rate
        self.setsweeptime(time)

    def getsweeprate(self, stepsize):
        start = self.getfreqsweepstart()
        stop = self.getfreqsweepstop()
        time = self.getsweeptime(self)
        rate = abs(stop - start)/time
        return rate

    def startfreqsweep(self, status=True):
        msg = ":SOUR:LIST:RETR OFF"
        self.write(msg)
        msg = "SOUR:FREQ:M ODE SWE" if status else "SOUR:FREQ:MODE CW"
        self.write(msg)

    def stopfreqsweep(self):
        self.startfreqsweep(False)
    '''
    

    def on(self, status=True):
        '''Turn on MW output or, if the argument is false, turn it off'''
        msg = ":OUTP ON" if status else "OUTP OFF"
        self.write(msg)

    def off(self):
        '''Turn off MW output'''
        self.on(False)


    def mod_on(self, status=True):
        '''Turn on MW modulation or, if the argument is false, turn it off'''
        self.IQ_on()
        msg = ":OUTP:MOD:STAT ON" if status else "OUTP:MOD:STAT OFF"
        self.write(msg)
        self.on()
        
    def mod_off(self):
        '''Turn off MW modulation'''
        self.IQ_off()
        self.mod_on(False)
                

    def IQ_on(self, status=True):
        '''Enable IQ modulator'''
        msg = "SOUR:DM:STAT ON" if status else "SOUR:DM:STAT OFF"
        self.write(msg)

    def IQ_off(self):
        '''Disable IQ modulator'''
        self.IQ_on(False)


    def wideband_on(self, status=True):
        '''Enable wideband IQ modulator'''
        msg = "SOUR:WDM:STAT ON" if status else "SOUR:WDM:STAT OFF"
        self.write(msg)

    def wideband_off(self):
        '''Disable wideband IQ modulator'''
        self.wideband_on(False)
        self.IQ_on()


    def F(self, name="Keysight Frequency (GHz)", limits=(-inf, inf)):
        '''
        Return a frequency control channel with 'name'
        specified as positional argument.
        Units are in GHz.
        '''
        return KeysightFreq(self, name, limits)

    def L(self, name="Keysight Power (dBm)", limits=(-inf, inf)):
        '''
        Return a power control channel with 'name'
        specified as positional argument.
        Setting is in dBm.
        '''
        return KeysightPower(self, name, limits)

'''

Copyright (c) 2015, Alexander Bogan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''

#!/usr/bin/env python
import pyvisa
import pyvisa
import time
import os
import math
import struct
import gc

import platypus

import numbers
import numpy as np

class AWGWaveformFile(object):
    def __init__(self, awg, fname, ch=1):
        '''
            'fname'
                The name of the target waveform file on the AWG hard drive.
            'builder'
                PulseBuilder object that produces the arbitrary waveform.
        '''
        self.awg = awg
        self.fname = fname
        self.ch = ch
    def set(self, data=None, CLOCK=None):
        if not isinstance(CLOCK, numbers.Number): CLOCK = CLOCK.read()

        # Convert to binary, with (trivial) marker data.
        data = b''.join([struct.pack("fb", datum, i < data.shape[0]/2 and i!=0) for i, datum in enumerate(data)])

        # Generate the header and tail.
        head = "MAGIC 1000\r\n".encode()
        N = len(data)
        count = "#{}{}".format(len(str(N)), N).encode()
        tail = "CLOCK {}\t\n".format(CLOCK).encode()
        # Upload to AWG.
        if (int(self.awg._execute(":AWGControl:RSTate?")) == 0) and (int(self.awg._execute(":OUTPut?")) == 0):
            print("Warning: AWG waveform updated, but awg not running or CH1 disabled.")
        self.awg.load_file_from_memory(self.fname+".wfm", head+count+data+tail)
        self.awg.select_file(self.fname, ch=self.ch)

class AWGSequencerChannel(platypus.base.Channel):
    def __init__(self, parent, name, filename):
        self.parent = parent
        self.count = 1
        self.setname(name)
        self.filename = filename
    def set(self, count):
        self.count = round(count)
        self.parent.update()
        platypus.update(self)
    def read(self): return self.count
    def getvalue(self): return self.count
    def validate(self, v): return v >= 1
    def gettype(self): return 'c'
    def __str__(self):
        def gettype(self): return 'c'
        return '"{}", "", {}, 0, 0, 0, 0'.format(self.filename, self.count)

class AWGSequenceFile(object):
    def __init__(self, awg, fname, waveforms):
        '''
            <waveforms>::=[(name, filename)]
        '''
        self.awg = awg
        self.fname = fname
        self.lines = [AWGSequencerChannel(self, name, filename) for name, filename in waveforms]
    def set(self):
        head = "MAGIC 3002A\r\nLINES {}\r\n".format(len(self.lines))
        head = head.encode()
        body = b''.join([(str(line) + "\r\n").encode() for line in self.lines])
        self.awg.load_file_from_memory(self.fname, head+body)

def convert(v):
    if isinstance(v, numbers.Number): return v
    v = v.lower()
    v2 = v.replace("ghz", "")
    if v2 != v: return float(v2)*1000000000
    v2 = v.replace("mhz", "")
    if v2 != v: return float(v2)*1000000
    v2 = v.replace("khz", "")
    if v2 != v: return float(v2)*1000
    v2 = v.replace("hz", "")
    return float(v2)

class AWGBase(platypus.base.Channel):
    def __init__(self, name, instrument, channel=1, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setname(name)
        self.inst = instrument
        self.channel = channel
        self.v = 0
    def stop(self): pass
    
    def getvalue(self): return self.v
    def gettype(self): return 'c'

class AWGVoltage(AWGBase):
    def validate(self, v):
        return (v <= 2 and v >= 0.00)
    def read(self):
        self.v = float(self.inst.get_voltage(self.channel))
        return self.v
    def set(self, v):
        self.v = v
        self.inst.set_voltage(v,self.channel)

class AWGFrequency(AWGBase):
    def validate(self, v):
        v = convert(v)
        if (self.inst.get_function()[0]):
            return (v >= convert("1Hz") and v <= convert("400MHz"))
        else:
            return (v >= convert("50kHz") and v <= convert("4000MHz"))
    def read(self):
        self.v = float(self.inst.get_frequency(self.channel))
        return self.v
    def set(self, v):
        self.v = convert(v)
        self.inst.set_frequency(v,self.channel)

class AWGDutyCycle(AWGBase):
    def validate(self, v):
        return (v >= 0.1 and v <= 99.9)
    def read(self):
        self.v = float(self.inst.get_duty())
        return self.v
    def set(self, v):
        self.v = v
        self.inst.set_duty(v)

class AWGFileNumber(AWGBase):
    def validate(self, v): return True
    def read(self):
        self.v = self.inst.get_current_filename()
        return self.v
    def set(self, v):
        self.inst.select_file(v)
        self.v = v

class AWGBasic(object):
    # Base class for both (or more) Tektronix AWG instruments.

    # Universal methods
    def __init__(self, address):
        # Connect to the device
        rm = pyvisa.ResourceManager()
        self.instr = rm.open_resource(address, timeout=20000)
        if type(self.instr) is pyvisa.resources.gpib.GPIBInstrument:
            self.instr.write_termination = None
            self.instr.read_termination  = None
        if type(self.instr) is pyvisa.resources.TCPIPInstrument:
            self.instr.write_termination = '\n'
            self.instr.read_termination  = '\n'
    def _execute(self, msg):
        self.instr.write(msg)
        ret = self.instr.read() if msg.strip()[-1] == '?' else None
        self.instr.query("*OPC?")
        self.instr.write(":SYST:ERROR?")
        error = self.instr.read()
        if error[0] != '0':
            raise Exception(error)
        return ret
    def _write_raw(self, msg):
        self.instr.write_raw(msg)
        self.instr.write(":SYST:ERROR?")
        error = self.instr.read()
        if error[0] != '0':
            raise Exception(error)
    def on(self, state=True):
        self._execute(":AWGCONTROL:RUN" if state else ":AWGCONTROL:STOP")
        if state and (int(self._execute(":OUTPut?")) == 0):
            print("Warning: AWG running, CH1 disabled.")
    def off(self): self.on(False)

    # Function generator group
    def set_frequency(self, freq): raise NotImplementedError
    def get_frequency(self): raise NotImplementedError
    def set_voltage(self, amp): raise NotImplementedError
    def get_voltage(self): raise NotImplementedError
    def set_function(self, function): raise NotImplementedError
    def get_function(self): raise NotImplementedError
    def set_duty(self, duty): raise NotImplementedError
    def get_duty(self): raise NotImplementedError

    def select_file(self, file_name, extension = "WFM"): raise NotImplementedError

    def get_current_filename(self): raise NotImplementedError

    # Universal file manipulation commands.

    def load_files(self, dir_or_files):
        '''
            Loads a set of files from the disk to the AWG, placing them in the root of the mass storage device.
            File names are preserved. Path information is discarded.
            Input 'dir_or_files': Either a sequence of file paths to be loaded into mass storage on the AWG OR a directory whose direct children (files only) should be loaded.
            Output: None
        '''
        if isinstance(dir_or_files, str) and os.path.isdir(dir_or_files):
            dir_or_files = [os.path.join(dir_or_files, i) for i in next(os.walk(dir_or_files))[2]]
        for file in dir_or_files:
            _, fname = os.path.split(file)
            with open(file, 'rb') as f:
                data = f.read()
                self.load_file_from_memory(fname, data)
    def load_file_from_memory(self, fname, data):
        '''
            Loads a set of files from the disk to the AWG, placing them in the root of the mass storage device.
            File names are preserved. Path information   discarded.
            Input 'fname': String name of the destination file.
            Input 'data': Bytes the file is to contain.
            Output: None
        '''
        N = str(len(data))
        head = "#" + str(len(N)) + N
        self._write_raw(':MMEM:DELETE "{}"'.format(fname).encode())
        self._write_raw(':MMEM:DATA "{}",{}'.format(fname,head).encode()+data)
        self._execute("*OPC")

        # Launch gc in main thread because this is thorough and may release objects allocated in other threads (like TKinter objects) and the destructors of those objects are not thread safe.
        platypus.execinmain(lambda:gc.collect())
    def delete_files(self, files = None):
        '''
            Deletes a given list of files OR deletes them all if none are specified.
        '''
        if not files: # Delete everything. EVERYTHING!
            files = self.list_files()
            if not files: return
        for file in files:
            self._execute(':MMEM:DEL "{}"'.format(file))
    def list_files(self):
        '''
            Return a list of file-names stored in mass storage on the device.
        '''
        files = self._execute(":MMEM:CAT?").split(',')[2:] # Discard size information
        # Condition the list by taking the first token of every string of the form: "<fname>, [<dir?>,] <size>"
        files = [y for x,y in zip(files, [i.lstrip("'\"") for i in files]) if x != y]
        return files

class AWG710B(AWGBasic):
    SIN = "SIN"
    TRI = "TRI"
    SQUARE = "SQU"
    RAMP = "RAMP"
    PULSE = "PULS"
    DC = "DC"

    def set_frequency(self, freq):
        freq = convert(freq)
        if (self.get_function()[0]):
            assert(freq >= convert("1Hz") and freq <= convert("400MHz"))
            self._execute(":AWGCONTROL:FG:FREQ {}".format(freq))
        else:
            assert(freq >= convert("50kHz") and freq <= convert("4GHz"))
            self._execute("FREQ {}".format(freq))
    def get_frequency(self):
        if (self.get_function()[0]):
            return float(self._execute(":AWGCONTROL:FG:FREQ?"))
        else:
            return float(self._execute("FREQ?"))
    def set_voltage(self, amp):
        assert(amp <= 2 and amp >= 0.02)
        if (self.get_function()[0]):
            self._execute(":AWGCONTROL:FG:VOLT {}".format(amp))
        else:
            self._execute(":VOLT {}".format(amp))
    def get_voltage(self):
        if (self.get_function()[0]):
            return float(self._execute(":AWGCONTROL:FG:VOLT?"))
        else:
            return float(self._execute(":VOLT?"))
    def set_function(self, function=SIN):
        assert(function in [self.SIN, self.TRI, self.SQUARE, self.RAMP, self.PULSE, self.DC])
        self._execute(":AWGCONTROL:FG:STATE ON")
        self._execute(":AWGCONTROL:FG:FUNC {}".format(function))
    def get_function(self):
        return (self._execute(":AWGCONTROL:FG:STATE?").strip() == '1', self._execute(":AWGCONTROL:FG:FUNC?").strip())        
    def set_duty(self, duty):
        assert(duty <= 99.9 and duty >= 0.1)
        self._execute(":AWGCONTROL:FG:PULSE:DCYCLE {}".format(duty))
    def get_duty(self):
        return float(self._execute(":AWGCONTROL:FG:PULSE:DCYCLE?"))

    def select_file(self, file_name, extension = "WFM", ch=None):
        '''
            Input 'file_name': A number or string-encoded number which, when rounded to an integer, corresponds to a file (ex:<N>.WFM) or a string which is the literal file name.
            Input 'extension': The extension to use for the file if the file_name is just a number.
            Output: None
            Ex: awg.select_file(2) # Selects "2.WFM"
            Ex: awg.select_file(2.4) # Selects "2.WFM"
            Ex: awg.select_file(2, "PAT") # Selects "2.PAT"
            Ex: awg.select_file("TEST.WFM") # Selects "TEST.WFM"
        '''
        try:
            N = round(int(file_name))
            file_name = "{}.{}".format(N, extension)
        except ValueError: pass
        self._execute(':FUNC:USER "{}.{}"'.format(file_name, extension))
    def get_current_filename(self):
        '''Returns the current file's name (or a blank string).'''
        ret = self._execute(":FUNC:USER?")
        ret = ret.split(',')[0].strip('"')
        return os.path.split(ret)[1]

class AWG70002A(AWGBasic):
    SIN = "SIN"
    TRI = "TRI"
    SQUARE = "SQU"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._execute(r"MMEM:CDIR '\waves'")

    def set_frequency(self, freq, channel=1):
        freq = convert(freq)
        if (self.get_function()[0]):
            assert(freq >= convert("1Hz") and freq <= convert("50MHz"))
            self._execute("FGEN:CHANnel{}:FREQ {}".format(channel,freq))
        else:
            assert(freq >= convert("1.49kHz") and freq <= convert("25GHz"))
            self._execute("CLOCK:SRATE {}".format(freq))
    def get_frequency(self, channel=1):
        if (self.get_function()[0]):
            return float(self._execute("FGEN:CHANnel{}:FREQ?".format(channel)))
        else:
            return float(self._execute("CLOCK:SRATE?"))
    def set_voltage(self, amp, channel=1):
        assert(amp <= 0.5 and amp >= 0)
        if (self.get_function()[0]):
            self._execute("FGEN:CHANnel{}:AMPLitude {}".format(channel,amp))
        else:
            self._execute("Source{}:VOLT {}".format(channel, amp))
    def get_voltage(self, channel=1):
        if (self.get_function()[0]):
            return float(self._execute("FGEN:CHANnel{}:AMPL?".format(channel)))
        else:
            return float(self._execute("Source{}:VOLT?".format(channel)))
    def set_function(self, function=SIN):
        assert(function in [self.SIN, self.TRI, self.SQUARE])
        self._execute("INSTrument:MODE FGEN")
        self._execute("FGEN:TYPE {}".format(function))
    def get_function(self):
        return (self._execute("INSTrument:MODE?").strip() == 'FGEN', self._execute("FGEN:TYPE?").strip())
    def select_file(self, file_name, ch=1, extension = "WFM"):
        '''
            Input 'file_name': A number or string-encoded number which, when rounded to an integer, corresponds to a file (ex:<N>.WFM) or a string which is the literal file name.
            Input 'extension': The extension to use for the file if the file_name is just a number.
            Output: None
            Ex: awg.select_file(2) # Selects "2.WFM"
            Ex: awg.select_file(2.4) # Selects "2.WFM"
            Ex: awg.select_file(2, "PAT") # Selects "2.PAT"
            Ex: awg.select_file("TEST.WFM") # Selects "TEST.WFM"
        '''
        self._execute(r'MMEM:OPEN "C:\waves\{}.{}"'.format(file_name,extension))
        self._execute('SOURCE{}:WAV "{}"'.format(ch,file_name))
    def get_current_filename(self):
        '''Returns the current file's name (or a blank string).'''
        return self._execute("WAV?")

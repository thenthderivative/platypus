# -*- coding: utf-8 -*-
"""
Created on Tuesday March 15 2016

@author: Louis Gaudreau
"""

import platypus
import pyvisa
import numpy as np


class TDS6154C(object):
    '''
    Controls the TDS6154C oscilloscope.
    Inputs:
    address (str)
    '''
    
    def __init__(self, address,*args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)
        # Create the instrument
        rm = pyvisa.ResourceManager()
        self.inst = rm.open_resource(address)

    def validate(self):
        return True
        
    def acquire_sample_waveform(self, channel):
        '''
        Acquire a sample waveform (i.e. non averaged) from a given channel.
        Inputs:
        channel (int or str)
        Return:
        time in seconds (float array)
        waveform in Volts (float array)
        '''
        #Select channel
        self.select_channel(channel)
        #Remove headers from queries, makes parsing easier
        self.inst.write('HEADER OFF')
        #For a math channel, set the encoding to ASCII 
        if type(channel) == str and channel[0:4] == 'MATH':
            self.inst.write('DATA:WIDTH 1')
            self.inst.write('DATa:ENCdg ASCII')
        else:
            #Set the encoding to binary signed integer data-point representation
            #with the most significant byte transferred first.
            self.inst.write('DATA:WIDTH 1')
            self.inst.write('DATa:ENCdg RIB')
            #Get the Y multiplying factor
            y_multiplier = self.get_y_multiplier()
            #Get the Y offset
            y_offset = self.get_y_offset()
            #Get the zero of Y
            y_zero = self.get_y_zero()
        #Get the time increment per point
        time_step = self.get_time_step()
        #Set to 'Sample' mode acquisition
        self.inst.write('ACQuire:MODe SAMPLE')
        print('Acquiring mode: Sample')
        #Set the acuisition mode on
        self.inst.write('ACQuire:STATE ON')
        #Set the length of the waveform to 500000 (maximum possible) to get the whole waveform
        self.inst.write('DATa:STOP 500000')
        #Start the measurement (equivalent of pressing 'SINGLE' on the scope)
        #This indicates that the next acquisition the instrument makes will
        #be of the single-sequence type.
        self.inst.write('ACQuire:STOPAfter SEQUENCE')
        #Wait until the trace is completed
        busy = 1
        while (busy == 1):
            busy = int(self.inst.query('BUSY?'))
        #Acquire the waveform from the scope after trace is completed
        if type(channel) == str and channel[0:4] == 'MATH':
            #Acquire the waveform from the scope after trace is completed
            waveform = self.inst.query('CURVe?')
            #Divide string received into a list
            waveform = waveform.split(',')
            #Transform the list of strings into a list of floats
            waveform = [float(x) for x in waveform]
            #Transform the python list into an numpy array
            waveform = np.asarray(waveform)
        else:
            waveform = self.inst.query_binary_values('CURVe?', datatype='b', is_big_endian=True)
            #Transform the python list into an numpy array of strings
            waveform = np.asarray(waveform)
            #Transform the array of strings into an array of floats
            waveform = waveform.astype(np.float)
            #The data commes in 8 bit integer format (-128->127), the following line transforms it into
            #the actual voltage
            waveform = y_multiplier*(waveform - y_offset) + y_zero
        #Create the time array(horiz. axis)
        time_array = np.arange(0, time_step * len(waveform), time_step)
        return time_array,waveform



    def acquire_math_sample_waveform(self, channel):
        '''
        Acquire a sample waveform (i.e. non averaged) from a given channel.
        Inputs:
        math channel (int)
        Return:
        time in seconds (float array)
        waveform in Volts (float array)
        '''
        #Select channel
        self.select_channel('MATH'+str(channel))
        #Remove headers from queries, makes parsing easier
        self.inst.write('HEADER OFF')
        #Set the encoding to ASCII 
        self.inst.write('DATA:WIDTH 1')
        self.inst.write('DATa:ENCdg ASCII')
        #Get the time increment per point
        time_step = self.get_time_step()
        #Set to 'Sample' mode acquisition
        self.inst.write('ACQuire:MODe SAMPLE')
        print('Acquiring mode: Sample')
        #Set the acquisition mode on
        self.inst.write('ACQuire:STATE ON')
        #Set the length of the waveform to 500000 (maximum possible) to get the whole waveform
        self.inst.write('DATa:STOP 500000')
        #Start the measurement (equivalent of pressing 'SINGLE' on the scope)
        #This indicates that the next acquisition the instrument makes will
        #be of the single-sequence type.
        self.inst.write('ACQuire:STOPAfter SEQUENCE')
        #Wait until the trace is completed
        busy = 1
        while (busy == 1):
            busy = int(self.inst.query('BUSY?'))
        #Acquire the waveform from the scope after trace is completed
        waveform = self.inst.query('CURVe?')
        #Divide string received into a list
        waveform = waveform.split(',')
        #Transform the list of strings into a list of floats
        waveform = [float(x) for x in waveform]
        #Transform the python list into an numpy array
        waveform = np.asarray(waveform)
        #Create the time array(horiz. axis)
        time_array = np.arange(0, time_step * len(waveform), time_step)
        return time_array,waveform
    
    
    
    def acquire_averaged_waveform(self, channel, number_of_averages):
        '''
        Acquire an averaged waveform (i.e. non averaged) from a given channel.
        Inputs:
        channel (int)
        number_of_averages (int)
        Return:
        time in seconds (float array)
        waveform in Volts (float array)
        '''
        #Select channel
        self.select_channel(channel)
        #Remove headers from queries, makes parsing easier
        self.inst.write('HEADER OFF')
        #Set the encoding to binary signed integer data-point representation
        #with the most significant byte transferred first.
        self.inst.write('DATA:WIDTH 1')
        self.inst.write('DATa:ENCdg RIB')
        #Get the Y multiplying factor
        y_multiplier = self.get_y_multiplier()
        #Get the Y offset
        y_offset = self.get_y_offset()
        #Get the zero of Y
        y_zero = self.get_y_zero()
        #Get the time increment per point
        time_step = self.get_time_step()
        #Set to 'Average' mode acquisition
        self.inst.write('ACQuire:MODe AVErage')
        #Set the number of traces to average
        self.inst.write('ACQuire:NUMAVg '+str(number_of_averages))
        print('Acquiring mode: Average '+str(number_of_averages)+' traces')
        #Set the acuisition mode on
        self.inst.write('ACQuire:STATE ON')
        #Set the length of the waveform to 500000 (maximum possible)to get the whole waveform
        self.inst.write('DATa:STOP 500000')
        #Start the measurement (equivalent of pressing 'SINGLE' on the scope)
        #This indicates that the next acquisition the instrument makes will
        #be of the single-sequence type.
        self.inst.write('ACQuire:STOPAfter SEQUENCE')
        #Wait until the trace is completed
        busy = 1
        while (busy == 1):
            busy = int(self.inst.query('BUSY?'))
        #Acquire the waveform from the scope after trace is completed
        waveform = self.inst.query_binary_values('CURVe?', datatype='b', is_big_endian=True)
        #Transform the python list into an numpy array of strings
        waveform = np.asarray(waveform)
        #Transform the array of strings into an array of floats
        waveform = waveform.astype(np.float)
        #The data commes in 8 bit integer format (-128->127), the following line transforms it into
        #the actual voltage
        waveform = y_multiplier*(waveform - y_offset) + y_zero
        #Create the time array(horiz. axis)
        time_array = np.arange(0, time_step * len(waveform), time_step)
        return time_array,waveform

    def select_channel(self, channel):
        '''
        Selects a channel on the scope. To use a regular channel, input the
        integer identifying the channel. To use a Reference or Math channel,
        input, for example, 'REF1' or 'MATH2'.
        
        Input:
        channel (int or str)
        Return:
        None
        '''
        if type(channel) == str and channel[0:4] == 'MATH' or type(channel) == str and channel[0:3] == 'REF':
            self.inst.write('DATa:SOUrce '+channel)
        else:
            self.inst.write('DATa:SOUrce CH'+str(channel))
            self.inst.write('MEASUrement:IMMed:SOUrce1 CH'+str(channel))
            print('Channel '+str(channel)+' selected.')
            self.channel = channel
        

    def get_y_multiplier(self):
        '''
        Returns the vertical scale factor per digitizing
        level for the waveform.
        Input:
        None
        Return:
        y_multiplier (float)
        '''
        y_multiplier = float(self.inst.query('WFMPRE:YMULT?'))
        return y_multiplier

    def get_y_offset(self):
        '''
        Returns the vertical offset in digitizing levels
        for the waveform
        Input:
        None
        Return:
        y_offset (float)
        '''
        y_offset = float(self.inst.query('WFMO:YOFF?'))
        return y_offset

    def get_y_zero(self):
        '''
        Returns the vertical offset for the waveform.
        Input:
        None
        Return:
        y_zero (float)
        '''
        y_zero = float(self.inst.ask('WFMPRE:YZERO?'))
        return y_zero

    def get_time_step(self):
        '''
        Returns the horizontal sampling interval.
        Input:
        None
        Return:
        time_step (float)
        '''
        time_step = float(self.inst.query('WFMPRE:XINCR?'))
        return time_step

    def get_RMS(self):
        '''
        Returns the root mean square value of the selected channel.
        Inputs:
        None
        Return:
        RMS (float)
        '''
        #Set the immediate acquisition to RMS
        self.inst.write('MEASUREMENT:IMMED:TYPE RMS')
        #Get the RMS value
        RMS = float(self.inst.query('MEASUREMENT:IMMED:VALUE?'))
        return RMS


class TDS6154C_CHAN(platypus.base.Channel):
    '''
    This class is to be able to create multiple channels from the same instrument.
    You need to create a child of this class for every channel you need.
    '''
    def __init__(self, name, scope, *args, **kwargs):
        self.setname(name)
        self.scope = scope
        self.last_value = None
        super().__init__(*args, **kwargs)
    def validate(self, value): pass
    def set(self, v): pass
    def stop(self):pass
    def getvalue(self):
        return self.last_value
    def gettype(self):
        #This is a measurment channel, so the type 'm' is returned.
        return 'm'



class TDS6154C_RMS(TDS6154C_CHAN):
    '''
    This class can be used to create a channel that always
    shows the RMS value of the scope. It is a child of TDS6154C_CHAN
    One has to declare it in the platypus config file as follows:
        FASTscope = TDS6154C('TCPIP0::169.254.62.140::inst0::INSTR') #Declare the scope
        FASTscope_RMS = TDS6154C_RMS('Fast Scope RMS', FASTscope) #Declare the scope_RMS channel

    '''
    def read(self):
        self.last_value = self.scope.get_RMS()
        return self.last_value
